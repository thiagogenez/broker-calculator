\documentclass[12pt, letter]{article}

%packages put by template
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{fullpage}
\usepackage[brazil]{babel}
\usepackage{indentfirst}
\usepackage{latexsym}
\usepackage{amssymb}
\usepackage{ae} 
\usepackage{moreverb}
\usepackage[hyperindex=false]{hyperref}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{lmodern} %hifienizacao
\usepackage{acronym}
\usepackage{xspace}

\title{Primeiro Relatório do Projeto da Disciplina MO648/MC962}
\author{Diego Hachmann \\ Thiago Augusto Lopes Genez}
\date{\today}


\acrodef{DAG}			{\textsl{Directed Acyclic Graph}}
\acrodef{IaaS}    		{\textsl{Infrastructure-as-a-Service}}
\acrodef{PaaS}    		{\textsl{Plataform-as-a-Service}}
\acrodef{SaaS}    		{\textsl{Software-as-a-Service}}
\acrodef{VM}			{\textsl{Virtual Machine}}
\acrodef{PCP}			{\textsl{Partial Critical Paths}}
\acrodef{PSO}			{\textsl{Particle Swarm Optimization}}
\acrodef{hcoc}	[HCOC]	{\textsl{Hybrid Cloud Optimized Cost}}
\acrodef{rdpso}	[RDPSO]	{\textsl{Revised Discrete Particle Swarm Optimization}}
\acrodef{brs}	[BRS]	{\textsl{Best Resource Selection}}


\def\etc{etc.\@\xspace}
\def\etal{\emph{et al.}\@\xspace}

\begin{document} 
\maketitle


\section{Introdução}

Computação em nuvem emergiu como um novo paradigma eficaz, onde hardware e software são entregues como serviços de utilidade geral e disponibilizados para os usuários através da Internet. Graças à camada de virtualização, construída em cima dos recursos computacionais físicos, é possível otimizar o uso da infraestrutura e, principalmente, oferecer diferentes tipos de hardware e software aos usuários da nuvem. Dessa maneira, computação em nuvem permite que os recursos (serviços) sejam alugados e liberados de acordo com a necessidade dos usuários e tarifados através do modelo ``pago-pelo-uso'' (do inglês, \textit{pay-as-you-go}) \cite{springerlink:10.1007/s13174-011-0027-x}.

A nuvem está atualmente sendo muito utilizada por instituições   para evitar custos de manutenção e investimentos iniciais em infraestrutura computacional. Por exemplo,   as  aplicações  complexas de e-Ciência e \emph{e-Business} requisitam, hoje em dia,  um  poder computacional cada vez maior, o qual é tradicionalmente superior ao montante que está disponível nas instalações de uma única instituição. Assim,  as nuvens podem ser utilizadas para fornecer recursos de computação extras sempre que necessário, trazendo  \emph{elasticidade} ao poder computacional local (ou privado).   Essas aplicações complexas podem ser modeladas como \emph{workflows}, compondo um conjunto de serviços a serem processados em uma ordem bem-definida; ou seja, podem haver dependências de dados entre serviços que geram precedência de execução. \emph{Workflows} são geralmente representados por  grafos acíclicos direcionados (\emph{Directed Acyclic Graphs} -- \acsp{DAG}) para representar os serviços e suas dependências. O escalonador é o componente responsável por decidir de que forma a distribuição desses serviços será realizada; porém, o problema de escalonamento de \emph{workflows}, em sua forma geral, é NP-Completo. Assim, aprimoramentos em algoritmos, técnicas e heurísticas são fundamentais para se obter um bom balanceamento entre o tempo de execução do escalonador, a complexidade e  a qualidade do escalonamento \cite{springerlink:10.1007/s13174-010-0007-6}.

De acordo com o tipo de serviço providenciado, a computação em nuvem pode ser divida em três modelos \cite{springerlink:10.1007/s13174-010-0007-6, nist}: \ac{IaaS}, \ac{PaaS} e \ac{SaaS}.  IaaS se refere ao fornecimento de recursos de infraestrutura, tais como processamento e armazenamento, geralmente em termos de máquinas virtuais -- \ac{VM}. Por outro lado,  PaaS fornece ambientes para o desenvolvimento de softwares e permite que os clientes possam implantar e testar seus próprios aplicativos na nuvem, eliminando as peculiaridades de gerenciamento de requisitos relacionados com a camada de infraestrutura. Por último,  SaaS se refere à disponibilidade de aplicativos através da Internet, porém os usuários não estão autorizados a controlar o ambiente da aplicação.


Em termos de disponibilidade de recursos, a computação em nuvem é classificada em quatros tipos diferentes \cite{nist, springerlink:10.1007/s13174-011-0032-0}:  privada, comunitária, pública e híbrida. Na nuvem privada, os recursos são disponibilizados para uso exclusivo de uma única organização, semelhante à grade  e fazenda de servidores (do inglês, \emph{server farms}\footnote{\emph{Server farm} é um conjunto de servidores ligados em rede que dividem tarefas, atuando como se fossem um único grande servidor.}). Já na nuvem comunitária, os recursos são liberados para uso exclusivo por uma comunidade específica de organizações cujos interesses são  comuns (por exemplo, requisitos de segurança, política de acesso, etc.). A nuvem pública, por sua vez, são nuvens onde os provedores  oferecem  recursos computacionais como serviços ao público em geral. Esse tipo nuvem existe sob as premissas de um provedor de serviços e, além disso,   tarifam os serviços disponibilizados através do modelo ``pago-pelo-uso''.  Por último, a nuvem híbrida é a combinação do uso da nuvem  pública com a  privada, a fim de solucionar as limitações de cada abordagem. Em uma nuvem híbrida, por exemplo, as aplicações não-críticas podem ser executadas em nuvens públicas, enquanto as críticas  em nuvens privadas. 

Este relatório trata sobre o início do estudo e da implementação de algoritmos de escalonamento de \emph{workflows} em nuvens híbridas. Além disso, consideramos uma nuvem pública composta por múltiplos provedores de \ac{IaaS}, sendo que cada provedor estipula as configurações das suas próprias máquinas virtuais com um preço associado. Portanto,  o principal objetivo deste trabalho é realizar uma comparação dos algoritmos de escalonamento de \emph{workflows} existente na literatura, cuja função objetivo é minimizar custo monetários do escalonamento; ou seja, reduzir gastos com a terceirização de recursos computacionais. Para realizar este trabalho, vamos utilizar o simulador de computação em nuvem denominado  \emph{Cloudsim}\footnote{\url{http://code.google.com/p/cloudsim/}}\cite{Calheiros:2011:CTM:1951445.1951450}, o qual é \emph{open-source} e implementado em \emph{Java}\footnote{\url{http://www.java.com}}.

O restante deste relatório está organizado da seguinte maneira. Detalhes do trabalho anterior desenvolvido nesta disciplina relacionado com este tema está descrito na Seção \ref{sec:anteriores}, enquanto  escalonadores de  \emph{workflows} em nuvens híbridas (ou de apenas em nuvens públicas) que serão implementados neste trabalho  são apresentados na Seção \ref{sec:escalonadores}. A Seção \ref{sec:realizados} descreve os procedimentos realizados deste projeto até esta data, e para finalizar, a Seção \ref{sec:proximos} apresenta os próximos passos deste trabalho.


\section{Trabalhos Anteriores}
\label{sec:anteriores}

Este projeto é uma continuação do trabalho desenvolvido nesta disciplina no segundo semestre de 2011 pelos alunos Aloisio Vilas-Boas,  Andre Petris Esteve e Atilio Gomes Luiz. Estes alunos implementaram quatro algoritmos de escalonamento de \emph{workflows} em nuvens híbridas: algoritmo aleatório,  algoritmo \emph{round-robin}, \ac{PCP}\cite{Abrishami:2012:CSG:2360765.2361186} e \ac{PSO}\cite{Pandey:2010:PSO:1825731.1826139}.
Os dois primeiros algoritmos são simples e serviram apenas para realizar  uma comparação com aos outros dois algoritmos restantes, os quais são mais sofisticados pois são  baseados em heurísticas. Entretanto, o código implementados por esses alunos não consideram múltiplos provedores de \ac{IaaS} na nuvem pública, uma aspecto muito importante no ambiente da nuvem. Além disso, nas simulações apresentadas por esses alunos, cada máquina física instancia apenas uma única máquina virtual, uma característica não realística na computação em nuvem. Neste trabalho, pretendemos simular outros algoritmos de escalonamento de \emph{workflows} em nuvens híbridas (descritos na Seção \ref{sec:escalonadores}), além de utilizar múltiplos provedores de \ac{IaaS}, sendo que cada provedor será composto por \emph{hacks} homogêneos de máquinas físicas; porém, os \emph{hacks} entre terão poder computacional heterogêneo. 

\section{Escalonadores de \emph{Workflows} em Nuvens}
\label{sec:escalonadores}

Nesta seção vamos apresentar os algoritmos de escalonamento de \emph{workflows} que serão implementados neste projeto. Além disso, vamos utilizar os algoritmos implementados da seção anterior para fazer uma comparação aprofundada dos resultados. 

Bittencourt \etal propõem em \cite{5691241} uma estratégia (heurística) para escalonar \emph{workflow} de serviços em nuvens híbridas. Essa estratégia determina quando e como solicitar novos recursos da nuvem pública para satisfazer \emph{deadlines} ou obter um tempo de execução (\emph{makespan}) razoável, enquanto minimiza os custos monetários envolvidos. Em outras palavras, mostra como maximizar o uso da infraestrutura privada (computadores locais ou grades, por exemplo), enquanto minimiza o uso da infraestrutura terceirizada (nuvens públicas). Os autores realizaram experimentos apenas com o \ac{DAG} \emph{fork-join}  e concluíram que, ao providenciar \emph{elasticidade} à nuvem privada, foi possível diminuir consideravelmente o \emph{makespan} do \emph{workflow}, com pouco investimento em recursos terceirizados.


Uma outra estratégia (heurística) para escalonar \textit{workflows} de serviços em nuvens híbridas foi proposta por Bittencourt e Madeira em \cite{springerlink:10.1007/s13174-011-0032-0}. É apresentado um algoritmo, denominado \ac{hcoc}, que decide quais recursos devem ser alugados da nuvem pública para aumentar o poder de processamento da nuvem privada.  Esse trabalho é um avanço do anterior (em \cite{5691241}), pois são realizados experimentos e simulações com oito aplicações \emph{workflows} do mundo real: AIRSN, \emph{Chimera-1}, \emph{Chimera-2}, CSTEM, LIGO-1, LIGO-2, \emph{fork-join} e \emph{Montage}. 

Wu \etal também propõem em \cite{5696259} um algoritmo de escalonamento estático de \emph{workflows} de tarefas para computação em nuvem. O algoritmo é desenvolvido através de uma versão melhorada do \ac{PSO} (em \cite{Pandey:2010:PSO:1825731.1826139}),  conhecido como otimização por enxame de partículas discreto e revisado -- \ac{rdpso}. Além de  minimizar o tempo de execução (\emph{makespan}), os autores também minimizam o custo monetário da execução do \emph{workflow}. Para realizar o escalonamento, a formulação  também leva em consideração os custos de computação das tarefas e os custos de transmissão de dados entre elas. Enfim, ao utilizar a técnica de \ac{rdpso} para escalonar aplicações \emph{workfows} de larga escala em nuvens, os autores conseguiram melhores reduções nos custos monetários, quando comparado aos escalonamentos providos pelos algoritmos \ac{PSO} e \ac{brs}. É importante frisar que ambos os métodos de otimização -- \ac{PSO} e \ac{rdpso} -- são classificados como meta-heurística.

Mohammadi \etal  propõem em \cite{Fard:2011:BTM:2114498.2116131} um algoritmo de escalonamento bi-critério de \emph{workflows} de tarefas para ambientes distribuídos com fins comerciais, como a computação em nuvem, por exemplo. Baseado na teoria dos jogos, esse algoritmo minimiza tanto o \emph{makespan}, quanto  o custo monetário da execução do \emph{workflow}.  

\section{Procedimentos Realizados}
\label{sec:realizados}

Além da revisão bibliográfica dos algoritmos de escalonamento de \emph{workflows}, realizamos um estudo aprofundado no simulador \emph{Cloudsim} e  no código implementados pelos alunos descritos na seção anterior. Além disso, atualizamos a versão do \emph{Cloudsim}, da versão $2.1.1$ para $3.0$, a fim de corrigir \emph{bugs} interno do simulador. Em seguida, realizamos alguns testes nos algoritmos de escalonamento implementados e verificamos algumas inconsistências no código em relação as dependências de dados entre  nós do \emph{workflows}. Em outras palavras, o código implementado não considera várias dependências de um nó.  Além disso, devido à modelagem do código dos alunos não considerar múltiplos provedores de \ac{IaaS}, estamos restruturando o código a fim de deixar o ambiente de simulação mais realístico. Após terminado o ambiente de simulação do \emph{workflow} e corrigidos os erros encontrados até então, iniciaremos a implementação dos algoritmos de escalonamento de \emph{workflows}   apresentados na \nolinebreak Seção \ref{sec:escalonadores}.
\section{Próximos Passos}
\label{sec:proximos}


\bibliographystyle{ieeetr}
\bibliography{ref}


\end{document}