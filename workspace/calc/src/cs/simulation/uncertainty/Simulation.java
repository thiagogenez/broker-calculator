package cs.simulation.uncertainty;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.math3.fraction.Fraction;
import org.apache.commons.math3.fraction.FractionFormat;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import cs.core.dag.DAG;
import cs.core.dag.DAGParser;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.engine.EnvironmentFactory;
import cs.core.exception.SimulatiorException;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.scheduling.VMTypesLoader;
import cs.core.vm.simulation.VM;
import cs.core.vm.simulation.statistics.CloudProviderStatistics;
import cs.scheduling.algorithms.Algorithm;
import cs.scheduling.algorithms.StaticAlgorithmFactory;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;
import cs.simulation.config.DAGParameters;
import cs.simulation.config.SchedulerParameters;
import cs.simulation.config.SimulatorParameters;
import cs.simulation.config.VMParameters;

public class Simulation {

	private final VMTypesLoader vmTypesLoader;

	public Simulation(VMTypesLoader vmTypesLoader) {
		this.vmTypesLoader = vmTypesLoader;
	}

	public static void main(String[] args) {
		SimulatorParameters simulatorParameters = new SimulatorParameters();
		DAGParameters dagParameters = new DAGParameters();
		VMParameters vmParameters = new VMParameters();
		SchedulerParameters schedulerParameters = new SchedulerParameters();
		JCommander cmd = null;

		try {
			cmd = new JCommander();
			cmd.addObject(dagParameters);
			cmd.addObject(vmParameters);
			cmd.addObject(schedulerParameters);
			cmd.addObject(simulatorParameters);
			cmd.parse(args);
		} catch (ParameterException exp) {
			cmd.usage();
			exp.printStackTrace();
			System.exit(1);
		}

		Simulation testRun = new Simulation(new VMTypesLoader());

		try {
			testRun.run(dagParameters, vmParameters, schedulerParameters, simulatorParameters);
		} catch (SimulatiorException e) {
			throw new SimulatiorException(e.getMessage());
		}
	}

	private String getSimulationFilename(SchedulerParameters schedulerParameters, DAGParameters dagParameters) {

		String filename = "";
		if (!schedulerParameters.getFilenamePrefix().isEmpty()) {
			filename = schedulerParameters.getFilenamePrefix();

		} else {
			filename = schedulerParameters.getAlgorithm() + "-" + dagParameters.getApplication() + "-deadline-"
					+ schedulerParameters.getDeadline();
		}
		return filename;
	}

	private DAG getDAG(DAGParameters dagParameters) {
		// defining new values for edge and node weights
		DAGParser.setUniformeIntegerDistribution(dagParameters.getNodes(), "NODE");
		DAGParser.setUniformeIntegerDistribution(dagParameters.getEdges(), "EDGE");

		// reading the dag file and parse it
		DAG dag = DAGParser.parseDAG(new File(dagParameters.getApplicationDir(), dagParameters.getApplication()));
		dag.setId(new Integer(0).toString());

		return dag;
	}

	private Environment getEnvironment(VMParameters vmParameters) {
		// reading the vms file and parse it
		Map<String, VMType> vmTypes = vmTypesLoader.determineVMTypes(vmParameters);

		// creating the environment for the simulation
		Environment environment = EnvironmentFactory.createEnvironment(vmTypes);
		return environment;
	}

	private StaticAlgorithm getAlgorithm(DAG dag, Environment environment, double deadline, DAGParameters dagParameters,
			VMParameters vmParameters, SchedulerParameters schedulerParameters) {

		StaticAlgorithm algorithm = StaticAlgorithmFactory.createAlgorithm(dag, environment, deadline, dagParameters,
				vmParameters, schedulerParameters);

		// adding the vms to the algorithm
		algorithm.addVMTypes(environment.getAllVMTypes());

		return algorithm;

	}

	public void run(DAGParameters dagParameters, VMParameters vmParameters, SchedulerParameters schedulerParameters,
			SimulatorParameters simulatorParameters) {

		// creating the other log files
		String filename = getSimulationFilename(schedulerParameters, dagParameters);

		// get DAG
		DAG dag = getDAG(dagParameters);

		// get Environment
		Environment environment = getEnvironment(vmParameters);

		// get the makespan of the sequential execution
		double sequentialTimeExecution = getSequentialTimeExecution(dag, environment);

		// calculating the deadline value
		double deadline = getDeadline(schedulerParameters.getDeadline(), dag, environment, sequentialTimeExecution);

		// creating the algorithm
		StaticAlgorithm algorithm = getAlgorithm(dag, environment, deadline, dagParameters, vmParameters,
				schedulerParameters);

		// logging DAG, VMs and parameters used
		if (simulatorParameters.isVerbose()) {
			Logger simulationLogger = new Logger(simulatorParameters.getOutput(), filename, ".parameters",
					simulatorParameters.isVerbose());

			logVMType(environment.getAllVMTypes(), simulationLogger);
			logWorkflowsDescription(dag, simulationLogger);
			logSimulationParameters(simulatorParameters, dagParameters, schedulerParameters, vmParameters,
					simulationLogger);

			// closing the files
			simulationLogger.close();
		}

		//
		// SIMULATION RUNNING STEP
		//
		boolean contention = false;

		// Json
		JsonArray brsJsonArray = new JsonArray();

		// run the scheduling for all BR values
		for (String br : simulatorParameters.getBandwidthReductor()) {

			// setting the BR value
			Environment.setReductorValue(Double.parseDouble(br));

			// Json purposes
			JsonObject elementOfBrJsonArray = new JsonObject();
			brsJsonArray.add(elementOfBrJsonArray);

			elementOfBrJsonArray.addProperty("value", br);

			// algorithm's logger
			Logger schedulerLogger = new Logger(simulatorParameters.getOutput(), filename,
					"-br-" + br + ".scheduler.log", true);
			algorithm.setLogger(schedulerLogger);

			// scheduling
			algorithm.newPlan();
			boolean schedule = algorithm.schedule();

			// creating json for the schedule
			elementOfBrJsonArray.addProperty("schedule", schedule);

			// if a schedule has been created
			if (schedule) {

				// Removing the BR value from the environment
				Environment.setReductorValue(0.0);

				// simulate the produced schedule on the simulator
				algorithm.execute(0.0, contention, dagParameters.getDataFileSizeAmplifier());

				// creating a json object of the last simulation
				JsonObject original = getResults(algorithm, environment, deadline, sequentialTimeExecution, dag);

				// add the json to its father
				elementOfBrJsonArray.add("original_schedule_data", original);

				// creating a json array for each simulation with the bv value
				JsonArray bvJsonArray = new JsonArray();
				elementOfBrJsonArray.add("bvs", bvJsonArray);

				// simulate the schedule with the presence of variabilities
				for (String bv : vmParameters.getBandwidthVariances()) {

					// simulate the produced schedule with bv
					algorithm.execute(Double.parseDouble(bv), contention, dagParameters.getDataFileSizeAmplifier());

					// getting results
					JsonObject values = getResults(algorithm, environment, deadline, sequentialTimeExecution, dag);

					// Json purposes
					JsonObject elementOfBvJsonArray = new JsonObject();
					elementOfBvJsonArray.addProperty("value", bv);
					elementOfBvJsonArray.add("schedule_data_with_bv", values);

					// add the json object in the array
					bvJsonArray.add(elementOfBvJsonArray);

					// saving vms log in a json file
					JsonObject logVm = getVmStatment(environment);
					gsonToFile(simulatorParameters.getOutput(), filename + "-br-" + br + "-bv-" + bv + "-log-vm",
							logVm);
				}
			}

			// closing the logger file
			schedulerLogger.close();
		}

		JsonObject finalJson = new JsonObject();
		finalJson.add("brs", brsJsonArray);

		gsonToFile(simulatorParameters.getOutput(), filename, finalJson);
	}

	private void gsonToFile(String output, String filename, JsonObject jsonObject) {

		Logger logger = new Logger(output, filename, ".json", true);
		logger.logln(new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).disableHtmlEscaping().create()
				.toJson(jsonObject));
		logger.close();
	}

	private JsonObject getResults(Algorithm algorithm, Environment environment, double deadline,
			double sequentialTimeExecution, DAG dag) {

		// initializing the log variables for the statement
		double executionCost = 0.0, transferDataCost = 0.0;

		// LOG of each VM
		Map<String, List<VM>> vms = environment.getLeasedVMs();
		Map<String, CloudProviderStatistics> providers = new HashMap<String, CloudProviderStatistics>();

		// producing the statements
		for (String cloudProviderId : vms.keySet()) {

			String cloudName = VMTypesLoader.CLOUD_PROVIDER_FILENAME.get(cloudProviderId);

			CloudProviderStatistics cloud = new CloudProviderStatistics(cloudProviderId, cloudName);

			providers.put(cloudProviderId, cloud);

			double cost = 0.0, runtimeUsed = 0.0, dataTransferFromOutside = 0.0, dataTransferToOutside = 0.0,
					dataTransferCostFromOutside = 0.0, dataTransferCostToOutside = 0.0;

			// for each VM of the provider
			for (VM vm : vms.get(cloudProviderId)) {
				if (vm.isUsed()) {

					// statistics of data transfer FROM outside
					dataTransferCostFromOutside += vm.getDataTransferCostFromOutside();
					dataTransferFromOutside += vm.getTotalDataTransferFromOutside();

					// statistics of data transfer TO outside
					dataTransferCostToOutside += vm.getDataTransferCostToOutside();
					dataTransferToOutside += vm.getTotalDataTransferToOutside();

					cost += vm.getExecutionCost();
				}
			}

			// setting data
			cloud.setExecutionCost(cost);
			cloud.setRuntimeUsed(runtimeUsed);

			// statistics of data transfer FROM outside
			cloud.setDataTransferCostFromOutside(dataTransferCostFromOutside);
			cloud.setDataTransferFromOutside(dataTransferFromOutside);

			// statistics of data transfer TO outside
			cloud.setDataTransferCostToOutside(dataTransferCostToOutside);
			cloud.setDataTransferToOutside(dataTransferToOutside);

		}

		// printing the statements
		for (CloudProviderStatistics cloud : providers.values()) {
			// id
			executionCost += cloud.getExecutionCost();
			transferDataCost += cloud.getTotalMonetaryTransferCost();
		}

		// printing the overall simulation resultsf

		JsonObject values = new JsonObject();

		values.addProperty("sequential_execution", sequentialTimeExecution);
		values.addProperty("makespan", algorithm.getSimulatedMakespan().get(dag));
		values.addProperty("speedup", sequentialTimeExecution / algorithm.getSimulatedMakespan().get(dag));
		values.addProperty("runtime", algorithm.getSchedulingWallTimeInSeconds());
		values.addProperty("executionCost", executionCost);
		values.addProperty("transferDataCost", transferDataCost);
		values.addProperty("totalCost", (executionCost + transferDataCost));
		values.addProperty("deadline", deadline);
		values.addProperty("meet_the_deadline", (algorithm.getSimulatedMakespan().get(dag) <= deadline));

		return values;
	}

	private JsonObject getVmStatment(Environment environment) {

		JsonArray vmJsonArray = new JsonArray();
		double totalExecutionCost = 0.0;

		for (List<VM> vms : environment.getLeasedVMs().values()) {
			for (VM vm : vms) {
				if (vm.isUsed()) {
					// System.out.println(vm.getStatement());
					JsonObject info = new JsonObject();
					info.addProperty("VmType", vm.getVmType().toString());
					info.addProperty("vmId", vm.getId());
					info.addProperty("vmTypeId", vm.getVmType().getId());
					info.addProperty("datacenterId", vm.getVmType().getDatacenterId());
					info.addProperty("lunch_time", vm.getLaunchTime());
					info.addProperty("finish_time", vm.getTerminateTime());
					info.addProperty("busy_time", vm.getBusyTime());
					info.addProperty("runtime", vm.getRuntime());
					info.addProperty("execution_cost", vm.getExecutionCost());
					info.addProperty("utilization", vm.getUtilization());
					vmJsonArray.add(info);

					// System.out.println( vm.getStatement());

					totalExecutionCost += vm.getExecutionCost();

				}
			}
		}

		JsonObject jsonObject = new JsonObject();
		jsonObject.add("log", vmJsonArray);
		jsonObject.addProperty("total_execution_cost", totalExecutionCost);

		return jsonObject;
	}

	private void logSimulationParameters(SimulatorParameters simulatorParameters, DAGParameters dagParameters,
			SchedulerParameters schedulerParameters, VMParameters vmParameters, Logger simulationLogger) {
		// Echo dag parameters
		simulationLogger.logln("echo DAGParameters:");
		simulationLogger.log("application = %s\n", dagParameters.getApplication());
		simulationLogger.log("application-dir = %s\n\n", dagParameters.getApplicationDir());

		// Echo VM parameters
		simulationLogger.logln("echo VMParameters:");
		simulationLogger.log("vms-dir = %s\n", vmParameters.getVmDir());
		simulationLogger.log("vms = %s\n", vmParameters.getVms());
		simulationLogger.log("runtime variance = %s\n", vmParameters.getRuntimeVariance());
		simulationLogger.log("bandwidth variances = %s\n\n", vmParameters.getBandwidthVariances());

		// Echo simulator parameters
		simulationLogger.logln("echo SimulatorParameters:");
		simulationLogger.log("Algorithm = %s\n", schedulerParameters.getAlgorithm());
		simulationLogger.log("deadline = %s\n", schedulerParameters.getDeadline());
		simulationLogger.log("bandwidth reductor = %s\n", simulatorParameters.getBandwidthReductor());
		simulationLogger.log("sigma = %s\n", dagParameters.getSigma());
		simulationLogger.log("rho = %s\n", dagParameters.getRho());
		simulationLogger.log("chi = %s\n", vmParameters.getChi());
		simulationLogger.log("omega = %s\n", vmParameters.getOmega());
		simulationLogger.log("output = %s\n\n", simulatorParameters.getOutput());
	}

	private double getDeadline(String deadline, DAG dag, Environment environment, double sequentialExecution) {
		if (deadline.contains("inf")) {
			return Double.MAX_VALUE;
		}

		if (NumberUtils.isNumber(deadline)) {
			return NumberUtils.toDouble(deadline);
		}
		FractionFormat format = new FractionFormat();
		Fraction fraction = format.parse(deadline.replace('_', '/'));

		return sequentialExecution * fraction.doubleValue();
	}

	private double getSequentialTimeExecution(DAG dag, Environment environment) {
		// get the sequential execution time on the best resource
		return (dag.getTotalCPUDemands() / getBestResource(environment).getMips());
	}

	private void logVMType(List<VMType> vmTypes, Logger simulationLogger) {
		for (VMType vmType : vmTypes) {
			simulationLogger.log("VM id = %s\n", vmType.getId());
			simulationLogger.log("VM datacenter id = %s\n", vmType.getDatacenterId());
			simulationLogger.log("VM mips = %f\n", vmType.getMips());
			simulationLogger.log("VM cores = %d\n", vmType.getCores());
			simulationLogger.log("VM price = %f\n", vmType.getPriceForBillingUnit());
			simulationLogger.log("VM unit = %f\n", vmType.getBillingTimeInSeconds());
			simulationLogger.log("VM internal bandwidth = %f\n", vmType.getInternalBandwidth());
			simulationLogger.log("VM external bandwidth = %f\n", vmType.getExternalBandwidth());
			simulationLogger.logln("");
		}
	}

	private void logWorkflowsDescription(DAG dag, Logger simulationLogger) {
		String workflowDescription = String.format("Workflow %s,  filename = %s", dag.getId(), dag.getPath());
		simulationLogger.logln(workflowDescription);

		simulationLogger.logln("\n");
		for (String taskName : dag.getTasks()) {
			Task task = dag.getTaskById(taskName);
			simulationLogger.log("TASK %s:%s\n", task.getId(), task.getSize());
		}
		simulationLogger.logln("\n");
		for (String fileName : dag.getFiles()) {
			simulationLogger.log("FILE %s:%s\n", fileName, dag.getFileSize(fileName));
		}
		simulationLogger.logln("\n");
	}

	public VMType getBestResource(Environment environment) {
		VMType bestPrivate = null;
		VMType bestPublic = null;
		double privateCondition = 0.0;
		double publicCondition = Double.MAX_VALUE;

		for (VMType vmType : environment.getAllVMTypes()) {

			// condition to select a best private resource
			// the resource that the condition (mips * core) is maximum
			if (vmType.getPriceForBillingUnit() <= 0.0 && ((vmType.getMips() * vmType.getCores()) > privateCondition)) {
				privateCondition = vmType.getMips();
				bestPrivate = vmType;
			}
			// otherwise, the condition to select a best public resource,
			// if any private will be found, is to get the resource that
			// the condition $$$ / (core * mips) is minimal
			else if ((vmType.getPriceForBillingUnit() / (vmType.getCores() * vmType.getMips())) < publicCondition) {

				publicCondition = vmType.getPriceForBillingUnit() / (vmType.getCores() * vmType.getMips());
				bestPublic = vmType;
			}

		}

		if (bestPrivate == null && bestPublic == null) {
			throw new IllegalStateException("Any best resource has been found");
		}

		if (bestPrivate != null) {
			return bestPrivate;
		}

		return bestPublic;
	}

}
