package cs.simulation.lambda_granularity;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import cs.core.engine.Environment;
import cs.core.exception.SimulatiorException;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.scheduling.VMTypesLoader;
import cs.simulation.config.DAGParameters;
import cs.simulation.config.SchedulerParameters;
import cs.simulation.config.SimulatorParameters;
import cs.simulation.config.VMParameters;

public class Simulation extends cs.simulation.uncertainty.Simulation {

	public Simulation(VMTypesLoader vmTypesLoader) {
		super(vmTypesLoader);
	}

	public static void main(String[] args) {
		SimulatorParameters simulatorParameters = new SimulatorParameters();
		DAGParameters dagParameters = new DAGParameters();
		VMParameters vmParameters = new VMParameters();
		SchedulerParameters schedulerParameters = new SchedulerParameters();
		JCommander cmd = null;

		try {
			cmd = new JCommander();
			cmd.addObject(dagParameters);
			cmd.addObject(vmParameters);
			cmd.addObject(schedulerParameters);
			cmd.addObject(simulatorParameters);
			cmd.parse(args);
		} catch (ParameterException exp) {
			cmd.usage();
			exp.printStackTrace();
			System.exit(1);
		}

		cs.simulation.uncertainty.Simulation testRun = new Simulation(new VMTypesLoader());

		try {
			testRun.run(dagParameters, vmParameters, schedulerParameters, simulatorParameters);
		} catch (SimulatiorException e) {
			throw new SimulatiorException(e.getMessage());
		}
	}

	@Override
	public VMType getBestResource(Environment environment) {
		VMType cheaper = null;
		double lowCost = Double.MAX_VALUE;

		for (VMType vmType : environment.getAllVMTypes()) {

			if ((vmType.getPriceForBillingUnit() < lowCost)) {

				lowCost = vmType.getPriceForBillingUnit();
				cheaper = vmType;
			}

		}

		if (cheaper == null) {
			throw new IllegalStateException("Any best resource has been found");
		}

		return cheaper;
	}

}
