package cs.simulation.config;

import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.CommaParameterSplitter;

@Parameters(commandDescription = "Scheduler parameters")
public class SchedulerParameters {

	@Parameter(names = { "--deadline", "-d" }, description = "Deadline fraction", required = false)
	private String deadline = "inf";

	@Parameter(names = { "--algorithm", "-a" }, description = "The workflow scheduling algorithm", required = false)
	private String algorithm = "";

	@Parameter(names = {
			"--algorithms" }, splitter = CommaParameterSplitter.class, description = "algorithm_1,...,algorithm_n", required = false)
	private List<String> algorithms;

	@Parameter(names = { "--tiLim" }, description = "Time limit for CPLEX", required = false)
	private double tiLim = -1.0;

	@Parameter(names = { "--epgap" }, description = "Epgap for CPLEX", required = false)
	private double epgap = -1.0;

	@Parameter(names = { "--nodeLim" }, description = "Node limit for CPLEX", required = false)
	private int nodeLim = -1;

	@Parameter(names = {
			"--threads" }, description = "Maximum threads number allowed to be used by the simulator", required = false)
	private int threads = -1;

	@Parameter(names = { "--lambda" }, description = "Lambda value", required = false)
	private double lambda = 1.0;

	@Parameter(names = { "--outputFilenamePrefix" }, description = "Specific output filename", required = false)
	private String filenamePrefix = "simulation";

	@Parameter(names = {
			"--scheduling-policy" }, description = "Scheduling policy for multiple workflow scheduling", required = false)
	private String schedulingPolicy = "";

	public String getDeadline() {
		return deadline;
	}

	public String getAlgorithm() {
		return algorithm;
	}

	public String getFilenamePrefix() {
		return filenamePrefix;
	}

	public double getTiLim() {
		return tiLim;
	}

	public int getNodeLim() {
		return nodeLim;
	}

	public int getThreads() {
		return threads;
	}

	public String getSchedulingPolicy() {
		return schedulingPolicy;
	}

	public double getLambda() {
		return lambda;
	}

	public double getEpgap() {
		return epgap;
	}

	public List<String> getAlgorithms() {
		return algorithms;
	}

	
}
