package cs.simulation.config;

import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.CommaParameterSplitter;

@Parameters(commandDescription = "Simulator parameters")
public class SimulatorParameters {

	@Parameter(names = { "--output", "-out" }, description = "Output dir", required = false)
	private String output = "";

	@Parameter(names = { "--bandwidth-reductor",
			"-br" }, description = "Mechanism that reduces the bandwidth value", splitter = CommaParameterSplitter.class, required = false)
	private List<String> bandwidthReductor;

	@Parameter(names = { "--verbose", "-v" }, description = "Verbose", required = false, arity = 0)
	private boolean verbose = false;

	@Parameter(names = { "--equalCPU", "-eqCPU" }, description = "Equal CPU frequencies", required = false, arity = 0)
	private boolean equalCPU = false;
	

	public String getOutput() {
		return output;
	}

	public List<String> getBandwidthReductor() {
		return bandwidthReductor;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public boolean isEqualCPU() {
		return equalCPU;
	}

}
