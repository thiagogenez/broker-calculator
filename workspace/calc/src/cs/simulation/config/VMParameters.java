package cs.simulation.config;

import java.util.Arrays;
import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.converters.BooleanConverter;
import com.beust.jcommander.converters.CommaParameterSplitter;
import com.beust.jcommander.converters.DoubleConverter;
import com.beust.jcommander.converters.IntegerConverter;

public class VMParameters {

	@Parameter(names = { "--vm",
			"-vm" }, splitter = CommaParameterSplitter.class, description = "VM configs filename1 ... filename_n", required = false)
	private List<String> vms;

	@Parameter(names = { "--runtime-variance",
			"-rv" }, converter = DoubleConverter.class, description = "Runtime variance, defaults to 0.0")
	private double runtimeVariance = 0.0;

	@Parameter(names = { "--vms-directory",
			"-vms-dir" }, description = "VM config directory, config files are loaded relatively to its path")
	private String vmDir;

	@Parameter(names = { "--bandwidth-variances",
			"-bv" }, description = "Bandwidth variance", splitter = CommaParameterSplitter.class, required = false)
	private List<String> bandwidthVariances;

	@Parameter(names = {
			"--chi" }, converter = DoubleConverter.class, description = "Uncertainty level of chi% of processing capacity", required = false)
	private double chi = 0.0;

	@Parameter(names = {
			"--omega" }, converter = DoubleConverter.class, description = "Uncertainty level of omega% of the available bandwidth", required = false)
	private double omega = 0.0;

	@Parameter(names = {
			"--internalBandwidth" }, converter = DoubleConverter.class, description = "Available bandwidth value within the cloud", required = false)
	private double internalBandwidth = -1.0;

	@Parameter(names = {
			"--externalBandwidth" }, converter = DoubleConverter.class, description = "Available external bandwidth value outside the cloud", required = false)
	private double externalBandwidth = -1.0;

	@Parameter(names = {
			"--contention" }, description = "contention", splitter = CommaParameterSplitter.class, converter = BooleanConverter.class)
	private List<Boolean> contention = Arrays.asList(false);

	@Parameter(names = {
			"--processing-capacity" }, splitter = CommaParameterSplitter.class, converter = DoubleConverter.class, description = "min, max", required = false)
	private List<Double> processingCapacity;

	@Parameter(names = {
			"--bandwidth" }, splitter = CommaParameterSplitter.class, converter = DoubleConverter.class, description = "min, max", required = false)
	private List<Double> bandwidth;

	@Parameter(names = {
			"--resources" }, converter = IntegerConverter.class, description = "number of resources", required = false)
	private int numberOfResources = 0;

	public List<String> getVms() {
		return vms;
	}

	public double getRuntimeVariance() {
		return runtimeVariance;
	}

	public String getVmDir() {
		return vmDir;
	}

	public List<String> getBandwidthVariances() {
		return bandwidthVariances;
	}

	public double getOmega() {
		return omega;
	}

	public double getChi() {
		return chi;
	}

	public double getInternalBandwidth() {
		return internalBandwidth;
	}

	public double getExternalBandwidth() {
		return externalBandwidth;
	}

	public List<Boolean> getContention() {
		return contention;
	}

	public List<Double> getProcessingCapacity() {
		return processingCapacity;
	}

	public List<Double> getBandwidth() {
		return bandwidth;
	}

	public int getNumberOfResources() {
		return numberOfResources;
	}

}
