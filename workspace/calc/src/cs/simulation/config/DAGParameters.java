package cs.simulation.config;

import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.CommaParameterSplitter;
import com.beust.jcommander.converters.DoubleConverter;
import com.beust.jcommander.converters.IntegerConverter;
import com.beust.jcommander.converters.StringConverter;

@Parameters(commandDescription = "DAG Parameters")
public class DAGParameters {

	@Parameter(names = {
			"--amplifier" }, converter = DoubleConverter.class, description = "Amplifier value for the size of data files", required = false)
	private double dataFileSizeAmplifier = 1.0;

	@Parameter(names = {
			"--nodes" }, splitter = CommaParameterSplitter.class, description = "minNodeSize,maxNodeSize", required = false)
	private List<Integer> nodes;

	@Parameter(names = {
			"--edges" }, splitter = CommaParameterSplitter.class, converter = IntegerConverter.class, description = "minEdgeSize,maxEdgeSize", required = false)
	private List<Integer> edges;

	@Parameter(names = { "--application", "-app" }, description = "Application name", required = false)
	private String application = "";

	@Parameter(names = { "--applications",
			"-apps" }, splitter = CommaParameterSplitter.class, converter = StringConverter.class, description = "Application names", required = false)
	private List<String> applications = new ArrayList<>();

	@Parameter(names = { "--distribution" }, description = "Type of an ensemble workflows", required = false)
	private String distribution = "";

	@Parameter(names = { "--ensemble-size",
			"--group-size" }, converter = IntegerConverter.class, description = " The number of workflows an ensemble is comprised of.", required = false)
	private int ensembleSize = 20;

	@Parameter(names = {
			"--app-size" }, converter = IntegerConverter.class, description = "Size of the application in term of number of nodes.", required = false)
	private int appSize = 50;

	@Parameter(names = { "--application-directory",
			"-app-dir" }, description = "Appication directory", required = false)
	private String applicationDir = "";

	@Parameter(names = {
			"--sigma" }, converter = DoubleConverter.class, description = "Uncertainty level of sigma% of computing demand", required = false)
	private double sigma = 0.0;

	@Parameter(names = {
			"--rho" }, converter = DoubleConverter.class, description = "Uncertainty levl of rho% of communication demand", required = false)
	private double rho = 0.0;

	@Parameter(names = {
			"--ccr" }, converter = DoubleConverter.class, description = "communication to computation ratio (CCR)", required = false)
	private double ccr = 0.0;

	@Parameter(names = {
			"--ccrs" }, splitter = CommaParameterSplitter.class, converter = DoubleConverter.class, description = "communication to computation ratios (CCRs)", required = false)
	private List<Double> ccrs;
	
	@Parameter(names = {
	"--fromDAGNumber" }, converter = IntegerConverter.class, description = "From DAG number", required = false)
	private int fromDAGNumber = 0;
	
	@Parameter(names = {
	"--toDAGNumber" }, converter = IntegerConverter.class, description = "From DAG number", required = false)
	private int toDAGNumber = 0;
	
	public int getFromDAGNumber() {
		return fromDAGNumber;
	}

	public int getToDAGNumber() {
		return toDAGNumber;
	}

	public String getApplication() {
		return application;
	}

	public String getApplicationDir() {
		return applicationDir;
	}

	public double getSigma() {
		return sigma;
	}

	public double getRho() {
		return rho;
	}

	public List<Integer> getNodes() {
		return nodes;
	}

	public List<Integer> getEdges() {
		return edges;
	}

	public double getDataFileSizeAmplifier() {
		return dataFileSizeAmplifier;
	}

	public String getDistribution() {
		return distribution;
	}

	public int getEnsembleSize() {
		return ensembleSize;
	}

	public int getAppSize() {
		return appSize;
	}

	public List<String> getApplications() {
		return applications;
	}

	public double getCCR() {
		return ccr;
	}

	public List<Double> getCCRs() {
		return ccrs;
	}
	
	

}
