package cs.simulation.ensembles_workflows;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import cs.core.dag.DAG;
import cs.core.dag.DAGParser;
import cs.core.dag.EnsembleDAGListGenerator;
import cs.core.dag.MultipleDAGListGenerator;
import cs.core.engine.Environment;
import cs.core.engine.EnvironmentFactory;
import cs.core.exception.SimulatiorException;
import cs.core.utils.Logger;
import cs.core.utils.Statistics;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.scheduling.VMTypesLoader;
import cs.core.vm.simulation.VM;
import cs.scheduling.algorithms.Algorithm;
import cs.scheduling.algorithms.statizz.MWS;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;
import cs.simulation.config.DAGParameters;
import cs.simulation.config.SchedulerParameters;
import cs.simulation.config.SimulatorParameters;
import cs.simulation.config.VMParameters;

public class Simulation {

	private final VMTypesLoader vmTypesLoader;

	private SimulationResults multipleSimulationResultsZeroBV;

	public Simulation(VMTypesLoader vmTypesLoader) {
		this.vmTypesLoader = vmTypesLoader;
	}

	public static void main(String[] args) {
		SimulatorParameters simulatorParameters = new SimulatorParameters();
		DAGParameters dagParameters = new DAGParameters();
		VMParameters vmParameters = new VMParameters();
		SchedulerParameters schedulerParameters = new SchedulerParameters();

		JCommander cmd = null;

		try {
			cmd = new JCommander();
			cmd.addObject(dagParameters);
			cmd.addObject(vmParameters);
			cmd.addObject(schedulerParameters);
			cmd.addObject(simulatorParameters);
			cmd.parse(args);
		} catch (ParameterException exp) {
			cmd.usage();
			exp.printStackTrace();
			System.exit(1);
		}

		Simulation testRun = new Simulation(new VMTypesLoader());

		try {
			testRun.run(dagParameters, vmParameters, schedulerParameters, simulatorParameters);
		} catch (SimulatiorException e) {
			throw new SimulatiorException(e.getMessage());
		}
	}

	private void run(DAGParameters dagParameters, VMParameters vmParameters, SchedulerParameters schedulerParameters,
			SimulatorParameters simulatorParameters) {

		long seed = System.currentTimeMillis();

		String names[] = getDAGs(seed, dagParameters);

		// String names[] = { "Montage_25.dag"};//, "fork-join2.dag",
		// "fork-join2.dag",
		// //"Montage_25.dag" };// ',
		// // // // "fork-join2.dag", "fork-join2.dag", "fork-join2.dag"};//,
		// // // // "Montage_25.dag" };

		List<DAG> dags = new ArrayList<DAG>();
		int workflow_id = 0;

		for (String name : names) {
			// reading the dag file and parse it
			DAG dag = DAGParser.parseDAG(new File(name));
			dag.setId(new Integer(workflow_id).toString());
			workflow_id++;
			dags.add(dag);
		}

		// reading the vms file and parse it
		Map<String, VMType> vmTypes = vmTypesLoader.determineVMTypes(vmParameters);

		// creating the environment for the simulation
		Environment environment = EnvironmentFactory.createEnvironment(vmTypes);

		// creating the algorithm
		MWS algorithm = new MWS(dags, Double.MAX_VALUE, environment, schedulerParameters.getSchedulingPolicy());

		// adding the vms to the algorithm
		algorithm.addVMTypes(environment.getAllVMTypes());

		// run the scheduling for all BR values
		for (String br : simulatorParameters.getBandwidthReductor()) {

			// adding BR value into the environment
			Environment.setReductorValue(Double.parseDouble(br));

			// creating looger
			Logger simulationLogger = new Logger(simulatorParameters.getOutput(),
					schedulerParameters.getFilenamePrefix() + "-br-" + br, ".log", simulatorParameters.isVerbose());
			algorithm.setLogger(simulationLogger);

			// schedule
			algorithm.newPlan();
			boolean schedule = algorithm.schedule();

			// saving scheduled results
			// saveScheduledInformation(multipleSimulationResults, algorithm);

			if (schedule) {

				// removing BR value from the environment
				Environment.setReductorValue(0.0);

				// simulate the schedule with the presence of variabilities on
				// the available bandwidth
				for (String bv : vmParameters.getBandwidthVariances()) {

					// simulate the execution of a schedule
					SimulationResults multipleSimulationResults = simulateMultipleWorkflowSchedule(algorithm,
							environment, bv);

					JsonArray vmsStatment = getVmStatment(environment);

					// simulate the execution of each workflow separately
					HashMap<DAG, SimulationResults> singleSimulationResults = simulateSingleWorkflowSchedule(algorithm,
							dags, bv);

					if (Double.parseDouble(bv) <= 0.0) {
						this.multipleSimulationResultsZeroBV = multipleSimulationResults;
					}

					// create JSON
					JsonObject jsonObject = createJSON(br, bv, vmsStatment, multipleSimulationResults,
							singleSimulationResults, schedulerParameters.getSchedulingPolicy(),
							dagParameters.getApplications());

					Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls()
							.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).disableHtmlEscaping().create();

					// saving JSON
					Logger gsonLogger = new Logger(simulatorParameters.getOutput(),
							schedulerParameters.getFilenamePrefix() + "-br-" + br + "-bv-" + bv, ".json", true);
					gsonLogger.logln(gson.toJson(jsonObject));
					gsonLogger.close();
				}
			}

		}
	}

	private HashMap<DAG, SimulationResults> simulateSingleWorkflowSchedule(Algorithm algorithm, List<DAG> dags,
			String bv) {

		HashMap<DAG, SimulationResults> results = new HashMap<>();
		// execute one dag per time
		for (DAG dag : dags) {

			// execute
			algorithm.execute(dag, Double.parseDouble(bv), false, 1.0);

			// save
			SimulationResults simulatedResults = new SimulationResults();
			saveSimulatedInformation(simulatedResults, algorithm);

			results.put(dag, simulatedResults);
		}

		return results;
	}

	private SimulationResults simulateMultipleWorkflowSchedule(StaticAlgorithm algorithm, Environment environment,
			String bv) {

		// simulate the execution of the schedule
		algorithm.execute(Double.parseDouble(bv), false, 1.0);

		// saving simulated results
		SimulationResults multipleSimulatedResults = new SimulationResults();
		saveSimulatedInformation(multipleSimulatedResults, algorithm);

		Map<String, List<VM>> vms = environment.getLeasedVMs();
		double cost = 0.0, utilization = 0.0;
		int numberOfVMs = 0;

		// producing the statements
		for (String cloudProviderId : vms.keySet()) {

			// for each VM of the provider
			for (VM vm : vms.get(cloudProviderId)) {
				if (vm.isUsed()) {
					cost += vm.getExecutionCost();
					utilization += vm.getUtilization();
					numberOfVMs++;
				}
			}
		}

		multipleSimulatedResults.setCost(cost);
		multipleSimulatedResults.setRuntime(algorithm.getSchedulingWallTimeInSeconds());
		multipleSimulatedResults.setAvgResourceUtilization(utilization / numberOfVMs);
		multipleSimulatedResults.setOrderOfExecution(algorithm.getDAGOrderOfExecution());

		return multipleSimulatedResults;
	}

	private JsonArray getVmStatment(Environment environment) {

		JsonArray VMsArray = new JsonArray();

		for (List<VM> vms : environment.getLeasedVMs().values()) {
			for (VM vm : vms) {
				if (vm.isUsed()) {
					// System.out.println(vm.getStatement());
					JsonObject info = new JsonObject();
					info.addProperty("VmType", vm.getVmType().toString());
					info.addProperty("lunch_time", vm.getLaunchTime());
					info.addProperty("terminate_time", vm.getTerminateTime());
					info.addProperty("busy_time", vm.getBusyTime());
					info.addProperty("runtime", vm.getRuntime());
					info.addProperty("execution_cost", vm.getExecutionCost());
					info.addProperty("utilization", vm.getUtilization());
					VMsArray.add(info);
				}
			}
		}

		return VMsArray;
	}

	private String[] getDAGs(long seed, DAGParameters dagParameters) {
		// Determine the distribution
		String[] names = null;

		int groupSize = dagParameters.getEnsembleSize();
		String distribution = dagParameters.getDistribution();

		// defining new values for edge and node weights
		DAGParser.setUniformeIntegerDistribution(dagParameters.getNodes(), "NODE");
		DAGParser.setUniformeIntegerDistribution(dagParameters.getEdges(), "EDGE");

		if (dagParameters.getApplications().isEmpty()) {
			String inputname = dagParameters.getApplicationDir() + "/" + dagParameters.getApplication();

			if (distribution.startsWith("uniform_unsorted")) {
				names = EnsembleDAGListGenerator.generateEnsembleDAGListUniformUnsorted(new Random(seed), inputname,
						groupSize);
			}

			else if (distribution.startsWith("uniform_sorted")) {
				names = EnsembleDAGListGenerator.generateEnsembleDAGListUniformSorted(new Random(seed), inputname,
						groupSize, false);
			}

			else if (distribution.startsWith("pareto_unsorted")) {
				names = EnsembleDAGListGenerator.generateEnsembleDAGListParetoUnsorted(new Random(seed), inputname,
						groupSize);
			}

			else if (distribution.startsWith("pareto_sorted")) {
				names = EnsembleDAGListGenerator.generateEnsembleDAGListParetoSorted(new Random(seed), inputname,
						groupSize, false);
			}

			else if (distribution.startsWith("constant")) {
				names = EnsembleDAGListGenerator.generateEnsembleDAGListConstant(new Random(seed), inputname,
						groupSize);
			}

			else if (distribution.startsWith("fixed")) {
				int dagSize = dagParameters.getAppSize();
				names = EnsembleDAGListGenerator.generateEnsembleDAGListConstant(new Random(seed), inputname, dagSize,
						groupSize);
			}

			else {
				throw new RuntimeException("Unrecognized distribution: " + distribution);
			}
		}

		else {
			List<String> dagNames = new ArrayList<String>();

			for (String app : dagParameters.getApplications()) {
				String inputname = dagParameters.getApplicationDir() + "/" + app + "/" + app;
				dagNames.add(inputname);
			}

			if (distribution.startsWith("fixed")) {
				int dagSize = dagParameters.getAppSize();
				names = MultipleDAGListGenerator.generateMultipleDAGListConstantShuffled(new Random(seed), dagNames,
						dagSize, groupSize);
			}

			else if (distribution.startsWith("multiple_ensemble_fixed")) {
				int dagSize = dagParameters.getAppSize();
				names = MultipleDAGListGenerator.generateMultipleDAGEnsembleListConstant(new Random(seed), dagNames,
						dagSize, groupSize * dagNames.size());
			}

			else if (distribution.startsWith("2_DAGs_sizes_50_50_Proportions_")) {
				int dagSize = dagParameters.getAppSize();

				if (dagNames.size() != Character.getNumericValue(distribution.charAt(0))) {
					throw new RuntimeException("Expected " + distribution.charAt(0) + " DAGs, given " + dagNames.size()
							+ " : " + dagNames);
				}

				HashMap<String, Double> proportionsName = new HashMap<>();
				HashMap<String, Double> sizes = new HashMap<>();

				String props[] = null;

				if (distribution.endsWith("RANDOM")) {
					props = new String[2];
					Random random = new Random(seed);
					int prop = (random.nextInt((10 - 0) + 1) + 0) * 10;

					props[0] = String.valueOf(prop);
					props[1] = String.valueOf((100 - prop));

				} else {
					props = distribution.replaceAll("2_DAGs_Equal_Sizes_Proportions_", "").split("_");
				}

				for (int i = 0; i < props.length; i++) {
					double prop = Double.parseDouble(props[i]) / 100.0;
					proportionsName.put(dagNames.get(i), prop);
				}

				names = MultipleDAGListGenerator.generateMultipleDAGListProportionShuffled(new Random(seed),
						proportionsName, dagSize, groupSize);
			}

		}

		return names;
	}

	private JsonObject createJSON(String br, String bv, JsonArray vmsStatment,
			SimulationResults multipleSimulationResults, HashMap<DAG, SimulationResults> singleSimulationResults,
			String policyName, List<String> appNames) {

		JsonObject data = new JsonObject();
		JsonArray DAGsArray = new JsonArray();

		HashMap<DAG, Double> simulatedSlowdowns = new HashMap<DAG, Double>();
		List<DAG> dags = multipleSimulationResults.getOrderOfExecution();
		multipleSimulationResults.setSlowdowns(simulatedSlowdowns);

		double sequentialSimulatedMakespan = 0.0;

		for (DAG dag : dags) {

			// create a dataset
			JsonObject info = new JsonObject();
			info.addProperty("id", Integer.parseInt(dag.getId()));
			info.addProperty("name", dag.getName());

			JsonObject simulatorData = new JsonObject();
			createSimulatorJsonData(multipleSimulationResults, singleSimulationResults, dag, simulatorData,
					simulatedSlowdowns);
			info.add("simulator_data", simulatorData);

			sequentialSimulatedMakespan += singleSimulationResults.get(dag).getSimulatedMakespan().get(dag);

			DAGsArray.add(info);
		}

		Statistics slowdownStatsGeneral = new Statistics(simulatedSlowdowns.values());

		double rizos_unfairness_index = calculateRizosUnFairnessIndex(slowdownStatsGeneral);
		double jains_fairness_index = calculateJainsFairnessIndex(slowdownStatsGeneral);
		double bit_unfairness_index_min_max = calculateBitUnfairnessIndex(slowdownStatsGeneral, 1);
		double bit_unfairness_index_max_min = calculateBitUnfairnessIndex(slowdownStatsGeneral, 2);
		double speedup = sequentialSimulatedMakespan / multipleSimulationResults.getOverallSimulatedMakespan();

		multipleSimulationResults.setSequentialSimulatedMakespan(sequentialSimulatedMakespan);

		multipleSimulationResults.setSpeedup(speedup);

		JsonObject metrics = new JsonObject();
		metrics.addProperty("overall_simulator_makespan", multipleSimulationResults.getOverallSimulatedMakespan());
		metrics.addProperty("sequential_makespan", sequentialSimulatedMakespan);

		metrics.addProperty("speedup", speedup);

		metrics.addProperty("mean_slowdown", slowdownStatsGeneral.getMean());
		metrics.addProperty("std_slowdown", String.format("%.15f", slowdownStatsGeneral.getStandardDeviation()));
		metrics.addProperty("rizos_unfairness_index", String.format("%.15f", rizos_unfairness_index));
		metrics.addProperty("bit_unfairness_index_min_max", String.format("%.15f", bit_unfairness_index_min_max));
		metrics.addProperty("bit_unfairness_index_max_min", String.format("%.15f", bit_unfairness_index_max_min));
		metrics.addProperty("jains_fairness_index", jains_fairness_index);
		metrics.addProperty("jains_fairness_index_min", 1.0 / simulatedSlowdowns.size());
		metrics.addProperty("policy_name", policyName);
		metrics.addProperty("simulation_runtime", multipleSimulationResults.getRuntime());
		metrics.addProperty("cost", multipleSimulationResults.getCost());
		metrics.addProperty("avg_resource_utilization", multipleSimulationResults.getAvgResourceUtilization());

		metrics.addProperty("br", br);
		metrics.addProperty("bv", bv);

		// relative data
		JsonObject relativeData = new JsonObject();
		relativeData.addProperty("relative_speedup",
				this.multipleSimulationResultsZeroBV.getSequentialSimulatedMakespan()
						/ multipleSimulationResults.getOverallSimulatedMakespan());

		relativeData.addProperty("speedup_current_BV/speedup_0_BV",
				speedup / this.multipleSimulationResultsZeroBV.getSpeedup());
		relativeData.addProperty("cost_current_BV/cost_0_BV",
				multipleSimulationResults.getCost() / this.multipleSimulationResultsZeroBV.getCost());
		relativeData.addProperty("overall_makespan_current_BV/ overall_makespan_0_BV",
				multipleSimulationResults.getOverallSimulatedMakespan()
						/ this.multipleSimulationResultsZeroBV.getOverallSimulatedMakespan());

		metrics.add("relative_data_uncertainty", relativeData);

		// results per DAG type

		JsonObject perDAGtype = new JsonObject();
		for (String appName : appNames) {
			List<Double> makespansMultiple = new LinkedList<Double>();
			List<Double> makespansSingle = new LinkedList<Double>();
			List<Double> eftMultiple = new LinkedList<Double>();
			List<Double> slowdowns = new LinkedList<Double>();
			int dagExecuted = 0;

			for (DAG dag : multipleSimulationResults.getSimulatedMakespan().keySet()) {
				if (dag.getName().startsWith(appName)) {
					makespansMultiple.add(multipleSimulationResults.getSimulatedMakespan().get(dag));
					slowdowns.add(multipleSimulationResults.getSlowdowns().get(dag));

					eftMultiple.add(multipleSimulationResults.getSimulatedEFT().get(dag));
					makespansSingle.add(singleSimulationResults.get(dag).getSimulatedMakespan().get(dag));

					dagExecuted++;

				}
			}

			Statistics makespanMultipleStatsPerDAG = new Statistics(makespansMultiple);
			Statistics slowdownsStatsPerDAG = new Statistics(slowdowns);
			Statistics eftMultipleStatsPerDAG = new Statistics(eftMultiple);
			Statistics makespanSingleStatsPerDAG = new Statistics(makespansSingle);

			JsonObject makespan = new JsonObject();
			makespan.addProperty("mean", makespanMultipleStatsPerDAG.getMean());
			makespan.addProperty("std", makespanMultipleStatsPerDAG.getStandardDeviation());
			makespan.addProperty("makespan_multiple_values", Arrays.toString(makespanMultipleStatsPerDAG.getValues()));
			makespan.addProperty("EFT_multiple_values", Arrays.toString(eftMultipleStatsPerDAG.getValues()));
			makespan.addProperty("makespan_single_values", Arrays.toString(makespanSingleStatsPerDAG.getValues()));
			makespan.addProperty("speedup", makespanSingleStatsPerDAG.getSum() / eftMultipleStatsPerDAG.getMax());

			JsonObject slowdown = new JsonObject();
			slowdown.addProperty("mean", slowdownsStatsPerDAG.getMean());
			slowdown.addProperty("std", slowdownsStatsPerDAG.getStandardDeviation());
			slowdown.addProperty("values", Arrays.toString(slowdownsStatsPerDAG.getValues()));
			slowdown.addProperty("rizos_unfairness_index", calculateRizosUnFairnessIndex(slowdownsStatsPerDAG));

			slowdown.addProperty("jains_fairness_index", calculateJainsFairnessIndex(slowdownsStatsPerDAG));

			JsonObject individualStats = new JsonObject();
			individualStats.addProperty("amount_executed_DAG", dagExecuted);
			individualStats.add("makespans", makespan);
			individualStats.add("slowdowns", slowdown);

			perDAGtype.add(appName, individualStats);

		}

		data.add("schedules", DAGsArray);
		data.add("vms", vmsStatment);
		data.add("metrics", metrics);
		data.add("metric_per_DAG", perDAGtype);

		return data;
	}

	private void createSimulatorJsonData(SimulationResults multipleSimulationResults,
			HashMap<DAG, SimulationResults> singleSimulationResults, DAG dag, JsonObject simulatedData,
			HashMap<DAG, Double> simulatedSlowdowns) {

		JsonObject multiple = new JsonObject();
		JsonObject alone = new JsonObject();

		multiple.addProperty("EST:", multipleSimulationResults.getSimulatorEST().get(dag));
		multiple.addProperty("EFT:", multipleSimulationResults.getSimulatedEFT().get(dag));
		multiple.addProperty("makespan", multipleSimulationResults.getSimulatedMakespan().get(dag));

		alone.addProperty("EST:", singleSimulationResults.get(dag).getSimulatorEST().get(dag));
		alone.addProperty("EFT:", singleSimulationResults.get(dag).getSimulatedEFT().get(dag));
		alone.addProperty("makespan", singleSimulationResults.get(dag).getSimulatedMakespan().get(dag));

		double slowdown = singleSimulationResults.get(dag).getSimulatedMakespan().get(dag)
				/ multipleSimulationResults.getSimulatedMakespan().get(dag);

		simulatedSlowdowns.put(dag, slowdown);

		simulatedData.add("multiple", multiple);
		simulatedData.add("alone", alone);
		simulatedData.addProperty("slowdown", slowdown);
	}

	private double calculateJainsFairnessIndex(Statistics stats) {
		return (stats.getSum() * stats.getSum()) / (stats.getN() * stats.getSumSquared());
	}

	private double calculateRizosUnFairnessIndex(Statistics slowdowns) {

		double rizos_fairness = 0.0;
		double mean_slowdown = slowdowns.getMean();

		for (Double slowdown : slowdowns.getValues()) {
			rizos_fairness += Math.abs(slowdown - mean_slowdown);
		}
		return rizos_fairness;
	}

	private double calculateBitUnfairnessIndex(Statistics stats, int opt) {
		if (opt == 1) {
			return stats.getMin() / stats.getMax();
		} else if (opt == 2) {
			return stats.getMax() / stats.getMin();
		}

		return Double.MAX_VALUE;
	}

	private void saveSimulatedInformation(SimulationResults results, Algorithm algorithm) {
		// makespan
		results.setSimulatedMakespan(algorithm.getSimulatedMakespan());

		// overall makespan
		results.setOverallSimulatedMakespan(algorithm.getOverallSimulatedMakespan());

		// execution finishing time
		results.setSimulatedEFT(algorithm.getSimulatedEFT());

		// execution starting time
		results.setSimulatorEST(algorithm.getSimulatedEST());

		// order of the DAG execution
		results.setOrderOfExecution(algorithm.getDAGOrderOfExecution());
	}

	@SuppressWarnings("unused")
	@Deprecated
	private void saveScheduledInformation(SimulationResults results, StaticAlgorithm algorithm) {

		// makespan
		results.setScheduledMakespan(algorithm.getScheduledMakespan());

		// overall makespan
		results.setOverallScheduledMakespan(algorithm.getOverallScheduledMakespan());

		// execution starting time
		results.setScheduledEST(algorithm.getScheduledSET());

		// execution finishing time
		results.setScheduledEFT(algorithm.getScheduledFET());

	}

	@SuppressWarnings("unused")
	@Deprecated
	private void createScheduledJsonData(SimulationResults multipleSimulationResults,
			HashMap<DAG, SimulationResults> singleSimulationResults, DAG dag, JsonObject scheduledData,
			List<Double> scheduledSlowdowns) {

		JsonObject multiple = new JsonObject();
		JsonObject alone = new JsonObject();

		multiple.addProperty("EST:", multipleSimulationResults.getScheduledEST().get(dag));
		multiple.addProperty("EFT:", multipleSimulationResults.getScheduledEFT().get(dag));
		multiple.addProperty("makespan", multipleSimulationResults.getScheduledMakespan().get(dag));

		alone.addProperty("EST:", singleSimulationResults.get(dag).getScheduledEST().get(dag));
		alone.addProperty("EFT:", singleSimulationResults.get(dag).getScheduledEFT().get(dag));
		alone.addProperty("makespan", singleSimulationResults.get(dag).getScheduledMakespan().get(dag));

		double slowdown = singleSimulationResults.get(dag).getScheduledMakespan().get(dag)
				/ multipleSimulationResults.getScheduledMakespan().get(dag);

		scheduledSlowdowns.add(slowdown);

		scheduledData.add("multiple", multiple);
		scheduledData.add("alone", alone);
		scheduledData.addProperty("slowdown", slowdown);

	}
}
