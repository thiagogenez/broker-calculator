package cs.simulation.ensembles_workflows;

import java.util.HashMap;
import java.util.List;

import cs.core.dag.DAG;

class SimulationResults {

	private HashMap<DAG, Double> simulatedMakespan;

	@Deprecated
	private HashMap<DAG, Double> scheduledMakespan;

	private HashMap<DAG, Double> simulatorEST;

	private HashMap<DAG, Double> simulatedEFT;

	@Deprecated
	private HashMap<DAG, Double> scheduledEST;

	@Deprecated
	private HashMap<DAG, Double> scheduledEFT;

	private HashMap<DAG, Double> slowdowns;

	private List<DAG> orderOfExecution;

	private double cost;

	private double runtime;

	private double avgResourceUtilization;

	private double overallSimulatedMakespan;

	private double overallScheduledMakespan;

	private double sequentialSimulatedMakespan;

	private double speedup;

	public HashMap<DAG, Double> getSimulatedMakespan() {
		return simulatedMakespan;
	}

	public void setSimulatedMakespan(HashMap<DAG, Double> simulatedMakespan) {
		this.simulatedMakespan = simulatedMakespan;
	}

	public HashMap<DAG, Double> getScheduledMakespan() {
		return scheduledMakespan;
	}

	public void setScheduledMakespan(HashMap<DAG, Double> scheduledMakespan) {
		this.scheduledMakespan = scheduledMakespan;
	}

	public HashMap<DAG, Double> getSimulatorEST() {
		return simulatorEST;
	}

	public void setSimulatorEST(HashMap<DAG, Double> simulatedEST) {
		this.simulatorEST = simulatedEST;
	}

	public HashMap<DAG, Double> getSimulatedEFT() {
		return simulatedEFT;
	}

	public void setSimulatedEFT(HashMap<DAG, Double> simulatedEFT) {
		this.simulatedEFT = simulatedEFT;
	}

	@Deprecated
	public HashMap<DAG, Double> getScheduledEST() {
		return scheduledEST;
	}

	@Deprecated
	public void setScheduledEST(HashMap<DAG, Double> scheduledEST) {
		this.scheduledEST = scheduledEST;
	}

	@Deprecated
	public HashMap<DAG, Double> getScheduledEFT() {
		return scheduledEFT;
	}

	@Deprecated
	public void setScheduledEFT(HashMap<DAG, Double> scheduledEFT) {
		this.scheduledEFT = scheduledEFT;
	}

	public List<DAG> getOrderOfExecution() {
		return orderOfExecution;
	}

	public void setOrderOfExecution(List<DAG> orderOfExecution) {
		this.orderOfExecution = orderOfExecution;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getRuntime() {
		return runtime;
	}

	public void setRuntime(double runtime) {
		this.runtime = runtime;
	}

	public double getAvgResourceUtilization() {
		return avgResourceUtilization;
	}

	public void setAvgResourceUtilization(double avgResourceUtilization) {
		this.avgResourceUtilization = avgResourceUtilization;
	}

	public double getOverallSimulatedMakespan() {
		return overallSimulatedMakespan;
	}

	public void setOverallSimulatedMakespan(double overallSimulatedMakespan) {
		this.overallSimulatedMakespan = overallSimulatedMakespan;
	}

	@Deprecated
	public double getOverallScheduledMakespan() {
		return overallScheduledMakespan;
	}

	public void setOverallScheduledMakespan(double overallScheduledMakespan) {
		this.overallScheduledMakespan = overallScheduledMakespan;
	}

	public double getSequentialSimulatedMakespan() {
		return sequentialSimulatedMakespan;
	}

	public void setSequentialSimulatedMakespan(double sequentialSimulatedMakespan) {
		this.sequentialSimulatedMakespan = sequentialSimulatedMakespan;
	}

	public double getSpeedup() {
		return speedup;
	}

	public void setSpeedup(double speedup) {
		this.speedup = speedup;
	}

	public HashMap<DAG, Double> getSlowdowns() {
		return slowdowns;
	}

	public void setSlowdowns(HashMap<DAG, Double> slowdowns) {
		this.slowdowns = slowdowns;
	}

}
