package cs.simulation;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.python.core.Options;
import org.python.core.PyList;
import org.python.core.PyTuple;
import org.python.util.PythonInterpreter;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import cs.core.dag.DAG;
import cs.core.dag.DAGParser;
import cs.core.engine.Environment;
import cs.core.engine.EnvironmentFactory;
import cs.core.exception.SimulatiorException;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.scheduling.VMTypesLoader;
import cs.core.vm.simulation.VM;
import cs.scheduling.algorithms.Algorithm;
import cs.scheduling.algorithms.StaticAlgorithmFactory;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;
import cs.simulation.config.DAGParameters;
import cs.simulation.config.SchedulerParameters;
import cs.simulation.config.SimulatorParameters;
import cs.simulation.config.VMParameters;

public class Simulation2 {

	public static Logger SCHEDULING_LOG;

	public static Logger SIMULATION_LOG;

	private final VMTypesLoader vmTypesLoader;

	public Simulation2(VMTypesLoader vmTypesLoader) {
		this.vmTypesLoader = vmTypesLoader;
	}

	public static void main(String[] args) {
		SimulatorParameters simulatorParameters = new SimulatorParameters();
		DAGParameters dagParameters = new DAGParameters();
		VMParameters vmParameters = new VMParameters();
		SchedulerParameters schedulerParameters = new SchedulerParameters();
		JCommander cmd = null;

		try {
			cmd = new JCommander();
			cmd.addObject(dagParameters);
			cmd.addObject(simulatorParameters);
			cmd.addObject(vmParameters);
			cmd.addObject(schedulerParameters);
			cmd.parse(args);
		} catch (ParameterException exp) {
			cmd.usage();
			exp.printStackTrace();
			System.exit(1);
		}

		Simulation2 testRun = new Simulation2(new VMTypesLoader());

		try {
			testRun.run(dagParameters, vmParameters, schedulerParameters, simulatorParameters);
		} catch (SimulatiorException e) {
			throw new SimulatiorException(e.getMessage());
		}
	}

	private void run(DAGParameters dagParameters, VMParameters vmParameters, SchedulerParameters schedulerParameters,
			SimulatorParameters simulatorParameters) {

		String filename = dagParameters.getApplication();

		SCHEDULING_LOG = new Logger(simulatorParameters.getOutput(), filename, ".schedule", false);

		SIMULATION_LOG = new Logger(simulatorParameters.getOutput(), filename, ".log", true);

		// read dag file
		DAG dag = parseDAG(dagParameters);
		dag.setId(new Integer(0).toString());

		// read the vms file
		Map<String, VMType> vmTypes = vmTypesLoader.determineVMTypes(vmParameters);

		// create the environment
		Environment environment = EnvironmentFactory.createEnvironment(vmTypes);

		// create the algorithm
		StaticAlgorithm algorithm = StaticAlgorithmFactory.createAlgorithm(dag, environment, Double.MAX_VALUE,
				dagParameters, vmParameters, schedulerParameters);
		algorithm.setLogger(SCHEDULING_LOG);

		// add selected vms
		algorithm.addVMTypes(environment.getAllVMTypes());

		List<Double> base = new ArrayList<Double>();
		double sumFrequency = 0.0;

		for (VMType vmType : vmTypes.values()) {
			sumFrequency += vmType.getMips();
			base.add(vmType.getMips());
		}

		// create a range of vms where the sum of frequencies keep the same
		List<List<Double>> listOffrequencies;

		if (simulatorParameters.isEqualCPU()) {
			listOffrequencies = new ArrayList<List<Double>>();
			listOffrequencies.add(base);
		} else {
			listOffrequencies = getFrequenciesRange(vmTypes.size(), base, sumFrequency);
		}

		// runtime
		long startTime = System.nanoTime();

		TreeMap<Double, List<Double>> results = solve(algorithm, listOffrequencies, vmTypes, environment);

		long finishTime = System.nanoTime();

		// log
		SIMULATION_LOG.logln("makespan;runtime;frequencies");

		SIMULATION_LOG.logln(
				results.firstKey() + ";" + (finishTime - startTime) / 1.0e9 + ";" + results.get(results.firstKey()));

		if (!simulatorParameters.isEqualCPU()) {
			SIMULATION_LOG.logln(
					results.lastKey() + ";" + (finishTime - startTime) / 1.0e9 + ";" + results.get(results.lastKey()));
		}

		SCHEDULING_LOG.close();
		SIMULATION_LOG.close();
	}

	private TreeMap<Double, List<Double>> solve(Algorithm algorithm, List<List<Double>> listOffrequencies,
			Map<String, VMType> vmTypes, Environment environment) {
		TreeMap<Double, List<Double>> results = new TreeMap<Double, List<Double>>();

		for (int i = 0; i < listOffrequencies.size(); i++) {
			List<Double> frequencies = listOffrequencies.get(i);
			Collections.sort(frequencies);

			// change cpu frequency
			changeCpuFrequencies(vmTypes, frequencies);

			// clear information of previous schedule
			environment.clearLeasedVMs();

			// run the scheduling algorithm
			algorithm.schedule();

			// run the workflow on the environment
			algorithm.execute();

			double makespan = 0.0;

			Map<String, List<VM>> vms = environment.getLeasedVMs();

			for (String cloudProviderId : vms.keySet()) {
				for (VM vm : vms.get(cloudProviderId)) {
					if (vm.isUsed()) {
						makespan = Math.max(makespan, vm.getTerminateTime());
					}
				}
			}

			results.put(makespan, frequencies);
		}
		return results;
	}

	private void changeCpuFrequencies(Map<String, VMType> vmTypes, List<Double> frequencies) {
		if (vmTypes.size() != frequencies.size()) {
			System.out.println("erro");
		}

		int index = 0;
		for (VMType type : vmTypes.values()) {
			type.setMips(frequencies.get(index++));
		}

	}

	private List<List<Double>> getFrequenciesRange(int numberOfVms, List<Double> base, double sumFrequency) {

		int minFrequency = 500;
		double maxFrequency = 0.0;

		sumFrequency *= 1000;
		maxFrequency = sumFrequency - (numberOfVms - 1) * minFrequency;

		Options.importSite = false;
		PythonInterpreter python = new PythonInterpreter();
		python.exec("import itertools");
		python.exec("f = sorted(set([tuple(sorted(p))  for p in itertools.product(range(" + minFrequency + ","
				+ (int) maxFrequency + ",50), repeat=" + numberOfVms + ") if sum(p)== " + (int) sumFrequency + "]))");
		PyList list = (PyList) python.get("f");

		LinkedList<List<Double>> frequencies = new LinkedList<List<Double>>();
		for (Object object : list) {
			PyTuple tuple = (PyTuple) object;
			List<Double> mips = new ArrayList<Double>();
			for (Object tupleElement : tuple) {
				int integer = (Integer) tupleElement;
				mips.add(integer / 1000.0);

			}
			frequencies.add(mips);

		}

		python.cleanup();
		python.close();

		List<Double> last = frequencies.getLast();
		if (!sanityCheck(base, last)) {
			frequencies.add(base);
		} else {
			frequencies.removeLast();
			frequencies.addLast(base);
		}

		return frequencies;

	}

	private boolean sanityCheck(List<Double> base, List<Double> list) {
		if (base.size() != list.size())
			return false;

		for (int i = 0; i < base.size(); i++) {
			if (base.get(i).doubleValue() != list.get(i).doubleValue())
				return false;
		}
		return true;
	}

	private static DAG parseDAG(DAGParameters parameters) {
		return DAGParser.parseDAG(new File(parameters.getApplicationDir(), parameters.getApplication()));
	}

}
