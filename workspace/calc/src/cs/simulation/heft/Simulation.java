package cs.simulation.heft;

import java.io.File;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.math3.distribution.AbstractRealDistribution;
import org.apache.commons.math3.distribution.ConstantRealDistribution;
import org.apache.commons.math3.distribution.UniformRealDistribution;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import cs.core.dag.DAG;
import cs.core.dag.DAGParser;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.engine.EnvironmentFactory;
import cs.core.exception.SimulatiorException;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.scheduling.VMTypesLoader;
import cs.core.vm.simulation.VM;
import cs.scheduling.algorithms.statizz.MWS;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;
import cs.scheduling.algorithms.statizz.heft.HEFT;
import cs.scheduling.algorithms.statizz.heft.HEFTBudget1;
import cs.scheduling.algorithms.statizz.heft.HEFTBudget2;
import cs.scheduling.algorithms.statizz.heft.HEFTIlia;
import cs.scheduling.algorithms.statizz.heft.HEFTLookAhead;
import cs.scheduling.algorithms.statizz.heft.HEFTLookAheadTaskDuplication;
import cs.scheduling.algorithms.statizz.heft.HEFTaskDuplication;
import cs.scheduling.algorithms.statizz.heft.HEFTaskDuplication2;
import cs.scheduling.algorithms.statizz.heft.test.HEFTBudgetTest3Paper;
import cs.scheduling.algorithms.statizz.heft.test.HEFTIliaTest1;
import cs.scheduling.algorithms.statizz.heft.test.HEFTLookAheadTaskDuplicationTest1;
import cs.scheduling.algorithms.statizz.heft.test.HEFTLookAheadTaskDuplicationTest2Paper;
import cs.scheduling.algorithms.statizz.heft.test.HEFTLookAheadTest1;
import cs.scheduling.algorithms.statizz.heft.test.HEFTLookAheadTest2;
import cs.scheduling.algorithms.statizz.heft.test.HEFTLookAheadTest3Paper;
import cs.scheduling.algorithms.statizz.heft.test.HEFTaskDuplicationTest1;
import cs.scheduling.algorithms.statizz.heft.test.HEFTaskDuplicationTest2;
import cs.scheduling.algorithms.statizz.heft.test.HEFTaskDuplicationTest3Paper;
import cs.scheduling.algorithms.statizz.heft.test.HEFTest1;
import cs.scheduling.algorithms.statizz.heft.test.HEFTest2;
import cs.scheduling.algorithms.statizz.heft.test.HEFTest3Paper;
import cs.simulation.config.DAGParameters;
import cs.simulation.config.SchedulerParameters;
import cs.simulation.config.SimulatorParameters;
import cs.simulation.config.VMParameters;

public class Simulation {

	/*
	 * ARGUMENTS: --algorithms
	 * HEFT,HEFT-TaskDuplication,HEFT-LookAhead-TaskDuplication,HEFT-Ilia-W-0.05,
	 * HEFT-Ilia-W-0.10,HEFT-Ilia-W-0.50,HEFT-Ilia-W-0.90 -app HEFT.dag -app-dir
	 * data/applications -vms-dir data/datacenters/paperheft -vm heft.yaml --ccrs
	 * 1.0 --fromDAGNumber 0 --toDAGNumber 2 --nodes 100,500 --processing-capacity
	 * 1,10 --bandwidth 1,10 -out /tmp
	 */

	private final VMTypesLoader vmTypesLoader;

	public Simulation(VMTypesLoader vmTypesLoader) {
		this.vmTypesLoader = vmTypesLoader;
	}

	public static void main(String[] args) {
		SimulatorParameters simulatorParameters = new SimulatorParameters();
		DAGParameters dagParameters = new DAGParameters();
		VMParameters vmParameters = new VMParameters();
		SchedulerParameters schedulerParameters = new SchedulerParameters();
		JCommander cmd = null;

		try {
			cmd = new JCommander();
			cmd.addObject(dagParameters);
			cmd.addObject(vmParameters);
			cmd.addObject(schedulerParameters);
			cmd.addObject(simulatorParameters);
			cmd.parse(args);
		} catch (ParameterException exp) {
			cmd.usage();
			exp.printStackTrace();
			System.exit(1);
		}

		Simulation testRun = new Simulation(new VMTypesLoader());

		try {
			testRun.run(dagParameters, vmParameters, schedulerParameters, simulatorParameters);
		} catch (SimulatiorException e) {
			throw new SimulatiorException(e.getMessage());
		}
	}

	public void run(DAGParameters dagParameters, VMParameters vmParameters, SchedulerParameters schedulerParameters,
			SimulatorParameters simulatorParameters) {

		// creating the other log files
		String filename = getSimulationFilename(schedulerParameters, dagParameters);

		// Json
		JsonObject resultsJsonObject = new JsonObject();

		List<String> flavours = schedulerParameters.getAlgorithms();

		int from = dagParameters.getFromDAGNumber();
		int to = dagParameters.getToDAGNumber();
		List<Double> CCRs = dagParameters.getCCRs();

		for (double CCR : CCRs) {

			JsonArray jsonCCRArray = new JsonArray();
			resultsJsonObject.add(Double.toString(CCR), jsonCCRArray);

			JsonObject dagsJsonObject = new JsonObject();
			jsonCCRArray.add(dagsJsonObject);

			for (int i = from; i < to; i++) {

				// get Environment
				Environment environment = getEnvironment(vmParameters);

				// store results using JSON
				JsonArray heftFlavoursJsonArray = new JsonArray();
				dagsJsonObject.add(Integer.toString(i), heftFlavoursJsonArray);

				// output filename
				String localFilename = filename + "-num-" + i + "-CCR-" + CCR;

				// get DAG
				DAG dagOriginal = getDAG(dagParameters, CCR);

				// logging DAG, VMs and parameters used
				if (simulatorParameters.isVerbose()) {
					Logger simulationLogger = new Logger(simulatorParameters.getOutput(), localFilename,
							".simulation.parameters.log", simulatorParameters.isVerbose());

					logVMType(environment.getAllVMTypes(), simulationLogger);
					logWorkflowsDescription(dagOriginal, simulationLogger);
					logSimulationParameters(simulatorParameters, dagParameters, schedulerParameters, vmParameters,
							simulationLogger);

					// closing the files
					simulationLogger.close();
				}

				for (String flavour : flavours) {

					DAG dag = DAGParser.createDeepCopy(dagOriginal);

					// creating the algorithm
					StaticAlgorithm algorithm = createHeftAlgorithm(dag, environment, flavour);

					// algorithm's logger
					Logger schedulerLogger = new Logger(simulatorParameters.getOutput(), localFilename,
							"-" + flavour + ".log", simulatorParameters.isVerbose());
					algorithm.setLogger(schedulerLogger);

					System.out.println(i + ";CCR=" + CCR + ";" + flavour + ";B:"
							+ ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME));
					// scheduling
					algorithm.schedule();
					System.out.println(i + ";CCR=" + CCR + ";" + flavour + ";E:"
							+ ZonedDateTime.now().format(DateTimeFormatter.RFC_1123_DATE_TIME));

					// simulate
					algorithm.execute(0.0, false, 1.0);

					SimulatorResults scheduledResults = new SimulatorResults();
					SimulatorResults simulatedResults = new SimulatorResults();
					getResults(algorithm, environment, dag, scheduledResults, simulatedResults);

					JsonObject resultJsonObjects = new JsonObject();

					// schedule resutls
					resultJsonObjects.addProperty("flavour", flavour);
					resultJsonObjects.addProperty("makespan-scheduled", scheduledResults.getMakespan());
					resultJsonObjects.addProperty("runtime", scheduledResults.getRuntime());
					resultJsonObjects.addProperty("totalBytesReceived-scheduled",
							scheduledResults.getTotalBytesReceived());
					resultJsonObjects.addProperty("totalBytesSent-scheduled", scheduledResults.getTotalBytesSent());
					resultJsonObjects.addProperty("duplicatas", scheduledResults.getDuplicatas());
					resultJsonObjects.addProperty("cost-scheduled", scheduledResults.getCost());
					resultJsonObjects.addProperty("rescheduled", scheduledResults.getRescheduled());

					// simulated results
					resultJsonObjects.addProperty("makespan-simulated", simulatedResults.getMakespan());
					resultJsonObjects.addProperty("totalBytesReceived-simulated",
							simulatedResults.getTotalBytesReceived());
					resultJsonObjects.addProperty("totalBytesSent-simulated", simulatedResults.getTotalBytesSent());
					resultJsonObjects.addProperty("cost-simulated", simulatedResults.getCost());

					// save json
					heftFlavoursJsonArray.add(resultJsonObjects);

				}
			}
		}

		gsonToFile(simulatorParameters.getOutput(), filename, resultsJsonObject);
	}

	private void gsonToFile(String output, String filename, JsonObject jsonObject) {

		Logger logger = new Logger(output, filename, ".json", true);
		logger.logln(new GsonBuilder().setPrettyPrinting().serializeNulls()
				.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).disableHtmlEscaping().create()
				.toJson(jsonObject));
		logger.close();
	}

	private void getResults(StaticAlgorithm algorithm, Environment environment, DAG dag,
			SimulatorResults scheduledResults, SimulatorResults simulatedResults) {

		/********************************
		 * SCHEDULING PHASE /
		 *******************************/
		// makespan sanity check
		double makespan1 = algorithm.getScheduledMakespan().get(dag);
		double makespan2 = algorithm.getOverallScheduledMakespan();
		if (makespan1 != makespan2) {
			throw new RuntimeException("SCHEDULED makespan value different => " + makespan1 + " != " + makespan2);
		}

		// TOTAL FILE TRANSFER SIZE
		double sent = algorithm.getScheduledTotalBytesSent();
		double received = algorithm.getScheduledTotalBytesReceived();

		// TOTAL DUPLICATAS and RE-SCHEDULED
		int duplicatas = 0;
		int rescheduled = 0;
		
		if (algorithm instanceof HEFTLookAheadTaskDuplication) {
			HEFTLookAheadTaskDuplication heft = (HEFTLookAheadTaskDuplication) algorithm;
			duplicatas = heft.getTotalDuplicatas();
			rescheduled = heft.getRescheduled();
		}

		else if (algorithm instanceof HEFTaskDuplication) {
			HEFTaskDuplication heft = (HEFTaskDuplication) algorithm;
			duplicatas = heft.getTotalDuplicatas();
		}

		else if (algorithm instanceof HEFTaskDuplication2) {
			HEFTaskDuplication2 heft = (HEFTaskDuplication2) algorithm;
			duplicatas = heft.getTotalDuplicatas();
			rescheduled = heft.getRescheduled();
		}

		// RUNTIME
		double runtime = algorithm.getSchedulingWallTimeInSeconds();

		// COST
		double cost = algorithm.getScheduledExecutionCost();

		// save the data
		scheduledResults.setTotalBytesSent(sent);
		scheduledResults.setTotalBytesReceived(received);
		scheduledResults.setMakespan(makespan1);
		scheduledResults.setRuntime(runtime);
		scheduledResults.setDuplicatas(duplicatas);
		scheduledResults.setCost(cost);
		scheduledResults.setRescheduled(rescheduled);

		// /********************************
		// * SIMULATING PHASE /
		// *******************************/

		// MAKESPAN
		makespan1 = algorithm.getSimulatedMakespan().get(dag);
		makespan2 = algorithm.getOverallSimulatedMakespan();
		if (makespan1 != makespan2) {
			throw new RuntimeException("SIMULATED makespan value different => " + makespan1 + " != " + makespan2);
		}

		// DATA and COST
		sent = 0.0;
		received = 0.0;
		cost = 0.0;

		for (List<VM> vms : environment.getLeasedVMs().values()) {
			for (VM vm : vms) {
				if (vm.isUsed()) {
					received += vm.getTotalDataTransferFromInside() + vm.getTotalDataTransferFromOutside();
					sent += vm.getTotalDataTransferToInside() + vm.getTotalDataTransferToOutside();

					cost += vm.getExecutionCost();
					//System.out.println(vm.getStatement());
				}
			}
		}

		if (received != sent) {
			System.out.println("Received = " + received + " and sent =" + sent);
		}

		// save the data
		simulatedResults.setTotalBytesReceived(received);
		simulatedResults.setTotalBytesSent(sent);
		simulatedResults.setMakespan(algorithm.getSimulatedMakespan().get(dag));
		simulatedResults.setCost(cost);

	}

	private String getSimulationFilename(SchedulerParameters schedulerParameters, DAGParameters dagParameters) {

		String filename = schedulerParameters.getFilenamePrefix();

		return filename;
	}

	private DAG getDAG(DAGParameters dagParameters, double ccr) {

		if (dagParameters.getNodes() != null) {

			// defining new values for edge and node weights
			List<Integer> edges = new ArrayList<Integer>();

			for (Integer i : dagParameters.getNodes()) {
				edges.add((int) (i * ccr));
			}

			DAGParser.setUniformeIntegerDistribution(dagParameters.getNodes(), "NODE");
			DAGParser.setUniformeIntegerDistribution(edges, "EDGE");
		}

		// reading the dag file and parse it
		DAG dag = DAGParser.parseDAG(new File(dagParameters.getApplicationDir(), dagParameters.getApplication()));

		// set dag Id
		dag.setId(new Integer(0).toString());

		return dag;
	}

	private Environment getEnvironment(VMParameters vmParameters) {
		// reading the vms file and parse it

		Map<String, VMType> vmTypes = vmTypesLoader.determineVMTypes(vmParameters);

		VMType template = vmTypes.get("0");

		for (int i = 1; i < vmParameters.getNumberOfResources(); i++) {
			VMType vmType = new VMType(template.getMips(), template.getCores(), template.getPriceForBillingUnit(),
					template.getBillingTimeInSeconds(), template.getInternalBandwidth(),
					template.getExternalBandwidth(), String.valueOf(i), template.getDatacenterId(),
					template.getDataTransferInPrice(), template.getDataTransferOutPrice(),
					template.getDataTransferUnit(), "vm" + i);

			vmTypes.put(vmType.getId(), vmType);

		}

		AbstractRealDistribution processingDistribution = getUniformeRealDistribution(
				vmParameters.getProcessingCapacity());

		AbstractRealDistribution bandwidthDistribution = getUniformeRealDistribution(vmParameters.getBandwidth());

		for (Map.Entry<String, VMType> entry : vmTypes.entrySet()) {

			VMType type = entry.getValue();

			if (processingDistribution instanceof UniformRealDistribution) {
				double mips = processingDistribution.sample();
				type.setMips(mips);

				type.setBillingTimeInSeconds(1.0);
				type.setBillingUnitPrice(mips / 10000.0);
			}

			if (bandwidthDistribution instanceof UniformRealDistribution) {

				double bandwidth = bandwidthDistribution.sample();
				type.setExternalBandwidth(bandwidth);
				type.setInternalBandwidth(bandwidth);
			}
		}

		// creating the environment for the simulation
		Environment environment = EnvironmentFactory.createEnvironment(vmTypes);
		return environment;
	}

	private StaticAlgorithm createHeftAlgorithm(DAG dag, Environment environment, String algorithmName) {

		StaticAlgorithm algorithm;

		// HEFT,HEFT-TaskDuplication,HEFT-LookAhead-TaskDuplication,HEFT-Ilia-W-0.05,HEFT-Ilia-W-0.10,HEFT-Ilia-W-0.50,HEFT-Ilia-W-0.90

		switch (algorithmName) {

		case "HEFT":
			algorithm = new HEFT(dag, environment);
			break;

		case "HEFTBudget1":
			algorithm = new HEFTBudget1(dag, environment);
			break;
			
		case "HEFTBudget2":
			algorithm = new HEFTBudget2(dag, environment);
			break;

		case "HEFT-TaskDuplication":
			algorithm = new HEFTaskDuplication(dag, environment);
			break;

		case "HEFT-TaskDuplication2":
			algorithm = new HEFTaskDuplication2(dag, environment);
			break;

		case "HEFT-LookAhead":
			algorithm = new HEFTLookAhead(dag, environment);
			break;

		case "HEFT-LookAhead-TaskDuplication":
			algorithm = new HEFTLookAheadTaskDuplication(dag, environment);
			break;

		case "HEFT-Ilia-W-0.10":
			algorithm = new HEFTIlia(dag, environment, 0.1);
			break;

		case "HEFT-Ilia-W-0.05":
			algorithm = new HEFTIlia(dag, environment, 0.05);
			break;

		case "HEFT-Ilia-W-0.50":
			algorithm = new HEFTIlia(dag, environment, 0.50);
			break;

		case "HEFT-Ilia-W-0.90":
			algorithm = new HEFTIlia(dag, environment, 0.90);
			break;

		case "Flexible-Scheduler":

			List<DAG> dags = new ArrayList<DAG>();
			dags.add(dag);
			algorithm = new MWS(dags, Double.MAX_VALUE, environment, "min-overall-cost");

			break;

		case "HEFTILIATEST1":
			algorithm = new HEFTIliaTest1(dag, environment);
			break;

		case "HEFTASKDUPLICATIONTEST1":
			algorithm = new HEFTaskDuplicationTest1(dag, environment);
			break;

		case "HEFTASKDUPLICATIONTEST2":
			algorithm = new HEFTaskDuplicationTest2(dag, environment);
			break;

		case "HEFTEST1":
			algorithm = new HEFTest1(dag, environment);
			break;
		
		case "HEFTBUDGETEST1":
			algorithm = new HEFTBudgetTest3Paper(dag, environment);
			break;


		case "HEFTEST2":
			algorithm = new HEFTest2(dag, environment);
			break;

		case "HEFTLOOKAHEADTEST1":
			algorithm = new HEFTLookAheadTest1(dag, environment);
			break;

		case "HEFTLOOKAHEADTEST2":
			algorithm = new HEFTLookAheadTest2(dag, environment);
			break;

		case "HEFTLOOKAHEADASKDUPLICATIONTEST1":
			algorithm = new HEFTLookAheadTaskDuplicationTest1(dag, environment);
			break;

		case "HEFTestPaper":
			algorithm = new HEFTest3Paper(dag, environment);
			break;

		case "HEFTaskDuplicationTestPaper":
			algorithm = new HEFTaskDuplicationTest3Paper(dag, environment);
			break;

		case "HEFTLookAheadTestPaper":
			algorithm = new HEFTLookAheadTest3Paper(dag, environment);
			break;

		case "HEFTLookAheadTaskDuplicationTestPaper":
			algorithm = new HEFTLookAheadTaskDuplicationTest2Paper(dag, environment);
			break;

		default:
			throw new SimulatiorException("Unknown algorithm name: " + algorithmName);

		}

		// adding the vms to the algorithm
		algorithm.addVMTypes(environment.getAllVMTypes());

		return algorithm;

	}

	private void logVMType(List<VMType> vmTypes, Logger simulationLogger) {
		for (VMType vmType : vmTypes) {
			simulationLogger.log("VM id = %s\n", vmType.getId());
			simulationLogger.log("VM datacenter id = %s\n", vmType.getDatacenterId());
			simulationLogger.log("VM mips = %f\n", vmType.getMips());
			simulationLogger.log("VM cores = %d\n", vmType.getCores());
			simulationLogger.log("VM internal bandwidth = %f\n", vmType.getInternalBandwidth());
			simulationLogger.log("VM external bandwidth = %f\n", vmType.getExternalBandwidth());
			simulationLogger.logln("");
		}
	}

	private void logWorkflowsDescription(DAG dag, Logger simulationLogger) {
		String workflowDescription = String.format("Workflow %s,  filename = %s", dag.getId(), dag.getPath());
		simulationLogger.logln(workflowDescription);

		simulationLogger.logln("\n");
		for (String taskName : dag.getTasks()) {
			Task task = dag.getTaskById(taskName);
			simulationLogger.log("TASK %s:%s\n", task.getId(), task.getSize());
		}
		simulationLogger.logln("\n");
		for (String fileName : dag.getFiles()) {
			simulationLogger.log("FILE %s:%s\n", fileName, dag.getFileSize(fileName));
		}
		simulationLogger.logln("\n");
	}

	private void logSimulationParameters(SimulatorParameters simulatorParameters, DAGParameters dagParameters,
			SchedulerParameters schedulerParameters, VMParameters vmParameters, Logger simulationLogger) {
		// Echo dag parameters
		simulationLogger.logln("echo DAGParameters:");
		simulationLogger.log("application = %s\n", dagParameters.getApplication());
		simulationLogger.log("application-dir = %s\n", dagParameters.getApplicationDir());
		simulationLogger.log("nodes = %s\n", dagParameters.getNodes());
		simulationLogger.log("CCRs = %s\n\n", dagParameters.getCCRs());

		// Echo VM parameters
		simulationLogger.logln("echo VMParameters:");
		simulationLogger.log("vms-dir = %s\n", vmParameters.getVmDir());
		simulationLogger.log("vms = %s\n", vmParameters.getVms());

		// Echo simulator parameters
		simulationLogger.logln("echo SimulatorParameters:");
		simulationLogger.log("Algorithms = %s\n", schedulerParameters.getAlgorithms());
		simulationLogger.logln("\n");
	}

	public AbstractRealDistribution getUniformeRealDistribution(List<Double> values) {

		if (values == null) {
			return new ConstantRealDistribution(1.0);
		}

		if (values.size() < 2) {
			throw new RuntimeException("It necessary two values to create a uniform distribution");
		}

		Collections.sort(values);
		double lower = values.get(0);
		double upper = values.get(1);

		if (lower < 0 || upper < 0 || lower > upper) {
			throw new RuntimeException(
					"Lower and upper values must be > 0 and lower <= upper: lower = " + lower + "\t upper:" + upper);
		}
		return new UniformRealDistribution(lower, upper);
	}

}
