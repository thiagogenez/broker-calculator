package cs.simulation.heft;

class SimulatorResults {

	private double makespan;

	private double totalBytesSent;

	private double totalBytesReceived;

	private double duplicatas;

	private double cost;

	private int rescheduled;

	public int getRescheduled() {
		return rescheduled;
	}

	public void setRescheduled(int rescheduled) {
		this.rescheduled = rescheduled;
	}

	public double getDuplicatas() {
		return duplicatas;
	}

	public void setDuplicatas(double duplicatas) {
		this.duplicatas = duplicatas;
	}

	private double runtime;

	public double getRuntime() {
		return runtime;
	}

	public void setRuntime(double runtime) {
		this.runtime = runtime;
	}

	public double getMakespan() {
		return makespan;
	}

	public void setMakespan(double makespan) {
		this.makespan = makespan;
	}

	public double getTotalBytesSent() {
		return totalBytesSent;
	}

	public void setTotalBytesSent(double totalBytesSent) {
		this.totalBytesSent = totalBytesSent;
	}

	public double getTotalBytesReceived() {
		return totalBytesReceived;
	}

	public void setTotalBytesReceived(double totalBytesReceived) {
		this.totalBytesReceived = totalBytesReceived;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

}
