package cs.simulation.cpu_frequency;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;

import cs.core.dag.DAG;
import cs.core.dag.DAGParser;
import cs.core.engine.Environment;
import cs.core.engine.EnvironmentFactory;
import cs.core.exception.SimulatiorException;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.scheduling.VMTypesLoader;
import cs.scheduling.algorithms.StaticAlgorithmFactory;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;
import cs.scheduling.algorithms.statizz.cpu_frequency.CPUFrequencyPSO;
import cs.simulation.config.DAGParameters;
import cs.simulation.config.SchedulerParameters;
import cs.simulation.config.SimulatorParameters;
import cs.simulation.config.VMParameters;

public class Simulation {
	public static Logger SCHEDULING_LOG;

	public static Logger SIMULATION_LOG;

	private final VMTypesLoader vmTypesLoader;

	public Simulation(VMTypesLoader vmTypesLoader) {
		this.vmTypesLoader = vmTypesLoader;
	}

	public static void main(String[] args) {
		SimulatorParameters simulatorParameters = new SimulatorParameters();
		DAGParameters dagParameters = new DAGParameters();
		VMParameters vmParameters = new VMParameters();
		SchedulerParameters schedulerParameters = new SchedulerParameters();
		JCommander cmd = null;

		try {
			cmd = new JCommander();
			cmd.addObject(dagParameters);
			cmd.addObject(simulatorParameters);
			cmd.addObject(vmParameters);
			cmd.addObject(schedulerParameters);
			cmd.parse(args);
		} catch (ParameterException exp) {
			cmd.usage();
			exp.printStackTrace();
			System.exit(1);
		}

		Simulation testRun = new Simulation(new VMTypesLoader());

		try {
			testRun.run(dagParameters, vmParameters, schedulerParameters, simulatorParameters);
		} catch (SimulatiorException e) {
			throw new SimulatiorException(e.getMessage());
		}
	}

	private void run(DAGParameters dagParameters, VMParameters vmParameters, SchedulerParameters schedulerParameters,
			SimulatorParameters simulatorParameters) {

		// read dag file
		DAG dag = parseDAG(dagParameters);
		dag.setId(new Integer(0).toString());

		// read the vms file
		Map<String, VMType> vmTypes = vmTypesLoader.determineVMTypes(vmParameters);

		String filename = dagParameters.getApplication();

		SCHEDULING_LOG = new Logger(simulatorParameters.getOutput(), filename, ".schedule", false);

		SIMULATION_LOG = new Logger(simulatorParameters.getOutput(), filename, ".log", true);

		// logging vms
		// logVMType(vmTypes);

		// create the environment
		Environment environment = EnvironmentFactory.createEnvironment(vmTypes);

		// create the algorithm
		StaticAlgorithm algorithm = StaticAlgorithmFactory.createAlgorithm(dag, environment, Double.MAX_VALUE,
				dagParameters, vmParameters, schedulerParameters);
		algorithm.setLogger(SCHEDULING_LOG);

		// add selected vms
		algorithm.addVMTypes(environment.getAllVMTypes());

		// create the algorithm
		CPUFrequencyPSO pso = new CPUFrequencyPSO((StaticAlgorithm) algorithm, environment);

		// runtime
		TreeMap<Double, List<Double>> results;
		long startTime, finishTime;
		do {
			startTime = System.nanoTime();
			results = pso.solve();
			finishTime = System.nanoTime();

			if (results.isEmpty()) {
				System.out.println(dagParameters.getApplication());
				System.out.println(vmParameters.getVms());
			}
		} while (results.isEmpty());

		// log
		SIMULATION_LOG.logln("makespan;runtime;frequencies");

		SIMULATION_LOG.logln(
				results.firstKey() + ";" + (finishTime - startTime) / 1.0e9 + ";" + results.get(results.firstKey()));
		SIMULATION_LOG.logln(
				results.lastKey() + ";" + (finishTime - startTime) / 1.0e9 + ";" + results.get(results.lastKey()));

		SCHEDULING_LOG.close();
		SIMULATION_LOG.close();

	}

	private static DAG parseDAG(DAGParameters parameters) {
		return DAGParser.parseDAG(new File(parameters.getApplicationDir(), parameters.getApplication()));
	}

}
