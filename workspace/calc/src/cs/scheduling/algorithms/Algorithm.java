package cs.scheduling.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import cs.core.dag.DAG;
import cs.core.dag.DAGJob;
import cs.core.dag.Task;
import cs.core.dag.algorithms.TopologicalOrder;
import cs.core.engine.Environment;
import cs.core.engine.WorkflowEngine;
import cs.core.job.JobListener;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.VM;

public abstract class Algorithm {

	/** All simulation's DAGs */
	private List<DAG> dags;

	private List<VMType> vmTypes;

	/** Environment of simulation (VMs, storage info) */
	private Environment environment;

	/** Engine that executes workflows */
	private WorkflowEngine engine;

	private Logger logger;

	private double deadline;

	public Algorithm(DAG dag, double deadline, Environment environment) {
		this(deadline, environment);

		this.dags = new LinkedList<DAG>();
		this.dags.add(dag);

	}

	public Algorithm(List<DAG> dags, double deadline, Environment environment) {
		this(deadline, environment);
		this.dags = dags;
	}

	private Algorithm(double deadline, Environment environment) {
		this.deadline = deadline;
		this.environment = environment;
		this.vmTypes = null;
		this.engine = null;

		// create the engine object and binds with the Algorithm
		engine = new WorkflowEngine(environment);

		this.logger = new Logger();
	}

	/**
	 * Get the scheduler name
	 * 
	 * @return The name of the scheduling algorithm
	 */
	public abstract String getSchedulingAlgorithmName();

	/**
	 * Should return the number of wall time in seconds spent for scheduling
	 */
	abstract public double getSchedulingWallTimeInSeconds();

	public DAG getDAG() {
		return dags.get(0);
	}

	public final List<DAG> getAllDags() {
		return dags;
	}

	public double getDeadline() {
		return deadline;
	}

	public void logln(String msg) {
		logger.logln(msg);
	}

	public void log(String msg) {
		logger.log(msg);
	}

	public boolean schedule() {
		if (vmTypes == null || vmTypes.isEmpty()) {
			throw new IllegalAccessError(
					"The scheduling algorithm needs at least one virtual machine to produce de schedule");
		}
		return simulateInternal();
	}

	protected abstract boolean simulateInternal();

	private void checkEngine() {
		if (engine == null) {
			throw new IllegalStateException("The scheduling algorithm " + getSchedulingAlgorithmName()
					+ " does not create the workflow engine");
		}
	}

	private void checkExecution(List<DAG> dags, List<VM> usedVMs) {

		HashMap<DAG, List<Task>> executed = new HashMap<>();
		for (DAG dag : dags) {
			executed.put(dag, new ArrayList<Task>());
		}

		for (VM vm : usedVMs) {
			if (vm.isUsed()) {
				List<Task> history = vm.getTaskExecutionsHistory();

				// sanity check if a task has been
				// executed more than once
				for (Task task : history) {
					DAG dag = task.getDAG();
					if (executed.get(dag).contains(task)) {
						throw new RuntimeException("Task" + task + "has been executed more than once!!!!");
					}
					executed.get(dag).add(task);
				}
			}

		}

		// sanity check if a task
		// has not been executed
		for (DAG dag : dags) {
			for (String taskName : dag.getTasks()) {
				Task task = dag.getTaskById(taskName);
				if (!executed.get(dag).contains(task)) {
					throw new RuntimeException("Task" + task + "has NOT been executed!!!!");
				}
			}
		}

	}

	public void execute(long seed, double QoI_value, boolean contention, double dataFileSizeAmplifier, List<DAG> dags) {
		checkEngine();

		List<DAGJob> dagJobs = new ArrayList<DAGJob>();
		for (DAG dag : dags) {
			dagJobs.add(new DAGJob(dag));
		}

		engine.initialize();
		engine.setQoI(QoI_value);
		engine.setDataFileSizeAmplifier(dataFileSizeAmplifier);
		engine.execute(dagJobs, contention);

		// sanity check
		List<VM> usedVMs = environment.getUsedVMs();
		checkExecution(dags, usedVMs);
	}

	public void execute(long seed, double QoI_value, boolean contention, double dataFileSizeAmplifier) {

		execute(seed, QoI_value, contention, dataFileSizeAmplifier, getAllDags());
	}

	public void execute(double QoI_value, boolean contention, double dataFileSizeAmplifier) {

		// creating the seed
		long seed = System.currentTimeMillis();
		execute(seed, QoI_value, contention, dataFileSizeAmplifier, getAllDags());
	}

	public void execute() {
		execute(System.currentTimeMillis(), -1, false, 1.0, getAllDags());
	}

	public void execute(DAG dag, double QoI_value, boolean contention, double dataFileSizeAmplifier) {
		List<DAG> toExecute = new LinkedList<DAG>();
		toExecute.add(dag);

		execute(System.currentTimeMillis(), QoI_value, contention, dataFileSizeAmplifier, toExecute);
	}

	public void addJobListener(JobListener listener) {
		engine.addJobListener(listener);
	}

	public void addVMTypes(List<VMType> types) {
		for (VMType vmType : types) {
			addVMType(vmType);
		}
	}

	public void addVMType(VMType vmType) {
		if (vmTypes == null) {
			vmTypes = new LinkedList<VMType>();
		}
		vmTypes.add(vmType);
	}

	public void addVmToTheExecutionEnvironment(VM vm) {
		this.environment.addLeasedVM(vm);
	}

	public HashMap<DAG, Double> getSimulatedMakespan() {
		return engine.getMakespan();
	}

	public HashMap<DAG, Double> getSimulatedEFT() {
		return engine.getSimulatedEFT();
	}

	public HashMap<DAG, Double> getSimulatedEST() {
		return engine.getSimulatedEST();
	}

	public List<DAG> getDAGOrderOfExecution() {
		return engine.getDAGOrderOfExecution();
	}

	public double getOverallSimulatedMakespan() {
		return engine.getOverallMakespan();
	}

	public TopologicalOrder getTopologicalOrder(DAG dag) {
		int index = dags.indexOf(dag);
		return dags.get(index).getTopologicalOrder();
	}

	public TopologicalOrder getTopologicalOrder() {
		return dags.get(0).getTopologicalOrder();
	}

	public List<VMType> getAllVMTypes() {
		return vmTypes;
	}

	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}
}
