package cs.scheduling.algorithms.statizz.pso.multiple_workflows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;

import cs.core.dag.DAG;
import cs.core.dag.DAGFile;
import cs.core.dag.Task;
import cs.core.dag.algorithms.TopologicalOrder;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

public abstract class SchedulingFitnessFunction {

	// keep saved the pointers all the time
	private LinkedHashMap<Integer, VMType> vmTypePointer;

	private LinkedHashMap<Integer, Task> taskPointer;

	private HashMap<Task, Integer> dagPointer;

	private List<DAG> dags;

	private List<VMType> vmTypes;

	private SortedSet<Task> tasks;

	private boolean maximize;

	public SchedulingFitnessFunction(List<DAG> dags, List<VMType> vmTypes, boolean maximize) {
		this.dags = dags;
		this.vmTypes = vmTypes;
		createPointer();

		tasks = definePriority();
		this.maximize = maximize;
	}

	private void createPointer() {
		this.vmTypePointer = new LinkedHashMap<Integer, VMType>();
		this.taskPointer = new LinkedHashMap<Integer, Task>();
		this.dagPointer = new HashMap<Task, Integer>();

		int taskId = 0;

		// creating pointer for VMs
		for (VMType vmType : getAllVMTypes()) {
			vmTypePointer.put(taskId, vmType);
			taskId++;
		}

		// creating pointers for Tasks
		taskId = 0;

		for (DAG dag : dags) {
			List<Task> tasks = new ArrayList<Task>();

			// create a list of tasks
			for (String taskName : dag.getTasks()) {
				tasks.add(dag.getTaskById(taskName));
			}

			for (Task task : tasks) {
				taskPointer.put(taskId, task);
				taskId++;
				dagPointer.put(task, dags.indexOf(dag));
			}
		}
	}

	public int getTaskId(Task task) {

		for (Entry<Integer, Task> e : taskPointer.entrySet()) {
			Integer key = e.getKey();
			Task value = e.getValue();
			if (value.equals(task)) {
				return key;
			}
		}

		throw new RuntimeException("Task :" + task + " not found");
	}

	public VMType getVMType(int vmId) {
		return vmTypePointer.get(vmId);
	}

	public List<VMType> getAllVMTypes() {
		return vmTypes;
	}

	public SortedSet<Task> definePriority() {
		SortedSet<Task> tasks = definePriority(dags);

		// default priority
		if (tasks == null || tasks.isEmpty()) {
			int order = 0;
			tasks = new TreeSet<>(Task.COMPARE_BY_DOWNWARD_RANK);
			for (DAG dag : dags) {
				TopologicalOrder tp = new TopologicalOrder(dag);
				for (Task task : tp) {
					task.setDownwardRank(order);
					order++;
					tasks.add(task);
				}
			}
		}

		return tasks;
	}

	public abstract SortedSet<Task> definePriority(List<DAG> dags);

	public abstract double calulatelanFitnessValue(int[] position, Plan plan);

	public abstract String getPolicyName();

	public HashMap<DAG, Double> getMakespanMult(int[] particlePosition, Plan plan) {

		for (Task task : tasks) {
			calculateFinishExecutionTime(particlePosition, task, plan);
		}

		return plan.getMakespan();
	}

	public double getOverallMakespan(int[] particlePosition, Plan plan) {
		getMakespanMult(particlePosition, plan);

		return plan.getOverallMakespan();
	}

	public HashMap<DAG, Double> getMakespanOwn(int[] particlePosition, Plan plan) {

		HashMap<DAG, Double> m_Own = new HashMap<>();
		for (DAG dag : dags) {
			// create new plan just to simulate the workflow
			// execution by using all resources by itself
			Plan newPlan = PlanFactory.getEmptyPlan(plan);
			// should be HEFT here
			calculateMakespan(particlePosition, dag, newPlan);
			m_Own.put(dag, newPlan.getMakespan(dag));
		}

		return m_Own;
	}

	public HashMap<DAG, Double> getSlowdowns(int[] particlePosition, Plan plan) {

		HashMap<DAG, Double> m_Mult = getMakespanMult(particlePosition, plan);
		HashMap<DAG, Double> m_Own = getMakespanOwn(particlePosition, plan);

		return calculateSlowdowns(m_Mult, m_Own);

	}

	private HashMap<DAG, Double> calculateSlowdowns(HashMap<DAG, Double> m_Mult, HashMap<DAG, Double> m_Own) {
		HashMap<DAG, Double> slowdowns = new HashMap<>();

		for (DAG dag : m_Mult.keySet()) {
			double makespan_multi = m_Mult.get(dag);
			double makespan_own = m_Own.get(dag);
			slowdowns.put(dag, makespan_own / makespan_multi);
		}
		return slowdowns;
	}

	private double calculateMakespan(int[] position, DAG dag, Plan plan) {
		double makespan = 0.0;
		TopologicalOrder tp = new TopologicalOrder(dag);
		for (Task task : tp) {
			makespan = Math.max(makespan, calculateFinishExecutionTime(position, task, plan));
		}
		return makespan;
	}

	private double calculateFinishExecutionTime(int[] position, Task task, Plan plan) {

		int taskId = getTaskId(task);
		VMType vmType = getVMType(position[taskId]);

		// execution time information
		double startExecutionTime = 0.0;
		double runtime = Environment.getPredictedRuntime(task, vmType);

		// calculate the start execution time
		if (!task.getParents().isEmpty()) {

			for (Task parent : task.getParents()) {

				double communicationDemand = parent.getBiggerFile(task);
				// double communicationDemand = parent.getTransferSize(task);

				int parentId = getTaskId(parent);
				VMType parentVmType = getVMType(position[parentId]);

				double bandwidthValue = Environment.getBandwidth(parentVmType, vmType);
				double parentEFT = plan.getScheduleEstimation(parent).getFinishExecutionTime();
				double communicatonTime = (communicationDemand / bandwidthValue);

				// calculating the starting execution time of the
				// current task
				startExecutionTime = Math.max(startExecutionTime, (parentEFT + communicatonTime));

				// time when the VM will be available
				startExecutionTime = Math.max(startExecutionTime,
						plan.getEarliestIdleTimeFrom(startExecutionTime, runtime, vmType));
			}
		}
		double finishExecutionTime = startExecutionTime + runtime;

		ScheduleEstimation estimation = new ScheduleEstimation(startExecutionTime, runtime, finishExecutionTime,
				vmType);

		// logging purposes
		for (Task child : task.getChildren()) {

			int childId = getTaskId(child);
			VMType childVmType = getVMType(position[childId]);

			double bandwidthValue = Environment.getBandwidth(vmType, childVmType);

			for (DAGFile parentFile : task.getOutputFiles()) {
				for (DAGFile childFile : child.getInputFiles()) {

					if (parentFile.equals(childFile)) {
						estimation.addTransmission(child, parentFile, finishExecutionTime,
								childFile.getSize() / bandwidthValue);
					}
				}
			}
		}

		// add to the plan
		plan.addScheduleEstimation(task, estimation);

		return finishExecutionTime;
	}

	public String printSolution(int[] solution) {
		StringBuilder string = new StringBuilder();

		for (DAG dag : dags) {
			string.append("----------------DAG: " + dag.getId() + "----------------\n");

			for (String taskName : dag.getTasksOrdedByDownwardRank()) {
				Task task = dag.getTaskById(taskName);
				int taskId = getTaskId(task);
				int vmId = solution[taskId];
				VMType vmType = getVMType(vmId);

				// string.append(task + " => " + vmType + "\n");
				string.append(String.format("%1$-15s  =>  %2$s\n", task, vmType));
			}
		}
		string.append("---------------------------------------\n");
		return string.toString();
	}

	public String logPointer() {
		StringBuilder builder = new StringBuilder();

		builder.append("----------------Task Pointer ----------------\n");
		for (Integer taskId : this.taskPointer.keySet()) {
			Task task = this.taskPointer.get(taskId);
			int dagId = this.dagPointer.get(task);
			builder.append(String.format("%1$-3s =>  %2$-10s : %3$-5s  <%4$s>\n", taskId, task, dagId, task.getSize()));
		}

		builder.append("----------------VmType Pointer ----------------\n");
		for (Integer vmId : this.vmTypePointer.keySet()) {
			VMType vmType = this.vmTypePointer.get(vmId);
			builder.append(String.format("%1$-3s =>  %2$s\n", vmId, vmType));
		}
		builder.append("---------------------------------------\n");

		return builder.toString();

	}

	public final boolean isMaximize() {
		return this.maximize;
	}

}
