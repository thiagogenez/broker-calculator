package cs.scheduling.algorithms.statizz.pso;

import cs.scheduling.algorithms.statizz.plan.Plan;
import net.sourceforge.jswarm_pso.Swarm;

public interface PSO {

	public static final int MAX_ITERATION = 200;

	public static final int NUMBER_OF_PARTICLES = Swarm.DEFAULT_NUMBER_OF_PARTICLES * 10;

	public static final double INERTIA = Swarm.DEFAULT_INERTIA;

	public static final double PARTICLE_INCREMENT =  Swarm.DEFAULT_PARTICLE_INCREMENT;

	public static final double GLOBAL_INCREMENT = Swarm.DEFAULT_GLOBAL_INCREMENT;

	public static final double NEIGHBORHOOD_INCREMENT = 0.95;

	public int getDimension();

	public double calculateFitnessValue(int[] position, Plan templatePlan);

	public Plan pso(WorkflowFitnessFunction fitness, WorkflowParticle particle);
}
