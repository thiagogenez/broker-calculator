package cs.scheduling.algorithms.statizz.pso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;
import net.sourceforge.jswarm_pso.FitnessFunction;

public class WorkflowFitnessFunction extends FitnessFunction {

	private PSO pso;

	private Plan template;

	private SavedPlan selected;

	private Plan current;

	public WorkflowFitnessFunction(PSO scheduler, boolean maximize, Plan currentPlan, int numberOfDAGs) {
		// Minimize
		super(maximize);
		this.pso = scheduler;
		this.template = currentPlan;
		this.selected = new SavedPlan(maximize);
	}

	// Evaluates a particles at a given position
	@Override
	public double evaluate(double[] position) {
		
		int[] roundedPosition = round(position);
		//System.out.println(positionToString(position) + "\t->"+ positionToString(roundedPosition));
		Plan newPlan = PlanFactory.getEmptyPlan(this.template);

		double fitnessValue = pso.calculateFitnessValue(roundedPosition, newPlan);

		selected.save(fitnessValue, roundedPosition, newPlan);
		current = newPlan;

		newPlan.setFitnessValue(fitnessValue);
		return fitnessValue;
	}
	
	private String positionToString(int[] position) {
		StringBuilder stats = new StringBuilder();
		for (int j = 0; j < position.length; j++) {
			stats.append(String.valueOf(position[j]));
			stats.append(j < (position.length - 1) ? ", " : "");
		}
		stats.append("]");
		return stats.toString();
	}
	
	private String positionToString(double[] position) {
		StringBuilder stats = new StringBuilder();
		for (int j = 0; j < position.length; j++) {
			stats.append(String.valueOf(position[j]));
			stats.append(j < (position.length - 1) ? ", " : "");
		}
		stats.append("]");
		return stats.toString();
	}

	// Round the dimension values
	// to get VM pointer ID
	public int[] round(double[] position) {
		int[] copy = new int[position.length];

		for (int taskId = 0; taskId < copy.length; taskId++) {
			copy[taskId] = (int) Math.round(position[taskId]);
		}
		return copy;
	}

	public Plan getSavedPlan() {
		return selected.getPlan();
	}

	public int[] getSavedPosition() {
		return selected.getPosition();
	}

	public Plan getCurrentPlan() {
		return current;
	}

	@SuppressWarnings("unused")
	private int[] shuffle(int number) {
		List<Integer> list = new ArrayList<>();
		for (int i = 0; i < number; i++) {
			list.add(i);
		}
		Collections.shuffle(list);

		int[] ret = new int[list.size()];
		Iterator<Integer> iter = list.iterator();
		for (int i = 0; iter.hasNext(); i++) {
			ret[i] = iter.next();
		}
		return ret;
	}

}

class SavedPlan {
	private Plan plan;

	private int[] position;

	private double fitnessValue;

	private boolean maximize;

	public SavedPlan(boolean maximize) {
		this.plan = null;
		this.position = null;
		this.maximize = maximize;

		if (maximize) {
			fitnessValue = 0.0;
		} else {
			fitnessValue = Double.MAX_VALUE;
		}
	}

	public void save(double fitnessValue, int[] roundedPosition, Plan newPlan) {

		if (maximize) {
			if (fitnessValue > this.fitnessValue) {
				this.fitnessValue = fitnessValue;
				this.position = roundedPosition;
				this.plan = newPlan;
			}

		} else {
			if (fitnessValue < this.fitnessValue) {
				this.fitnessValue = fitnessValue;
				this.position = roundedPosition;
				this.plan = newPlan;
			}
		}
	}

	public Plan getPlan() {
		return plan;
	}

	public int[] getPosition() {
		return position;
	}

	public double getFitnessValue() {
		return fitnessValue;
	}

	public boolean isMaximize() {
		return maximize;
	}

}