package cs.scheduling.algorithms.statizz.pso;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import net.sourceforge.jswarm_pso.Particle;

public class WorkflowParticle extends Particle {

	private int dimension;

	public WorkflowParticle(int dimension) {
		super(dimension);
		this.dimension = dimension;
	}

	@Override
	public Object selfFactory() {
		Class<? extends WorkflowParticle> cl = this.getClass();
		Constructor<? extends WorkflowParticle> cons;
		Class<?>[] parameterTypes = { int.class };
		Object[] parameterValues = { this.dimension };

		// get the constructor according with the
		// parameter types
		try {
			cons = cl.getConstructor(parameterTypes);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}

		// instanciate the object
		// with the parameters values
		try {
			return cons.newInstance(parameterValues);
		} catch (IllegalArgumentException e1) {
			throw new RuntimeException(e1);
		} catch (InstantiationException e1) {
			throw new RuntimeException(e1);
		} catch (IllegalAccessException e1) {
			throw new RuntimeException(e1);
		} catch (InvocationTargetException e1) {
			throw new RuntimeException(e1);
		}
	}

}
