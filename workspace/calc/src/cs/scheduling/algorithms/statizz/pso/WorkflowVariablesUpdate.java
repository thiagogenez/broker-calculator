package cs.scheduling.algorithms.statizz.pso;

import cs.core.utils.AssortedMethods;
import net.sourceforge.jswarm_pso.Swarm;
import net.sourceforge.jswarm_pso.VariablesUpdate;

public class WorkflowVariablesUpdate extends VariablesUpdate {

	private double[] velocityUpdate;

	public WorkflowVariablesUpdate() {
		super();
	}

	public WorkflowVariablesUpdate(double[] velocity) {
		this();
		this.velocityUpdate = velocity;
	}

	/**
	 * Update Swarm parameters here
	 * 
	 * @param swarm
	 *            : Swarm to update
	 */
	public void update(Swarm swarm) {
		swarm.setInertia(0.095 * swarm.getInertia());
		swarm.setParticleIncrement(0.95 * swarm.getParticleIncrement());
		swarm.setGlobalIncrement(0.95 * swarm.getGlobalIncrement());
		// swarm.setNeighborhoodIncrement(0.95 * swarm.getNeighborhoodIncrement());
	//	swarm.setMaxVelocity(updateVelocity(swarm.getMaxVelocity()));
	//	swarm.setMinVelocity(updateVelocity(swarm.getMinVelocity()));
	}

	private double[] updateVelocity(double[] velocity) {
		for (int i = 0; i < velocity.length; i++) {
			velocity[i] *= AssortedMethods.randomIntInRange(95, 105) / 100.0; // velocityUpdate[i];
		}

		return velocity;
	}
}
