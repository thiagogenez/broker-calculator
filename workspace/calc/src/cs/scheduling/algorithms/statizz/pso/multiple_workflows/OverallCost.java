package cs.scheduling.algorithms.statizz.pso.multiple_workflows;

import java.util.List;
import java.util.SortedSet;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.Plan;

public class OverallCost extends SchedulingFitnessFunction {

	public OverallCost(List<DAG> dags, List<VMType> vmTypes, boolean maximize) {
		super(dags, vmTypes, maximize);
	}

	@Override
	public double calulatelanFitnessValue(int[] position, Plan plan) {
		// calculate the makespan
		getMakespanMult(position, plan);

		// get the cost of the plan
		return plan.getExecutionCost();
	}

	@Override
	public String getPolicyName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public SortedSet<Task> definePriority(List<DAG> dags) {
		return null;
	}
}
