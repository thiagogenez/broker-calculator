package cs.scheduling.algorithms.statizz.pso.multiple_workflows;

import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.utils.Statistics;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.Plan;

public class RizosUnfairness extends SchedulingFitnessFunction {

	public RizosUnfairness(List<DAG> dags, List<VMType> vmTypes, boolean maximize) {
		super(dags, vmTypes, maximize);
	}

	@Override
	public double calulatelanFitnessValue(int[] position, Plan plan) {

		HashMap<DAG, Double> slowdowns = getSlowdowns(position, plan);
		Statistics stats = new Statistics(slowdowns.values());

		double rizos_fairness = 0.0;
		double mean_slowdown = stats.getMean();

		for (Double slowdown : slowdowns.values()) {
			rizos_fairness += Math.abs(slowdown - mean_slowdown);
		}

		return rizos_fairness;

	}

	@Override
	public String getPolicyName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public SortedSet<Task> definePriority(List<DAG> dags) {
		return null;
	}
}
