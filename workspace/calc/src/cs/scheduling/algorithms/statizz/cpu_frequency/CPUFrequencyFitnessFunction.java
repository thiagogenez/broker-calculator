package cs.scheduling.algorithms.statizz.cpu_frequency;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import net.sourceforge.jswarm_pso.FitnessFunction;

import com.google.common.primitives.Doubles;

import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.VM;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;

public class CPUFrequencyFitnessFunction extends FitnessFunction {

	private Environment environment;

	private double sumFrequency;

	private StaticAlgorithm staticAlgorithm;

	private List<VMType> vmTypes;

	private TreeMap<Double, List<Double>> results = new TreeMap<Double, List<Double>>();

	public CPUFrequencyFitnessFunction(Environment environment, double sumFrequency, StaticAlgorithm algorithm) {
		super(false); // Minimize
		this.environment = environment;
		this.vmTypes = environment.getAllVMTypes();
		this.sumFrequency = sumFrequency;
		this.staticAlgorithm = algorithm;
		this.results = new TreeMap<Double, List<Double>>();
	}

	@Override
	public double evaluate(double[] position) {
		double result = 0;

		CPUFrequencyPSO.round(position, 2);

		// check the sum of the positions values that means the sum of cpu
		// frequencies
		for (double value : position) {
			result += value;
		}

		// check if the sum of the frequencies is not greater than the maximum
		// if (result > sumFrequency){
		if (Math.abs(result - sumFrequency) >= 0.025) {
			return Double.MAX_VALUE;
		}

		// sort the cpu frequencies
		Arrays.sort(position);

		changeCpuFrequencies(vmTypes, position);

		// clear information of previous schedule
		environment.clearLeasedVMs();

		// run the scheduling algorithm
		staticAlgorithm.schedule();

		// simulates the execution and calculates the makespan
		double makespan = simulate();

		// save the data

		if (Math.abs(result - sumFrequency) <= 0.025)
			this.results.put(makespan, Doubles.asList(cloneDoubles(position)));

		// fitness value
		return makespan;
	}

	public static double[] cloneDoubles(double[] from) {
		return (double[]) from.clone();
	}

	private double simulate() {
		double makespan = 0;

		// simulate the schedule
		staticAlgorithm.execute();

		Map<String, List<VM>> vms = environment.getLeasedVMs();

		// check the log of each vm and the makespan
		for (List<VM> provider : vms.values()) {
			for (VM vm : provider) {
				if (vm.isUsed()) {
					makespan = Math.max(makespan, vm.getTerminateTime());
				}
			}
		}

		// return the makespan
		return makespan;
	}

	private void changeCpuFrequencies(List<VMType> vmTypes, double[] frequencies) {
		if (vmTypes.size() != frequencies.length) {
			System.out.println("erro");
		}

		int index = 0;
		for (VMType type : vmTypes) {
			type.setMips(frequencies[index++]);
		}
	}

	public TreeMap<Double, List<Double>> getResults() {
		return results;
	}

}
