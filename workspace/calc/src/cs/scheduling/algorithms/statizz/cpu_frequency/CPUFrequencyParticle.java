package cs.scheduling.algorithms.statizz.cpu_frequency;

import cs.scheduling.algorithms.statizz.plan.Plan;
import net.sourceforge.jswarm_pso.Particle;

public class CPUFrequencyParticle extends Particle {
	private Plan plan;

	public CPUFrequencyParticle() {
		super(CPUFrequencyPSO.DIMENSION);
	}

	@Override
	public void init(double[] maxPosition, double[] minPosition, double[] maxVelocity, double[] minVelocity) {
		super.init(maxPosition, minPosition, maxVelocity, minVelocity);
		// double[] pos = { 1.0, 1.0, 1.0 };
		// setPosition(pos);
	}

	public Plan getPlan() {
		return plan;
	}

	public void setPlan(Plan plan) {
		this.plan = plan;
	}
}
