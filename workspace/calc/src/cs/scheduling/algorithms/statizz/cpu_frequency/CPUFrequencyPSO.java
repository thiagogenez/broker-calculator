package cs.scheduling.algorithms.statizz.cpu_frequency;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.TreeMap;

import net.sourceforge.jswarm_pso.Swarm;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;

public class CPUFrequencyPSO {

	private static final int MAX_ITERATION = 200;

	private static final double MINIMAL_FREQUENCY = 0.5;

	private static final int NUMBER_OF_PARTICLES = Swarm.DEFAULT_NUMBER_OF_PARTICLES;

	static int DIMENSION = 0;

	private Environment environment;

	private StaticAlgorithm staticAlgorithm;

	public CPUFrequencyPSO(StaticAlgorithm algorithm, Environment environment) {
		this.environment = environment;
		this.staticAlgorithm = algorithm;
	}

	public TreeMap<Double, List<Double>> solve() {
		return run();
	}

	private TreeMap<Double, List<Double>> run() {
		int numberOfVms = environment.getAllVMTypes().size();

		// Set particle dimension as equal to the size of ready tasks
		DIMENSION = numberOfVms;

		if (DIMENSION == 0) {
			return null;
		}

		double sumFrequency = 0.0;
		double maxFrequency = 0.0;

		for (VMType vmType : environment.getAllVMTypes()) {
			sumFrequency += vmType.getMips();
		}
		maxFrequency = sumFrequency - (numberOfVms - 1) * MINIMAL_FREQUENCY;

		CPUFrequencyFitnessFunction fitnessFunction = new CPUFrequencyFitnessFunction(environment, sumFrequency,
				staticAlgorithm);

		CPUFrequencyParticle particle = new CPUFrequencyParticle();

		// Initialize the swarm
		Swarm swarm = new Swarm(NUMBER_OF_PARTICLES, particle, fitnessFunction);

		swarm.setMaxPosition(maxFrequency);
		swarm.setMinPosition(MINIMAL_FREQUENCY);

		for (int i = 0; i < MAX_ITERATION; i++) {
			swarm.evolve();
			// System.out.println(i + " : "+swarm.getBestFitness());
		}

		return fitnessFunction.getResults();

	}

	public static void round(double[] value, int places) {
		for (int i = 0; i < value.length; i++) {
			value[i] = round(value[i], places);
		}
	}

	public static double round(double value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd.doubleValue();
	}

}
