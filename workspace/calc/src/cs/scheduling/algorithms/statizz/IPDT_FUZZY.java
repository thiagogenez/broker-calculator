package cs.scheduling.algorithms.statizz;

import cs.core.dag.DAG;
import cs.core.engine.Environment;

public class IPDT_FUZZY extends IPDT_FULL_FUZZY {

	public IPDT_FUZZY(DAG dag, double deadline, Environment environment, double rho, double sigma, int nodeLim,
			double tiLim, int threads, double epgap) {
		super(dag, deadline, environment, rho, sigma, 0.0, 0.0, nodeLim, tiLim, threads, epgap);
	}

	public IPDT_FUZZY(DAG dag, double deadline, Environment environment, double rho, double sigma, int nodeLim,
			double tiLim) {
		super(dag, deadline, environment, rho, sigma, 0.0, 0.0, nodeLim, tiLim);
	}

	public IPDT_FUZZY(DAG dag, double deadline, Environment environment, double rho, double sigma) {
		super(dag, deadline, environment, rho, sigma, 0.0, 0.0);
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "IPDT-FUZZY";
	}

}
