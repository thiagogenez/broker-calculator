package cs.scheduling.algorithms.statizz;

import cs.core.dag.DAG;
import cs.core.engine.Environment;
import cs.scheduling.algorithms.statizz.ilp.noms.GenezAlgorithm;
import cs.scheduling.algorithms.statizz.ilp.noms.GenezILP;
import cs.scheduling.algorithms.statizz.ilp.noms.IPWS_ILP;

public class IPWS extends GenezAlgorithm {

	private int nodeLim;

	private double tiLim;

	private int threads;

	private double lambda;

	private double deadline;

	private double epgap;

	public IPWS(DAG dag, double deadline, Environment environment, int nodeLim, double tiLim, int threads,
			double lambda, double epgap) {
		super(dag, deadline, environment);
		this.tiLim = tiLim;
		this.nodeLim = nodeLim;
		this.threads = threads;
		this.lambda = lambda;
		this.deadline = deadline;
		this.epgap = epgap;
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "IPDT";
	}

	@Override
	public GenezILP createILP() {
		return new IPWS_ILP(getDAG(), getAllVMTypes(), getLogger(), this.nodeLim, this.tiLim, this.threads, this.epgap,
				this.deadline, this.lambda);
	}

}
