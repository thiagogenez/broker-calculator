package cs.scheduling.algorithms.statizz.heft.test;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.heft.HEFTLookAheadTaskDuplication;

public class HEFTLookAheadTaskDuplicationTest2Paper extends HEFTLookAheadTaskDuplication {

	// paper: ORIGINAL HEFT
	// Performance-Effective and Low-Complexity Task Scheduling for Heterogeneous
	// Computing

	public HEFTLookAheadTaskDuplicationTest2Paper(DAG dag, Environment environment) {
		super(dag, environment);
	}

	@Override
	public double getPreComputationCost(Task t) {

		switch (t.getId()) {
		case "ID001":
			return (19 + 19 + 19) / 3.0;

		case "ID002":
			return (28 + 46 + 20) / 3.0;

		case "ID003":
			return (36 + 34 + 30) / 3.0;

		case "ID004":
			return (15 + 25 + 37) / 3.0;

		case "ID005":
			return (30 + 8 + 8) / 3.0;

		case "ID006":
			return (33 + 35 + 59) / 3.0;

		case "ID007":
			return (12 + 20 + 21) / 3.0;

		case "ID008":
			return (13 + 22 + 24) / 3.0;

		case "ID009":
			return (41 + 68 + 73) / 3.0;

		}

		return -1;

	}

	@Override
	public void createPreCommunicationCosts() {
	}

	@Override
	public double getPreCommunicationCost(Task t1, Task t2) {

		if (t1.getId().contains("ID001") && t2.getId().contains("ID002")) {
			return 31.0;
		}

		else if (t1.getId().contains("ID001") && t2.getId().contains("ID003")) {
			return 89.0;
		}

		else if (t1.getId().contains("ID001") && t2.getId().contains("ID004")) {
			return 80.0;
		}

		else if (t1.getId().contains("ID001") && t2.getId().contains("ID005")) {
			return 17.0;
		}

		else if (t1.getId().contains("ID002") && t2.getId().contains("ID006")) {
			return 45.0;
		}

		else if (t1.getId().contains("ID002") && t2.getId().contains("ID007")) {
			return 59.0;
		}

		else if (t1.getId().contains("ID003") && t2.getId().contains("ID006")) {
			return 31.0;
		}

		else if (t1.getId().contains("ID003") && t2.getId().contains("ID008")) {
			return 14.0;
		}

		else if (t1.getId().contains("ID004") && t2.getId().contains("ID006")) {
			return 73.0;
		}

		else if (t1.getId().contains("ID005") && t2.getId().contains("ID007")) {
			return 41.0;
		}

		else if (t1.getId().contains("ID005") && t2.getId().contains("ID008")) {
			return 33.0;
		}

		else if (t1.getId().contains("ID006") && t2.getId().contains("ID009")) {
			return 65.0;
		}

		else if (t1.getId().contains("ID007") && t2.getId().contains("ID009")) {
			return 78.0;
		}

		else if (t1.getId().contains("ID008") && t2.getId().contains("ID009")) {
			return 7.0;
		}

		return -1;
	}

	@Override
	public double getRuntime(Task task, VMType vmType) {

		if (task.getId().contains("ID001")) {
			if (vmType.getName().contains("vm0"))
				return 19.0;
			else if (vmType.getName().contains("vm1"))
				return 19.0;
			else if (vmType.getName().contains("vm2"))
				return 19.0;
		}

		else if (task.getId().contains("ID002")) {
			if (vmType.getName().contains("vm0"))
				return 28.0;
			else if (vmType.getName().contains("vm1"))
				return 46.0;
			else if (vmType.getName().contains("vm2"))
				return 20.0;
		}

		else if (task.getId().contains("ID003")) {
			if (vmType.getName().contains("vm0"))
				return 36.0;
			else if (vmType.getName().contains("vm1"))
				return 30.0;
			else if (vmType.getName().contains("vm2"))
				return 34.0;
		}

		else if (task.getId().contains("ID004")) {
			if (vmType.getName().contains("vm0"))
				return 15.0;
			else if (vmType.getName().contains("vm1"))
				return 25.0;
			else if (vmType.getName().contains("vm2"))
				return 37.0;
		}

		else if (task.getId().contains("ID005")) {
			if (vmType.getName().contains("vm0"))
				return 30.0;
			else if (vmType.getName().contains("vm1"))
				return 8.0;
			else if (vmType.getName().contains("vm2"))
				return 8.0;
		}

		else if (task.getId().contains("ID006")) {
			if (vmType.getName().contains("vm0"))
				return 33.0;
			else if (vmType.getName().contains("vm1"))
				return 35.0;
			else if (vmType.getName().contains("vm2"))
				return 59.0;
		}

		else if (task.getId().contains("ID007")) {
			if (vmType.getName().contains("vm0"))
				return 12.0;
			else if (vmType.getName().contains("vm1"))
				return 20.0;
			else if (vmType.getName().contains("vm2"))
				return 21.0;
		}

		else if (task.getId().contains("ID008")) {
			if (vmType.getName().contains("vm0"))
				return 13.0;
			else if (vmType.getName().contains("vm1"))
				return 22.0;
			else if (vmType.getName().contains("vm2"))
				return 24.0;
		}

		else if (task.getId().contains("ID009")) {
			if (vmType.getName().contains("vm0"))
				return 41.0;
			else if (vmType.getName().contains("vm1"))
				return 68.0;
			else if (vmType.getName().contains("vm2"))
				return 73.0;
		}

		return -1;
	}


	@Override
	public double getTransferTime(VMType to, Task source, Task sink) {
		return getPreCommunicationCost(source, sink);
	}

	@Override
	protected void onInternalAfterSchedulingCompleted() {
		logln(">>> " + getSchedulingAlgorithmName() + ";Makespan;" + getMakespan());
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "HEFTLookAheadTaskDuplicationTest2Paper";
	}

}
