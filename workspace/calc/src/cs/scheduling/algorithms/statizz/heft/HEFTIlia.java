package cs.scheduling.algorithms.statizz.heft;

import java.util.LinkedList;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

/*
 * Paper: Scheduling Data-Intensive Scientific Workflows with Reduced Communication
 */
public class HEFTIlia extends HEFT {

	private double w = 0.0;

	public HEFTIlia(DAG dag, Environment environment) {
		super(dag, environment);

		this.w = 1.0;
	}

	public HEFTIlia(DAG dag, Environment environment, double W) {
		super(dag, environment);

		// sanity check
		if (W < 0 || W > 1) {
			throw new RuntimeException("W value should be 0 < W <= 1");
		}

		this.w = W;
	}

	@Override
	protected void onInternalBeforeSchedulingStart() {

		// skewed bandwidth
		Environment.setReductorValue(1.0 - this.w);

		super.onInternalBeforeSchedulingStart();
	}

	@Override
	protected Plan planDAG(Plan currentPlan) {
		Plan original = super.planDAG(currentPlan);
		
		if(this.w == 1.0) {
			return original;
		}

		// System.out.println(original.logSchedules());

		// normal bandwidth
		Environment.setReductorValue(0.0);

		// create a new plan
		Plan recomputationPlan = PlanFactory.getEmptyPlan(original);

		// set as the current plan
		setCurrentPlan(recomputationPlan);

		// get the priority list
		LinkedList<Task> list = prioritisingPhase();

		double EST = 0.0, runtime = 0.0, EFT = 0.0;

		// re-calculate the EST for each task
		for (Task task : list) {
			ScheduleEstimation schedule = original.getScheduleEstimation(task);
			VMType vmType = schedule.getVmType();

			// check the current EST
			EST = getEST(task, vmType);

			// calculate runtime
			runtime = getRuntime(task, vmType);

			// check EST according to the insertion policy in this resource
			EST = insertionBasedSchedulingPolicy(EST, runtime, vmType);

			// calculate the EFT in this resource
			EFT = EST + runtime;

			// create the schedule
			ScheduleEstimation newSchedule = new ScheduleEstimation(EST, runtime, EFT, vmType);

			// store the schedule
			recomputationPlan.addScheduleEstimation(task, newSchedule);
		}

		// return the new plan
		return recomputationPlan;
	}

	
	@Override
	public String getSchedulingAlgorithmName() {
		return "HEFT-Ilia";
	}
}
