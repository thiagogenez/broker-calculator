package cs.scheduling.algorithms.statizz.heft;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.SortedMap;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

public class HEFTaskDuplication2 extends HEFT {

	private LinkedHashMap<String, LinkedHashMap<String, Task>> duplicates;

	private int rescheduled = 0;

	public HEFTaskDuplication2(DAG dag, Environment environment) {
		super(dag, environment);

		this.duplicates = new LinkedHashMap<String, LinkedHashMap<String, Task>>();
		this.rescheduled = 0;
	}

	@Override
	public ScheduleEstimation resourceSelectionPhase(Task t) {

		// save the current plan before the schedule
		Plan currentPlan = getCurrentPlan();

		// get the resource that minimises the earliest finish time of the task
		ScheduleEstimation bestSchedule = null;

		// *******************
		// CASE 1: Use HEFT //
		// *******************
		bestSchedule = super.resourceSelectionPhase(t);

		if (t.getParents().isEmpty()) {
			return bestSchedule;
		}

		// ****************************************************************************
		// Try to improve LookAhead with TaskDuplication
		// ****************************************************************************

		// temp variables
		double minEFT = Double.MAX_VALUE;
		double EST = 0.0, runtime = 0.0, EFT = 0.0;

		// get task's critical parent
		Task criticalParent = getCriticalParent(t);

		// create a clone of the critical parent
		Task newCloneCriticalParent = createClone(criticalParent);

		// save the best critical parent
		Task bestCriticalParent = criticalParent;

		// save the schedule of the best critical parent
		ScheduleEstimation bestCriticalParentSchedule = null;

		// CASE
		// - 1: DEFAULT (HEFT)
		// - 2: Critical Parent is a clone already created
		// - 3: Critical Parent is a new clone
		short CASE = 1;

		// sanity check
		if (criticalParent != null && criticalParent.isClone()) {
			throw new RuntimeException("A critical parent should not be a copy at this point");
		}

		// minEFT is initialised with the HEFT one
		minEFT = bestSchedule.getFinishExecutionTime();

		// List of VMs where critical parent has been tested already
		List<VMType> alreadyTried = new ArrayList<>();
		alreadyTried.add(getScheduleEstimation(criticalParent).getVmType());

		// *******************************************
		// CASE 2: check the clones already created //
		// *******************************************
		if (this.duplicates.containsKey(criticalParent.getId())) {
			for (Task existingCriticalParentClone : this.duplicates.get(criticalParent.getId()).values()) {

				alreadyTried.add(getScheduleEstimation(existingCriticalParentClone).getVmType());

				// create a clone of the current plan
				Plan clonedPlan = PlanFactory.clone(currentPlan);

				// set the clonedPlan as the current plan
				setCurrentPlan(clonedPlan);

				// swap parents temporarily
				swapParents(criticalParent, existingCriticalParentClone, t);

				// scheduling again using HEFT
				ScheduleEstimation schedule = super.resourceSelectionPhase(t);

				EFT = schedule.getFinishExecutionTime();

				// tie-break condition in case EFT == minEFT
				boolean tieBreak = false;
				if ((EFT == minEFT) && (getScheduleEstimation(existingCriticalParentClone).getVmType()
						.equals(schedule.getVmType()))) {
					tieBreak = true;
				}

				// better schedule?
				if ((EFT < minEFT) || tieBreak) {

					// SAVING data
					minEFT = EFT;
					bestSchedule = schedule;
					bestCriticalParent = existingCriticalParentClone;

					// setting the case
					CASE = 2;
				}

				// return original parent
				swapParents(existingCriticalParentClone, criticalParent, t);

			}
		}

		// *******************************************
		// CASE 3: trying create new clones //
		// *******************************************

		for (VMType vmType : getAllVMTypes()) {

			// HEFT already tried here
			if (alreadyTried.contains(vmType)) {
				continue;
			}

			// create a clone of the current plan
			Plan clonedPlan = PlanFactory.clone(currentPlan);

			// set the clonedPlan as the current plan
			setCurrentPlan(clonedPlan);

			// open a scenario to duplicate the CriticalParent into this candidate resource
			EST = getEST(criticalParent, vmType);
			runtime = getRuntime(criticalParent, vmType);

			// check EST according to the insertion policy in this resource
			EST = insertionBasedSchedulingPolicy(EST, runtime, vmType);

			// schedule the current task on the current resource
			ScheduleEstimation scheduleClone = new ScheduleEstimation(EST, runtime, (EST + runtime), vmType);
			addScheduleEstimation(newCloneCriticalParent, scheduleClone);

			// swap parents temporarily
			swapParents(criticalParent, newCloneCriticalParent, t);

			// scheduling again using HEFT
			ScheduleEstimation schedule = super.resourceSelectionPhase(t);

			EFT = schedule.getFinishExecutionTime();

			// tie-break condition in case EFT == minEFT
			boolean tieBreak = false;
			if ((EFT == minEFT)) {

				// no tie-break decision, no advantage in creating a new clone here (considering
				// the context of anticipating the task execution)
				tieBreak = false;
			}

			// better schedule?
			if ((EFT < minEFT) || tieBreak) {

				// saving data
				minEFT = EFT;
				bestSchedule = schedule;
				bestCriticalParentSchedule = scheduleClone;
				bestCriticalParent = newCloneCriticalParent;

				// setting the case
				CASE = 3;
			}

			// return original parent
			swapParents(newCloneCriticalParent, criticalParent, t);

		}

		// *******************************************
		// FINAL ANAYLISE
		// *******************************************

		// return the original plan
		setCurrentPlan(currentPlan);

		switch (CASE) {
		case 1:
			deleteClone(newCloneCriticalParent);
			break; // go ahead
		case 2:

			// delete clone
			deleteClone(newCloneCriticalParent);

			// new t's critical parent (which is a clone one)
			swapParents(criticalParent, bestCriticalParent, t);

			// check any grandparent of the clone task
			checkGrandParents(criticalParent, bestCriticalParent);

			break;

		case 3:
			// new t's critical parent (which is a clone one)
			swapParents(criticalParent, bestCriticalParent, t);

			// schedule the clone
			addScheduleEstimation(bestCriticalParent, bestCriticalParentSchedule);

			// check any grandparent of the clone task
			checkGrandParents(criticalParent, bestCriticalParent);

			// add into the duplicate list
			if (!this.duplicates.containsKey(criticalParent.getId())) {
				this.duplicates.put(criticalParent.getId(), new LinkedHashMap<String, Task>());
			}
			this.duplicates.get(criticalParent.getId()).put(bestCriticalParent.getId(), bestCriticalParent);

		default:
			break;
		}

		// if the original criticalParent become with no children, then removed
		// the remove of a task is not allowed in the original HEFT
		// because once a task is scheduled, it will not removed or re-scheduled
		List<Task> removed = new ArrayList<Task>();
		checkConsistency(criticalParent, removed);

		this.rescheduled += removed.size();

		if (!removed.isEmpty()) {

			// create a new plan without the "removed tasks"
			// since PLAN does not allowed REMOVE ScheduleEstimation object only ADD it
			Plan newPlan = PlanFactory.getEmptyPlan(currentPlan);
			SortedMap<Task, ScheduleEstimation> schedules = currentPlan.getSchedules(criticalParent.getDAG());

			for (Task task : schedules.keySet()) {
				if (!removed.contains(task)) {
					ScheduleEstimation schedule = schedules.get(task);

					newPlan.addScheduleEstimation(task, schedule);
				}
			}

			setCurrentPlan(newPlan);
		}

		return bestSchedule;

	}

	private void checkConsistency(Task task, List<Task> removed) {

		if (task.getChildren().size() == 0) {

			DAG dag = task.getDAG();

			List<Task> edgesToRemove = new ArrayList<Task>(task.getParents());

			// remove edges from the parents
			for (Task parent : edgesToRemove) {
				// remove edge
				dag.removeEdge(parent.getId(), task.getId());
			}

			System.out.println("REMOVING=" + task);
			// remove task
			dag.removeTask(task);

			if (task.isClone()) {

				String id = task.getOriginalId();

				// sanity check
				if (this.duplicates.get(id).get(task.getId()) == null) {
					throw new RuntimeException("Clone is not there");
				}

				// remove the clone
				this.duplicates.get(id).remove(task.getId());

				// check if this is the no clone anymore
				if (this.duplicates.get(id).size() == 1) {

					// remove the duplicate information
					this.duplicates.remove(id);
				}
			}

			// pick a clone to be the original
			else {
				String id = task.getId();

				// sanity check
				if (this.duplicates.get(id) == null) {
					throw new RuntimeException("Must be a clone to become the original");
				}

				LinkedHashMap<String, Task> clones = this.duplicates.get(id);

				// sanity check
				if (clones.isEmpty()) {
					throw new RuntimeException("Must be a clone to become the original");
				}

				// get one clone to be the original
				Task newOriginal = clones.values().iterator().next();

				// remove the clone of the list
				clones.remove(newOriginal.getId());

				// check if there is more clones
				if (clones.isEmpty()) {

					// remove the information
					this.duplicates.remove(id);
				}

				// clone becomes the original
				task.cloneBecomingOriginal(newOriginal, clones);
			}

			// check consistency with the parents
			for (Task parent : edgesToRemove) {
				checkConsistency(parent, removed);
			}

			removed.add(task);
		}

	}

	private Task getCriticalParent(Task task) {

		Task critical = null;
		double maxRank = Double.MIN_NORMAL;

		for (Task parent : task.getParents()) {

			if (parent.getUpwardRank() > maxRank) {
				maxRank = parent.getUpwardRank();
				critical = parent;
			}
		}

		return critical;
	}

	private Task createClone(Task task) {

		// sanity check
		if (task.isClone()) {
			throw new RuntimeException(
					"A clone cannot duplicate itself since by default if a task is a clone, then it must be scheduled already");
		}

		Task clone = task.clone();

		// not sure about bellow
		clone.setDownwardRank(task.getDownwardRank());
		clone.setUpwardRank(task.getUpwardRank());

		// add new task to the DAG
		DAG dag = task.getDAG();
		dag.addTask(clone);

		return clone;
	}

	private void deleteClone(Task clone) {
		DAG dag = clone.getDAG();
		dag.removeTask(clone);
	}

	private void swapParents(Task original, Task duplicata, Task child) {

		// Sanity checks
		if (original.getDAG() != duplicata.getDAG()) {
			throw new RuntimeException("Different DAGs: oldParent.getDAG() != newParent.getDAG() ");
		}

		if (original.getDAG() != child.getDAG()) {
			throw new RuntimeException("Different DAGs: oldParent.getDAG() != child.getDAG() ");
		}

		DAG dag = original.getDAG();

		// remove old edge
		dag.removeEdge(original.getId(), child.getId());

		// add new edge
		dag.addEdge(duplicata.getId(), child.getId());

	}

	private void addGrandParent(Task grandParent, Task parent) {

		// sanity check
		if (grandParent.getDAG() != parent.getDAG()) {
			throw new RuntimeException("grandParent.getDAG() != parent.getDAG() ");
		}

		DAG dag = grandParent.getDAG();

		// add new edge
		dag.addEdge(grandParent.getId(), parent.getId());
	}

	private void checkGrandParents(Task criticalParent, Task cloneCriticalParent) {

		// sanity check
		if (criticalParent.isClone()) {
			throw new RuntimeException("A critical parent should not be a copy at this point");
		}

		// check if there is any clone of a parent in the same machine
		for (Task grandParent : criticalParent.getParents()) {

			String id = grandParent.getId();

			if (grandParent.isClone()) {
				id = grandParent.getOriginalId();
			}

			// by default, criticalParent's grandParent is set to be
			// cloneCriticalParent's grandParent too
			addGrandParent(grandParent, cloneCriticalParent);

			// if the cloneCriticalParent's grandparent has a clone,
			// then check if this clone is scheduled in the same
			// machine where cloneCriticalParent is scheduled
			if (this.duplicates.containsKey(id)) {

				// get the clones
				for (Task cloneGrandParent : this.duplicates.get(id).values()) {

					// get the schedule
					ScheduleEstimation schedule = getScheduleEstimation(cloneGrandParent);

					// if we have a match!
					if (schedule.getVmType().equals(getScheduleEstimation(cloneCriticalParent).getVmType())) {

						// swap grandParent
						swapParents(grandParent, cloneGrandParent, cloneCriticalParent);
					}
				}
			}
		}
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "HEFT-TaskDuplication";
	}

	public int getTotalDuplicatas() {
		int duplicatas = 0;

		for (LinkedHashMap<String, Task> clones : this.duplicates.values()) {
			duplicatas += clones.size();
		}

		return duplicatas;
	}

	public int getRescheduled() {
		return rescheduled;
	}

}
