package cs.scheduling.algorithms.statizz.heft.test;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.heft.HEFTIlia;

public class HEFTIliaTest1 extends HEFTIlia {

	public HEFTIliaTest1(DAG dag, Environment environment) {
		super(dag, environment);
	}

	
	@Override
	public double getPreComputationCost(Task t) {

		switch (t.getId()) {
		case "ID000":
			return 1300.0;

		case "ID001":
			return 900.0;

		case "ID002":
			return 1300.0;

		case "ID003":
			return 700.0;

		case "ID004":
			return 900.0;

		case "ID005":
			return 600.0;

		case "ID006":
			return 300.0;

		case "ID007":
			return 1100.0;

		case "ID008":
			return 1000.0;

		}

		return -1;

	}
	
	
	@Override
	public void createPreCommunicationCosts() {
	}

	@Override
	public double getPreCommunicationCost(Task t1, Task t2) {

		if (t1.getId().contains("ID000") && t2.getId().contains("ID001")) {
			return 118.11*8.0;
		}

		else if (t1.getId().contains("ID000") && t2.getId().contains("ID002")) {
			return 85.9*8.0;
		}

		else if (t1.getId().contains("ID000") && t2.getId().contains("ID003")) {
			return 96.64*8.0;
		}

		else if (t1.getId().contains("ID000") && t2.getId().contains("ID004")) {
			return 21.48*8.0;
		}
		
		else if (t1.getId().contains("ID001") && t2.getId().contains("ID005")) {
			return 3.22*8.0;
		}
		
		else if (t1.getId().contains("ID001") && t2.getId().contains("ID007")) {
			return 64.43*8.0;
		}
		
		else if (t1.getId().contains("ID002") && t2.getId().contains("ID006")) {
			return 118.11*8.0;
		}
		
		else if (t1.getId().contains("ID003") && t2.getId().contains("ID006")) {
			return 32.21*8.0;
		}
		
		else if (t1.getId().contains("ID003") && t2.getId().contains("ID007")) {
			return 75.16*8.0;
		}

		else if (t1.getId().contains("ID004") && t2.getId().contains("ID007")) {
			return 42.95*8.0;
		}
		
		else if (t1.getId().contains("ID005") && t2.getId().contains("ID008")) {
			return 96.64*8.0;
		}
		
		else if (t1.getId().contains("ID006") && t2.getId().contains("ID008")) {
			return 182.54*8.0;
		}
		
		else if (t1.getId().contains("ID007") && t2.getId().contains("ID008")) {
			return 64.43*8.0;
		}

		return -1;
	}
	
	
	
	@Override
	public double getRuntime(Task task, VMType vmType) {
		return getPreComputationCost(task);
	}

	@Override
	public double getTransferTime(VMType to, Task source, Task sink) {
		return getPreCommunicationCost(source, sink) / (0.1 * 1.0);
	}
	
}
