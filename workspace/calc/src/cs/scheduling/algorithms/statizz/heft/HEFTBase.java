package cs.scheduling.algorithms.statizz.heft;

import java.util.Arrays;
import java.util.LinkedList;

import cs.core.dag.DAG;
import cs.core.dag.DAGFile;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.job.Job;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.Transmission;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

public abstract class HEFTBase extends StaticAlgorithm {

	private Plan plan;

	public HEFTBase(DAG dag, double deadline, Environment environment) {
		super(dag, deadline, environment);
	}

	public abstract void createPreComputationCosts();

	public abstract double getPreComputationCost(Task t);

	public abstract void createPreCommunicationCosts();

	public abstract double getPreCommunicationCost(Task t1, Task t2);

	public abstract ScheduleEstimation resourceSelectionPhase(Task task);

	public void preHeftProcedure() {

		// calculating average computation cost
		createPreComputationCosts();

		// calculating average communication cost
		createPreCommunicationCosts();

		// upward rank policy
		calculateUpwardRanks();

		// downward rank policy
		calculateDownwardRank();
	}

	/**
	 * Invoke the calculateRank procedure for each entry task of the DAG
	 */
	public void calculateUpwardRanks() {
		for (String entry : getDAG().getEntryTasks()) {
			Task task = getDAG().getTaskById(entry);
			upwardRank(task);
		}
	}

	public void calculateDownwardRank() {
		for (String exit : getDAG().getExitTasks()) {
			Task task = getDAG().getTaskById(exit);
			downwardRank(task);
		}
	}

	/**
	 * Populates rank.get(task) with the rank of task as defined in the HEFT paper.
	 *
	 * @param task
	 *            The task have the rank calculates
	 * @return The rank
	 */
	private double upwardRank(Task task) {

		// if the upward rank value has been calculate, return it
		if (task.getUpwardRank() >= 0) {
			return task.getUpwardRank();
		}

		// the average computation cost
		double w = getPreComputationCost(task);

		// max (child's upward rank values + communication demands)
		double max = 0.0;

		for (Task child : task.getChildren()) {

			// the average communication cost of the edge
			double communicationCost = getPreCommunicationCost(task, child);

			// calculate child's upward rank value
			double childUpwardRank = upwardRank(child);

			// get max value
			max = Math.max(max, (communicationCost + childUpwardRank));
		}

		// storing the upward rank value of the current task
		task.setUpwardRank(w + max);

		return (w + max);
	}

	private double downwardRank(Task task) {

		// if the downward rank value has been calculate, return it
		if (task.getDownwardRank() >= 0) {
			return task.getDownwardRank();
		}

		// max (child's downward rank values + communication demands)
		double max = 0.0;

		for (Task parent : task.getParents()) {

			// calculate parent's downward rank value
			double parentDownwardRank = downwardRank(parent);

			// average parent's computation cost
			double w = getPreComputationCost(parent);

			// the average communication cost of the edge
			double communicationCost = getPreCommunicationCost(parent, task);

			// get max value
			max = Math.max(max, w + communicationCost + parentDownwardRank);
		}

		// storing the downward rank value of the current task
		task.setDownwardRank(max);

		return max;

	}

	@Override
	protected void onInternalBeforeSchedulingStart() {

		// calculate pre data
		preHeftProcedure();

		// logging purposes
		printRanks();
		printPrioritiseList();
	}

	public LinkedList<Task> prioritisingPhase() {
		// a set of unscheduled tasks
		// Sorting in non-ascending order of rank
		LinkedList<Task> unscheduledTasks = new LinkedList<Task>();

		String ranked[] = getDAG().getTasksOrdedByUpwardRank();
		// add task in the priority queue
		for (String taskName : ranked) {
			Task task = getDAG().getTaskById(taskName);
			unscheduledTasks.add(task);
		}

		return unscheduledTasks;
	}

	private void printPrioritiseList() {

		LinkedList<Task> list = prioritisingPhase();
		StringBuffer str = new StringBuffer();

		while (list.size() > 0) {
			Task t = list.poll();
			str.append(t.getId());
			if (list.size() > 0)
				str.append(";");
		}

		logln("PrioritiseList: " + str.toString());

	}

	@Override
	protected Plan planDAG(Plan currentPlan) {

		// set current plan
		setCurrentPlan(currentPlan);

		// Tasking Prioritising Phase
		LinkedList<Task> unscheduledTasks = prioritisingPhase();

		String[] exitTasksBefore = getDAG().getExitTasks();

		// Processor Selection Phase
		while (unscheduledTasks.size() > 0) {
			Task unscheduled = unscheduledTasks.poll();
			ScheduleEstimation schedule = resourceSelectionPhase(unscheduled);

			this.plan.addScheduleEstimation(unscheduled, schedule);

		}

		String[] exitTasksAfter = getDAG().getExitTasks();

		if (!Arrays.equals(exitTasksBefore, exitTasksAfter)) {
			System.err.println("before: " + Arrays.toString(exitTasksBefore));
			System.err.println("after: " + Arrays.toString(exitTasksAfter));
			throw new RuntimeException("DAG Inconsistency");
		}

		// HEFT always returns a schedule
		return this.plan;
	}

	private void printRanks() {
		logln("TASK\t\t\t\trankU\trankD\t(rankU + rankD)");

		for (String taskName : getDAG().getTasks()) {
			Task task = getDAG().getTaskById(taskName);

			double rankU = task.getUpwardRank();
			double rankD = task.getDownwardRank();

			logln(task + "\t" + String.format("%.03f", rankU) + "\t" + String.format("%.03f", rankD) + "\t"
					+ String.format("%.03f", (rankU + rankD)));
		}

	}

	/*
	 * Get Earliest start time
	 */
	public double getEST(Task unscheduledTask, VMType candidateResource) {

		double EST = 0.0;

		for (Task parent : unscheduledTask.getParents()) {

			// sanity check
			if (!isScheduled(parent)) {

				// The lookahead version raises some issues that need to be tackled. Let t be
				// the highest priority task ready to be scheduled. Task t is tried on every
				// resource and its children are scheduled using HEFT to calculate their
				// estimated finish time according to where t is scheduled. However, some (or
				// all) children of t may not become ready immediately after scheduling t (as a
				// result of a dependence to another, as yet unscheduled, parent). Still, to
				// calculate the estimated finish time of t's children, we only consider their
				// already scheduled parents and t, ignoring further delays that may arise due
				// to any unscheduled parents. This means that the estimated finish time
				// computed is rather optimistic.

				// throw new RuntimeException("attempting scheduling a task who parent has not
				// been scheduled yet!");
				// System.out.println("attempting scheduling a task who parent has not been
				// scheduled yet!");

				continue;
			}

			// get parent's schedule information
			ScheduleEstimation schedule = this.plan.getScheduleEstimation(parent);

			// parent's actual finish time AFT
			double AFT = schedule.getFinishExecutionTime();

			// add any transfer time into AFT
			if (schedule.getVmType() != candidateResource) {
				AFT += getTransferTime(candidateResource, parent, unscheduledTask);
			}

			// get Max EST for the current task
			EST = Math.max(EST, AFT);
		}

		return EST;
	}

	public double getTransferTime(VMType candidateResource, Task scheduledParent, Task unscheduledChild) {

		return this.plan.getTransferTime(candidateResource, scheduledParent, unscheduledChild);
	}

	/*
	 * Calculate the transfer time between a parent task already scheduled and a
	 * child not-yet-scheduled
	 */

	public double insertionBasedSchedulingPolicy(double readyTime, double runtime, VMType vmType) {
		return this.plan.getEarliestIdleTimeFrom(readyTime, runtime, vmType);
	}

	public double getRuntime(Task task, VMType vmType) {
		return Environment.getPredictedRuntime(task, vmType);
	}

	public double getMakespan() {
		return this.plan.getMakespan(getDAG());
	}

	@Override
	public void jobReleased(Job job) {
		Task task = job.getTask();
		job.setVM(getVM(task));
	}

	@Override
	public void jobSubmitted(Job job) {
		// do nothing
	}

	@Override
	public void jobStarted(Job job) {
		// do nothing
	}

	@Override
	public void jobFinished(Job job) {
		Task from = job.getTask();

		for (Task child : from.getChildren()) {
			for (DAGFile parentFile : from.getOutputFiles()) {
				for (DAGFile childFile : child.getInputFiles()) {

					if (parentFile.equals(childFile)) {
						Transmission t = new Transmission(child, parentFile);
						job.addTransmission(t.getReceiver(), t);
					}
				}
			}
		}
	}

	@Override
	protected void onInternalAfterSchedulingCompleted() {
		logln(">>> " + getSchedulingAlgorithmName() + ";Makespan=" + getMakespan() + ";dataReceived="
				+ getScheduledTotalBytesReceived() + ";dataSent=" + getScheduledTotalBytesSent());
	}

	protected boolean isScheduled(Task task) {
		return this.plan.isScheduled(task);
	}

	protected ScheduleEstimation getScheduleEstimation(Task task) {

		if (task == null) {
			return null;
		}

		return this.plan.getScheduleEstimation(task);
	}

	protected void addScheduleEstimation(Task task, ScheduleEstimation schedule) {
		this.plan.addScheduleEstimation(task, schedule);
	}

	protected void setCurrentPlan(Plan plan) {
		this.plan = plan;
	}

	protected Plan getCurrentPlan() {
		return this.plan;
	}
}
