package cs.scheduling.algorithms.statizz.heft;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

public class HEFTLookAhead extends HEFT {

	// maximum EFT calculated for children of the current task being scheduled
	// According to the paper: DAG Scheduling Using a Lookahead Variant of the
	// Heterogeneous Earliest Finish Time Algorithm
	private HashMap<Task, Double> childrenEFT;

	public HEFTLookAhead(DAG dag, Environment environment) {
		super(dag, environment);

		childrenEFT = new HashMap<>();
	}

	@Override
	public ScheduleEstimation resourceSelectionPhase(Task t) {

		// get the resource that minimizes the earliest finish time of the task
		VMType selectedResource = null;

		// temp variables
		double minEFT = Double.MAX_VALUE, minRuntime = Double.MAX_VALUE;
		double EST = 0.0, runtime = 0.0, maxEFT = 0.0;

		// save the current plan
		Plan currentPlan = getCurrentPlan();

		// creating the set L
		List<Task> L = t.getChildren();
		Collections.sort(L, Collections.reverseOrder(Task.COMPARE_BY_UPWARD_RANK));

		for (VMType vmType : getAllVMTypes()) {

			// create a clone of the current plan
			Plan clonedPlan = PlanFactory.clone(currentPlan);

			// set the clonedPlan as the current plan
			setCurrentPlan(clonedPlan);

			// scheduling the current task in this resource
			EST = getEST(t, vmType);

			// calculate runtime
			runtime = getRuntime(t, vmType);

			// check EST according to the insertion policy in this resource
			EST = insertionBasedSchedulingPolicy(EST, runtime, vmType);

			// schedule the current task on the current resource
			ScheduleEstimation currentTaskSchedule = new ScheduleEstimation(EST, runtime, (EST + runtime), vmType);
			addScheduleEstimation(t, currentTaskSchedule);

			// look ahead condition
			maxEFT = calculateCondition(currentTaskSchedule, L);

			// Select the resource which minimises the maximum EFT for t's children among
			// all resources where t is tried. In other words, this means that t will be
			// scheduled on the resource which gives the minimum completion time for all t's
			// children, which were scheduled using HEFT
			
			
			// tie-break condition in case EFT == minEFT
			// The resource that provides lowest EFT for the current task
			boolean tieBreak = false;
			if ((maxEFT == minEFT) && (runtime < minRuntime)) {
				tieBreak = true;
			}
			
			// better schedule?
			if ((maxEFT < minEFT) || 
					tieBreak) {
				selectedResource = vmType;
				minEFT = maxEFT;
				minRuntime = runtime;
			}

		}

		// return the original plan
		setCurrentPlan(currentPlan);

		// calculate EST and runtime values for t on the selected resource
		EST = getEST(t, selectedResource);
		runtime = getRuntime(t, selectedResource);

		// check EST according to the insertion policy in this resource
		EST = insertionBasedSchedulingPolicy(EST, runtime, selectedResource);

		// create a schedule
		ScheduleEstimation estimation = new ScheduleEstimation(EST, runtime, (EST + runtime), selectedResource);

		// save the maximum EFT for t's children calculated that ending up for this
		// current schedule
		childrenEFT.put(t, minEFT);

		// return the schedule
		return estimation;

	}

	public double calculateCondition(ScheduleEstimation currentTaskSchedule, List<Task> L) {

		// When the set L is empty, t is scheduled on the resource which gives the best
		// EFT for itself, so maxEFT it at least t's EFT in the current resource
		double maxEFT = currentTaskSchedule.getFinishExecutionTime();

		for (Task child : L) {

			// THIS CALL MUST BE FROM HEFT
			ScheduleEstimation childSchedule = super.resourceSelectionPhase(child);
			addScheduleEstimation(child, childSchedule);

			maxEFT = Double.max(maxEFT, childSchedule.getFinishExecutionTime());
		}

		return maxEFT;
	}

	public double getMinChildrenEFT(Task parent) {

		if (!this.childrenEFT.containsKey(parent)) {
			throw new RuntimeException("Parent not scheduled yet");
		}

		return this.childrenEFT.get(parent);
	}

}
