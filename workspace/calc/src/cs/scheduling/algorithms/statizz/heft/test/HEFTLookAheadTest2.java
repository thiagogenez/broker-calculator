package cs.scheduling.algorithms.statizz.heft.test;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.heft.HEFTLookAhead;

public class HEFTLookAheadTest2 extends HEFTLookAhead {

	// USING paper: DAG Scheduling Using a Lookahead Variant of the Heterogeneous
	// Earliest Finish Time Algorithm

	public HEFTLookAheadTest2(DAG dag, Environment environment) {
		super(dag, environment);
	}

	
	@Override
	public double getPreComputationCost(Task t) {

		switch (t.getId()) {
		case "ID001":
			return (14 + 16 + 9) / 3.0;

		case "ID002":
			return (13 + 19 + 18) / 3.0;

		case "ID003":
			return (11 + 13 + 19) / 3.0;

		case "ID004":
			return (13 + 8 + 17) / 3.0;

		case "ID005":
			return (12 + 13 + 10) / 3.0;

		case "ID006":
			return (13 + 16 + 9) / 3.0;

		case "ID007":
			return (7 + 15 + 11) / 3.0;

		case "ID008":
			return (5 + 11 + 14) / 3.0;

		case "ID009":
			return (18 + 12 + 20) / 3.0;

		case "ID010":
			return (21 + 7 + 16) / 3.0;

		}

		return -1;

	}

	@Override
	public void createPreCommunicationCosts() {
	}

	@Override
	public double getPreCommunicationCost(Task t1, Task t2) {

		if (t1.getId().contains("ID001") && t2.getId().contains("ID002")) {
			return 18.0;
		}

		else if (t1.getId().contains("ID001") && t2.getId().contains("ID003")) {
			return 12.0;
		}

		else if (t1.getId().contains("ID001") && t2.getId().contains("ID004")) {
			return 9.0;
		}

		else if (t1.getId().contains("ID001") && t2.getId().contains("ID005")) {
			return 11.0;
		}

		else if (t1.getId().contains("ID001") && t2.getId().contains("ID006")) {
			return 14.0;
		}

		else if (t1.getId().contains("ID002") && t2.getId().contains("ID008")) {
			return 19.0;
		}

		else if (t1.getId().contains("ID002") && t2.getId().contains("ID009")) {
			return 16.0;
		}

		else if (t1.getId().contains("ID003") && t2.getId().contains("ID007")) {
			return 23.0;
		}

		else if (t1.getId().contains("ID004") && t2.getId().contains("ID008")) {
			return 27.0;
		}

		else if (t1.getId().contains("ID004") && t2.getId().contains("ID009")) {
			return 23.0;
		}

		else if (t1.getId().contains("ID005") && t2.getId().contains("ID009")) {
			return 13.0;
		}

		else if (t1.getId().contains("ID006") && t2.getId().contains("ID008")) {
			return 15.0;
		}

		else if (t1.getId().contains("ID007") && t2.getId().contains("ID010")) {
			return 17.0;
		}

		else if (t1.getId().contains("ID008") && t2.getId().contains("ID010")) {
			return 11.0;
		}

		else if (t1.getId().contains("ID009") && t2.getId().contains("ID010")) {
			return 13.0;
		}

		return -1;
	}

	@Override
	public double getRuntime(Task task, VMType vmType) {

		if (task.getId().contains("ID001")) {
			if (vmType.getName().contains("vm0"))
				return 14.0;
			else if (vmType.getName().contains("vm1"))
				return 16.0;
			else if (vmType.getName().contains("vm2"))
				return 9.0;
		}

		else if (task.getId().contains("ID002")) {
			if (vmType.getName().contains("vm0"))
				return 13.0;
			else if (vmType.getName().contains("vm1"))
				return 19.0;
			else if (vmType.getName().contains("vm2"))
				return 18.0;
		}

		else if (task.getId().contains("ID003")) {
			if (vmType.getName().contains("vm0"))
				return 11.0;
			else if (vmType.getName().contains("vm1"))
				return 13.0;
			else if (vmType.getName().contains("vm2"))
				return 19.0;
		}

		else if (task.getId().contains("ID004")) {
			if (vmType.getName().contains("vm0"))
				return 13.0;
			else if (vmType.getName().contains("vm1"))
				return 8.0;
			else if (vmType.getName().contains("vm2"))
				return 17.0;
		}

		else if (task.getId().contains("ID005")) {
			if (vmType.getName().contains("vm0"))
				return 12.0;
			else if (vmType.getName().contains("vm1"))
				return 13.0;
			else if (vmType.getName().contains("vm2"))
				return 10.0;
		}

		else if (task.getId().contains("ID006")) {
			if (vmType.getName().contains("vm0"))
				return 13.0;
			else if (vmType.getName().contains("vm1"))
				return 16.0;
			else if (vmType.getName().contains("vm2"))
				return 9.0;
		}

		else if (task.getId().contains("ID007")) {
			if (vmType.getName().contains("vm0"))
				return 7.0;
			else if (vmType.getName().contains("vm1"))
				return 15.0;
			else if (vmType.getName().contains("vm2"))
				return 11.0;
		}

		else if (task.getId().contains("ID008")) {
			if (vmType.getName().contains("vm0"))
				return 5.0;
			else if (vmType.getName().contains("vm1"))
				return 11.0;
			else if (vmType.getName().contains("vm2"))
				return 14.0;
		}

		else if (task.getId().contains("ID009")) {
			if (vmType.getName().contains("vm0"))
				return 18.0;
			else if (vmType.getName().contains("vm1"))
				return 12.0;
			else if (vmType.getName().contains("vm2"))
				return 20.0;
		}

		else if (task.getId().contains("ID010")) {
			if (vmType.getName().contains("vm0"))
				return 21.0;
			else if (vmType.getName().contains("vm1"))
				return 7.0;
			else if (vmType.getName().contains("vm2"))
				return 16.0;
		}
		return -1;
	}

	@Override
	public double getTransferTime(VMType to, Task source, Task sink) {
		return getPreCommunicationCost(source, sink);
	}

	@Override
	protected void onInternalAfterSchedulingCompleted() {
		logln(">>> " + getSchedulingAlgorithmName() + ";Makespan;" + getMakespan());
		super.onInternalAfterSchedulingCompleted();
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "HEFT-LookAhead-Test-2";
	}
}
