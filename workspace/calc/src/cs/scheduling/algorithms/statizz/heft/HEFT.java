package cs.scheduling.algorithms.statizz.heft;

import java.util.HashMap;
import java.util.Map;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

// Paper title: Performance-Effective and Low-Complexity
// Task Scheduling for Heterogeneous Computing

public class HEFT extends HEFTBase {

	private Map<Task, Double> W;

	private Map<Task, Map<Task, Double>> C;

	public HEFT(DAG dag, Environment environment) {
		super(dag, Double.MAX_VALUE, environment);

		C = new HashMap<Task, Map<Task, Double>>();
		W = new HashMap<Task, Double>();

	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "HEFT";
	}

	@Override
	public void createPreComputationCosts() {
		for (String taskName : getDAG().getTasks()) {
			Task task = getDAG().getTaskById(taskName);

			double sum = 0.0;

			for (VMType vm : getAllVMTypes()) {
				sum += Environment.getPredictedRuntime(task, vm);
			}
			W.put(task, sum / getAllVMTypes().size());
		}

	}

	@Override
	public void createPreCommunicationCosts() {

		double averageBandwidth = calculateAverageBandwidth();

		// Initializing the matrix
		for (String taskName1 : getDAG().getTasks()) {
			Task parent = getDAG().getTaskById(taskName1);
			Map<Task, Double> taskTransferCosts = new HashMap<Task, Double>();

			C.put(parent, taskTransferCosts);
		}

		// Calculating the actual values
		for (String parentName : getDAG().getTasks()) {
			Task parent = getDAG().getTaskById(parentName);
			for (Task child : parent.getChildren()) {

				double bytes = parent.getTotalBytesToSent(child);

				C.get(parent).put(child, bytes / averageBandwidth);
			}
		}
	}

	private double calculateAverageBandwidth() {
		// calculating the average bandwidth
		double averageBandwidth = 0.0;

		for (VMType type : getAllVMTypes()) {
			averageBandwidth += (type.getInternalBandwidth() + type.getExternalBandwidth()) / 2;
		}
		averageBandwidth /= getAllVMTypes().size();

		return averageBandwidth;
	}

	@Override
	public double getPreComputationCost(Task t) {
		return W.get(t);
	}

	@Override
	public double getPreCommunicationCost(Task t1, Task t2) {
		return C.get(t1).get(t2);
	}


	@Override
	public ScheduleEstimation resourceSelectionPhase(Task task) {

		// get the resource that minimizes the earliest finish time of the task
		VMType selectedResource = null;

		// temp variables
		double minEFT = Double.MAX_VALUE;
		double EST = 0.0, runtime = 0.0, EFT = 0.0;

		for (VMType vmType : getAllVMTypes()) {

			// check the current EST
			EST = getEST(task, vmType);

			// calculate runtime
			runtime = getRuntime(task, vmType);

			// check EST according to the insertion policy in this resource
			EST = insertionBasedSchedulingPolicy(EST, runtime, vmType);

			// calculate the EFT in this resource
			EFT = EST + runtime;

			if (EFT < minEFT) {
				selectedResource = vmType;
				minEFT = EFT;
			}

		}

		// actual values
		double AFT = minEFT;
		runtime = getRuntime(task, selectedResource);
		double AST = AFT - runtime;

		// save the schedule
		ScheduleEstimation estimation = new ScheduleEstimation(AST, runtime, AFT, selectedResource);

		return estimation;

	}

}
