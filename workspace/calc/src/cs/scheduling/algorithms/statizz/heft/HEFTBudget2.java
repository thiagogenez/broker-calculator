package cs.scheduling.algorithms.statizz.heft;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

public class HEFTBudget2 extends HEFT {

	public HEFTBudget2(DAG dag, Environment environment) {
		super(dag, environment);
	}

	@Override
	public ScheduleEstimation resourceSelectionPhase(Task task) {
		// get the resource that minimizes the earliest finish time of the task
		VMType selectedResource = null;

		// temp variables
		double minCost = Double.MAX_VALUE;
		double EST = 0.0, runtime = 0.0, EFT = 0.0, finalEFT = 0.0;

		double cost = 0.0;
		for (VMType vmType : getAllVMTypes()) {

			// check the current EST
			EST = getEST(task, vmType);

			// calculate runtime
			runtime = getRuntime(task, vmType);

			// check EST according to the insertion policy in this resource
			EST = insertionBasedSchedulingPolicy(EST, runtime, vmType);

			// calculate the EFT in this resource
			EFT = EST + runtime;

			// calculate cost
			cost = (runtime / vmType.getBillingTimeInSeconds()) * vmType.getBillingUnitPrice();
			

			if (cost < minCost) {
				
				selectedResource = vmType;

				minCost = cost;
				finalEFT = EFT;

			}

		}

		// actual values
		double AFT = finalEFT;
		runtime = getRuntime(task, selectedResource);
		double AST = AFT - runtime;

		// save the schedule
		ScheduleEstimation estimation = new ScheduleEstimation(AST, runtime, AFT, selectedResource);

		return estimation;

	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "HEFTwHCOC";
	}
}
