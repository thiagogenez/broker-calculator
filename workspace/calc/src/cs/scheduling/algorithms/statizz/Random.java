package cs.scheduling.algorithms.statizz;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cs.core.dag.DAG;
import cs.core.dag.DAGFile;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.job.Job;
import cs.core.job.JobListener;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.Transmission;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

public class Random extends StaticAlgorithm implements JobListener {

	private Map<VMType, Double> TIME;

	public Random(DAG dag, double deadline, Environment environment) {
		super(dag, deadline, environment);
	}

	/****************************
	 *** Scheduling functions ***
	 ****************************/

	// none

	/****************************************************
	 ** Override functions used by the SCHEDULING step **
	 ****************************************************/

	@Override
	protected void onInternalBeforeSchedulingStart() {
		TIME = new HashMap<VMType, Double>();

		for (VMType vmType : getAllVMTypes()) {
			setTime(vmType, 0.0);
		}
	}

	@Override
	protected Plan planDAG(Plan currentPlan) {
		java.util.Random random = new java.util.Random();
		List<VMType> vms = getAllVMTypes();

		for (Task task : getTopologicalOrder()) {
			// get next vm
			System.out.println(task);
			int raffle = random.nextInt(vms.size());
			VMType vmType = vms.get(raffle);

			// execution time information
			double startExecutionTime = 0.0;
			double finishExecutionTime = 0.0;
			double runtime = Environment.getPredictedRuntime(task, vmType);

			// calculate the start execution time
			if (!task.getParents().isEmpty()) {

				for (Task parent : task.getParents()) {
					double communicationDemand = parent.getTotalBytesToSent(task);
					ScheduleEstimation parentScheduleEstimation = currentPlan.getScheduleEstimation(parent);
					double bandwidthValue = Environment.getBandwidth(parentScheduleEstimation.getVmType(), vmType);

					// calculating the starting execution time of the
					// current task
					startExecutionTime = Math.max(
							// start execution time of the current
							// task calculated so far
							startExecutionTime,

							// finish execution of task's father + transfer time
							// to the current task
							parentScheduleEstimation.getFinishExecutionTime() + (communicationDemand / bandwidthValue));

					// time when the VM will be available
					startExecutionTime = Math.max(startExecutionTime, TIME.get(vmType));
				}
			}

			// calculating the finish execution time
			finishExecutionTime = startExecutionTime + runtime;
			setTime(vmType, finishExecutionTime);

			// local save
			ScheduleEstimation estimation = new ScheduleEstimation(startExecutionTime, runtime, finishExecutionTime,
					vmType);

			currentPlan.addScheduleEstimation(task, estimation);
		}

		// Random always returns a schedule
		return currentPlan;
	}

	private void setTime(VMType vmType, double time) {
		TIME.put(vmType, time);
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "RANDOM";
	}

	@Override
	protected void onInternalAfterSchedulingCompleted() {
		// do nothing
	}

	/***************************************************
	 ** Override functions used by the SIMULATOR step **
	 ***************************************************/

	@Override
	public void jobReleased(Job job) {
		Task task = job.getTask();
		job.setVM(getVM(task));
	}

	@Override
	public void jobSubmitted(Job job) {

	}

	@Override
	public void jobStarted(Job job) {

	}

	@Override
	public void jobFinished(Job job) {
		Task from = job.getTask();

		for (Task child : from.getChildren()) {
			for (DAGFile parentFile : from.getOutputFiles()) {
				for (DAGFile childFile : child.getInputFiles()) {

					if (parentFile.equals(childFile)) {
						Transmission t = new Transmission(child, parentFile);
						job.addTransmission(t.getReceiver(), t);
					}
				}
			}
		}
	}

}
