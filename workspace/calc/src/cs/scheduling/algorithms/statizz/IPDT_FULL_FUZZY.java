package cs.scheduling.algorithms.statizz;

import cs.core.dag.DAG;
import cs.core.engine.Environment;
import cs.scheduling.algorithms.statizz.ilp.ipdt.BatistaAlgorithm;
import cs.scheduling.algorithms.statizz.ilp.ipdt.BatistaILP;
import cs.scheduling.algorithms.statizz.ilp.ipdt.IPDT_FUZZY_ILP;

public class IPDT_FULL_FUZZY extends BatistaAlgorithm {

	// rho % = uncertainty level of communication demand of the DAG edge
	private double rho = 0.0;

	// sigma % = uncertainty level of computing demand of the DAG node
	private double sigma = 0.0;

	// chi % = uncertainty level of processing capacity of the resources
	private double chi = 0.0;

	// omega % = uncertainty level of the available bandwidth of the system
	private double omega = 0.0;

	private int nodeLim = -1;

	private double tiLim = -1.0;

	private int threads = -1;

	private double epgap = -1;

	public IPDT_FULL_FUZZY(DAG dag, double deadline, Environment environment, double rho, double sigma, double chi,
			double omega) {
		this(dag, deadline, environment, rho, sigma, chi, omega, -1, -1.0, -1, -1);
	}

	public IPDT_FULL_FUZZY(DAG dag, double deadline, Environment environment, double rho, double sigma, double chi,
			double omega, int nodeLim, double tiLim) {
		this(dag, deadline, environment, rho, sigma, chi, omega, nodeLim, tiLim, -1, -1);
	}

	public IPDT_FULL_FUZZY(DAG dag, double deadline, Environment environment, double rho, double sigma, double chi,
			double omega, int nodeLim, double tiLim, int threads, double epgap) {

		super(dag, deadline, environment);

		this.rho = rho;
		this.sigma = sigma;
		this.chi = chi;
		this.omega = omega;
		this.nodeLim = nodeLim;
		this.tiLim = tiLim;
		this.threads = threads;
		this.epgap = epgap;
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "IPDT-FULL-FUZZY";
	}

	@Override
	public BatistaILP createILP() {
		return new IPDT_FUZZY_ILP(getDAG(), getAllVMTypes(), getLogger(), this.sigma, this.rho, this.chi, this.omega,
				this.nodeLim, this.tiLim, this.threads, this.epgap);
	}

}
