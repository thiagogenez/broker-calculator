package cs.scheduling.algorithms.statizz;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import cs.core.dag.DAG;
import cs.core.dag.DAGFile;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.job.Job;
import cs.core.utils.AssortedMethods;
import cs.core.utils.CSVFileWriter;
import cs.core.vm.simulation.Transmission;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.pso.PSO;
import cs.scheduling.algorithms.statizz.pso.WorkflowFitnessFunction;
import cs.scheduling.algorithms.statizz.pso.WorkflowParticle;
import cs.scheduling.algorithms.statizz.pso.WorkflowParticleUpdate;
import cs.scheduling.algorithms.statizz.pso.WorkflowVariablesUpdate;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.BiggerMakespan;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.BitUnfairness;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.JainsFairness;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.MeanMakespan;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.MeanMakespanCost;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.MeanSlowdown;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.MeanSlowdownCost;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.OverallCost;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.OverallMakespan;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.OverallMakespanCost;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.RizosUnfairness;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.SchedulingFitnessFunction;
import cs.scheduling.algorithms.statizz.pso.multiple_workflows.StdSlowdown;
import net.sourceforge.jswarm_pso.Neighborhood1D;
import net.sourceforge.jswarm_pso.ParticleUpdateFullyRandom;
import net.sourceforge.jswarm_pso.ParticleUpdateRandomByParticle;
import net.sourceforge.jswarm_pso.ParticleUpdateSimple;
import net.sourceforge.jswarm_pso.Swarm;

public class MWS extends StaticAlgorithm implements PSO {

	private SchedulingFitnessFunction policy;

	private String policyName;

	public MWS(List<DAG> dags, double deadline, Environment environment, String policyName) {
		super(dags, deadline, environment);
		this.policyName = policyName;
	}

	/****************************************************
	 ** Override PSO functions **
	 ****************************************************/

	@Override
	public Plan pso(WorkflowFitnessFunction fitnessFunction, WorkflowParticle particle) {

		// Initialize the swarm
		logln(">>> Creating the swarm ...");
		Swarm swarm = new Swarm(NUMBER_OF_PARTICLES, particle, fitnessFunction);

		// setting the configuration of the particle's positions
		swarm.setMaxPosition(getAllVMTypes().size() - 1);
		swarm.setMinPosition(0);
		double maxVelocity[] = getMaxVelocity(getDimension());
		double velocityUpdate[] = getVelocityUpdate(getDimension());
		swarm.setMaxVelocity(maxVelocity);

		swarm.setInertia(INERTIA);
		swarm.setParticleIncrement(PARTICLE_INCREMENT);
		swarm.setGlobalIncrement(GLOBAL_INCREMENT);
		
//		swarm.setVariablesUpdate(new WorkflowVariablesUpdate(velocityUpdate));
//		swarm.setParticleUpdate(new WorkflowParticleUpdate(particle));
		swarm.setParticleUpdate(new ParticleUpdateRandomByParticle(particle));
//
		Neighborhood1D neighborhood1d = new Neighborhood1D(getDimension() / 2, false);
		swarm.setNeighborhood(neighborhood1d);
		swarm.setNeighborhoodIncrement(NEIGHBORHOOD_INCREMENT);

		logln(policy.logPointer());

		evolve(swarm, fitnessFunction);

		int[] position = fitnessFunction.round(swarm.getBestPosition());
		int[] savedPosition = fitnessFunction.getSavedPosition();

		// sanity check
		for (int i = 0; i < savedPosition.length; i++) {
			if (savedPosition[i] != position[i])
				throw new RuntimeException("Position <" + positionToString(position) + "> is different from <"
						+ positionToString(savedPosition) + ">");
		}

		logln(policy.printSolution(savedPosition));

		Plan plan = fitnessFunction.getSavedPlan();
		

		return plan;

	}

	private double[] getVelocityUpdate(int dimension) {
		double[] velocityUpdate = new double[dimension];
		for (int i = 0; i < velocityUpdate.length; i++) {
			double value = AssortedMethods.randomDouble();
			if (value > 0.5) {
				velocityUpdate[i] = 0.99;
			} else {
				velocityUpdate[i] = 1.01;
			}
		}
		return velocityUpdate;
	}

	private double[] getMaxVelocity(int dimension) {
		double[] velocity = new double[dimension];
		for (int i = 0; i < velocity.length; i++) {
			velocity[i] = AssortedMethods.randomIntInRange(1, 10) / 10.0;
		}

		return velocity;
	}

	private void evolve(Swarm swarm, WorkflowFitnessFunction fitnessFunction) {

		String[] header = { "iteration", "evaluation", "time", "inertia", "global_increment", "particle_increment",
				"cost", "best_fitness" };

		//CSVFileWriter csvFile = new CSVFileWriter(getLogger().getPath(), getLogger().getFilename(), header);

		long letsGameBegin = System.currentTimeMillis();

		for (int i = 0; i < MAX_ITERATION; i++) {

			// System.out.println(swarm.toStringStats());
			// System.out.println(
			// "max velocity: " + Arrays.toString(swarm.getMaxVelocity()));
			// System.out.println(
			// "min velocity: " + Arrays.toString(swarm.getMinVelocity()));

			if (!Double.isNaN(swarm.getBestFitness())) {
				long current = System.currentTimeMillis();

				List<Object> line = new LinkedList<Object>();
				line.add(i);
				line.add(swarm.getNumberOfEvaliations());
				line.add(TimeUnit.SECONDS.convert((current - letsGameBegin), TimeUnit.MILLISECONDS));
				line.add(swarm.getInertia());
				line.add(swarm.getGlobalIncrement());
				line.add(swarm.getParticleIncrement());
				line.add(fitnessFunction.getCurrentPlan().getExecutionCost());

				line.add(String.format("%.15f", swarm.getBestFitness()));

				//csvFile.write(line);
			}

			swarm.evolve();
		}

		//csvFile.close();
	}

	private String positionToString(int[] position) {
		StringBuilder stats = new StringBuilder();
		for (int j = 0; j < position.length; j++) {
			stats.append(String.valueOf(position[j]));
			stats.append(j < (position.length - 1) ? ", " : "");
		}
		stats.append("]");
		return stats.toString();
	}

	@Override
	public int getDimension() {
		int dimension = 0;
		for (DAG g : getAllDags()) {
			dimension += g.getNumberOfTasks();
		}
		return dimension;

	}

	@Override
	public double calculateFitnessValue(int[] particlePosition, Plan plan) {
		return policy.calulatelanFitnessValue(particlePosition, plan);
	}

	/****************************************************
	 ** Override functions used by the SCHEDULING step **
	 ****************************************************/

	@Override
	protected Plan planDAG(Plan currentPlan) {
		boolean maximize = policy.isMaximize();
		WorkflowFitnessFunction fitnessFunction = new WorkflowFitnessFunction(this, maximize, currentPlan,
				getAllDags().size());
		WorkflowParticle particle = new WorkflowParticle(getDimension());

		Plan plan = pso(fitnessFunction, particle);
		return plan;
	}

	@Override
	protected void onInternalBeforeSchedulingStart() {
		switch (policyName.toLowerCase()) {
		case "min-overall-makespan":
			policy = new OverallMakespan(getAllDags(), getAllVMTypes(), false);
			break;
		case "min-mean-makespan":
			policy = new MeanMakespan(getAllDags(), getAllVMTypes(), false);
			break;
		case "min-std-slowdown":
			policy = new StdSlowdown(getAllDags(), getAllVMTypes(), false);
			break;
		case "max-jains-fairness":
			policy = new JainsFairness(getAllDags(), getAllVMTypes(), true);
			break;
		case "min-bigger-makespan":
			policy = new BiggerMakespan(getAllDags(), getAllVMTypes(), false);
			break;

		case "min-mean-makespan-cost":
			policy = new MeanMakespanCost(getAllDags(), getAllVMTypes(), false);
			break;
		case "max-mean-slowdown":
			policy = new MeanSlowdown(getAllDags(), getAllVMTypes(), true);
			break;
		case "min-mean-slowdown-cost":
			policy = new MeanSlowdownCost(getAllDags(), getAllVMTypes(), false);
			break;
		case "min-overall-cost":
			policy = new OverallCost(getAllDags(), getAllVMTypes(), false);
			break;

		case "max-bit-unfairness-index":
			policy = new BitUnfairness(getAllDags(), getAllVMTypes(), true);
			break;
		case "min-rizos-unfairness-index":
			policy = new RizosUnfairness(getAllDags(), getAllVMTypes(), false);
			break;
		case "min-overall-makespan-cost":
			policy = new OverallMakespanCost(getAllDags(), getAllVMTypes(), false);
			break;
		default:
			throw new RuntimeException("Undefined scheduling policy");
		}
	}

	@Override
	protected void onInternalAfterSchedulingCompleted() {
		// do nothing
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "PSO";
	}

	/***************************************************
	 ** Override functions used by the SIMULATOR step **
	 ***************************************************/

	@Override
	public void jobReleased(Job job) {
		Task task = job.getTask();
		job.setVM(getVM(task));
	}

	@Override
	public void jobSubmitted(Job job) {
		// do nothing

	}

	@Override
	public void jobStarted(Job job) {
		// do nothing

	}

	@Override
	public void jobFinished(Job job) {
		Task from = job.getTask();

		for (Task child : from.getChildren()) {
			for (DAGFile parentFile : from.getOutputFiles()) {
				for (DAGFile childFile : child.getInputFiles()) {

					if (parentFile.equals(childFile)) {
						Transmission t = new Transmission(child, parentFile);
						job.addTransmission(t.getReceiver(), t);
					}
				}
			}
		}
	}

}
