package cs.scheduling.algorithms.statizz.ilp.ipdt;

import java.util.List;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.ilp.ILP;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.UnknownObjectException;

public abstract class BatistaILP extends ILP {

	private IloIntVar[][][] X = null;

	private int maxTime = 0;

	private int minTime = 0;

	public BatistaILP(DAG dag, List<VMType> vms, Logger logger, int nodeLim, double tiLim, int threads, double epgap) {

		super(vms, dag, nodeLim, tiLim, threads, epgap, logger);

		computingMinMaxTime(dag, vms);

		logger.logln("MinTime = " + this.minTime);
		logger.logln("MaxTime = " + this.maxTime);
	}

	private void computingMinMaxTime(DAG dag, List<VMType> vms) {
		double cpuDemands = dag.getTotalCPUDemands();
		double mips = 0.0;
		for (VMType vm : vms) {
			mips = Math.max(mips, vm.getMips());
		}
		this.maxTime = (int) (Math.ceil(cpuDemands / mips));

		List<String> longestPath = dag.getLongestPath();
		cpuDemands = 0.0;

		for (String task : longestPath) {
			cpuDemands += dag.getTaskById(task).getSize();
		}

		this.minTime = (int) (Math.ceil(cpuDemands / mips));

	}

	@Override
	public void getResults(IloCplex cplex, Plan currentPlan, StringBuilder builder)
			throws UnknownObjectException, IloException {
		double makespan = 0.0;

		for (int t = 0; t <= this.maxTime; t++) {
			for (int n = 0; n < getTasksIlpMap().size(); n++) {
				for (int v = 0; v < getVMs().size(); v++) {
					if (cplex.getValue(X[n][t][v]) > 0) {

						builder.append("X[" + getTasksIlpMap().get(n).getId() + "," + t + "," + v + "]="
								+ cplex.getValue(X[n][t][v]) + "\n");

						Task task = this.getTasksIlpMap().get(n);
						VMType vmType = getVMs().get(v);
						double runtime = Environment.getPredictedRuntime(task, vmType);
						double finishExecutionTime = t;
						double startExecutionTime = finishExecutionTime - runtime;

						ScheduleEstimation estimation = new ScheduleEstimation(startExecutionTime, runtime,
								finishExecutionTime, vmType);

						currentPlan.addScheduleEstimation(task, estimation);
						// according to the Batista's ILP
						// the t value of the last DAG task is
						// equal to the makespan of the DAG
						if (getDAG().isExitTask(task)) {
							makespan = Math.max(makespan, t);
						}
					}
				}
			}
		}

		builder.append("\n");

		builder.append("CPLEX objetive value (" + getObjectiveValueName() + "): " + (makespan) + "\n");

	}

	public void createVariables(IloCplex cplex) {
		this.X = createVariableX(cplex);

		createOtherVariables(cplex);
	}

	protected abstract void createOtherVariables(IloCplex cplex);

	protected abstract IloIntVar[][][] createVariableX(IloCplex cplex);

	public int getMaxTime() {
		return maxTime;
	}

	public int getMinTime() {
		return minTime;
	}

	public IloIntVar[][][] getX() {
		return X;
	}

	public void increaseMaxTime() {
		getLogger().logln(">>> Doubling the maxTime from " + maxTime + " to " + (maxTime * 1.5) + " ...");
		this.maxTime *= 1.5;
	}

}
