package cs.scheduling.algorithms.statizz.ilp.ipdt;

import cs.core.dag.DAG;
import cs.core.dag.DAGFile;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.job.Job;
import cs.core.vm.simulation.Transmission;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;

public abstract class BatistaAlgorithm extends StaticAlgorithm {

	private BatistaILP ilp;

	public BatistaAlgorithm(DAG dag, double deadline, Environment environment) {
		super(dag, deadline, environment);
	}

	/****************************************************
	 ** Override functions used by the SCHEDULING step **
	 ****************************************************/

	@Override
	protected Plan planDAG(Plan currentPlan) {
		for (int i = 0; i <= 2; i++) {
			logln(">>> ILP solver Call #" + i + " ...");

			Plan newPlan = ilp.solve(PlanFactory.getEmptyPlan(currentPlan));

			if (newPlan != null) {
				return newPlan;
			}

			ilp.increaseMaxTime();
		}

		return null;
	}

	@Override
	protected void onInternalBeforeSchedulingStart() {
		ilp = createILP();
	}

	public abstract BatistaILP createILP();

	@Override
	protected void onInternalAfterSchedulingCompleted() {
		// do nothing
	}

	/***************************************************
	 ** Override functions used by the SIMULATOR step **
	 ***************************************************/

	@Override
	public void jobReleased(Job job) {
		Task task = job.getTask();
		job.setVM(getVM(task));

	}

	@Override
	public void jobSubmitted(Job job) {

	}

	@Override
	public void jobStarted(Job job) {

	}

	@Override
	public void jobFinished(Job job) {

		Task from = job.getTask();

		for (Task child : from.getChildren()) {
			for (DAGFile parentFile : from.getOutputFiles()) {
				for (DAGFile childFile : child.getInputFiles()) {

					if (parentFile.equals(childFile)) {
						Transmission t = new Transmission(child, parentFile);
						job.addTransmission(t.getReceiver(), t);
					}
				}
			}
		}
	}

}
