package cs.scheduling.algorithms.statizz.ilp.noms;

import java.util.ArrayList;
import java.util.List;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import ilog.concert.IloException;
import ilog.concert.IloIntExpr;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearIntExpr;
import ilog.concert.IloNumExpr;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplexModeler;

public class IPWS_ILP2 extends GenezILP {

	public IPWS_ILP2(DAG dag, List<VMType> vms, Logger logger, int nodeLim, double tiLim, int threads, double epgap,
			double deadline, double lambda) {
		super(dag, vms, nodeLim, tiLim, threads, epgap, logger, deadline, lambda);
	}

	@Override
	public IloIntVar[][][] createVariableX(IloCplex cplex) {

		IloIntVar[][][] X = new IloIntVar[getTasksIlpMap().size()][][];

		// creating the variable X
		try {
			// loop for each dag node
			for (int n = 0; n < getTasksIlpMap().size(); n++) {
				X[n] = new IloIntVar[getMaxTime() + 1][];

				// loop for each time slot
				for (int t = 0; t <= getMaxTime(); t++) {
					X[n][t] = new IloIntVar[getVMs().size()];

					// loop for each VM
					for (int v = 0; v < getVMs().size(); v++) {
						String label = "X[" + n + "," + dislambdafy(t) + "," + v + "]";
						X[n][t][v] = cplex.intVar(0, 1, label);
					}
				}
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}

		return X;
	}

	@Override
	public IloIntVar[] createVariableF(IloCplex cplex) {
		IloIntVar[] F = new IloIntVar[getVMs().size()];

		// creating the variable Y
		try {

			// loop for each VM
			for (int v = 0; v < getVMs().size(); v++) {
				String label = "F[" + v + "]";
				F[v] = cplex.intVar(0, getMaxTime(), label);
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}

		return F;
	}

	@Override
	protected void createObjectiveExpression(IloCplex cplex) {
		try {

			// create the expression of the minimization of the makespan

			ArrayList<IloNumExpr> objective = new ArrayList<IloNumExpr>();

			for (int v = 0; v < getVMs().size(); v++) {

				VMType vmType = getVMs().get(v);

				// objective.add(cplex.prod(vmType.getPriceForBillingUnit() *
				// getLambda(),
				// cplex.diff(getF()[v], getS()[v])));

				objective.add(cplex.prod(vmType.getPriceForBillingUnit() * getLambda(), getF()[v]));
			}

			// set the CPLEX to minimize the objective
			cplex.addMinimize(cplex.sum(objective.toArray(new IloNumExpr[objective.size()])));

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	@Override
	protected void defineConstraints(IloCplex cplex) {
		defineConstraintC1(cplex);
		defineConstraintC2(cplex);
		defineConstraintC3(cplex);
		defineConstraintC4(cplex);
		defineConstraintC5(cplex);
		defineConstraintC6(cplex);
	}

	// Constraints C1 determines that a task must
	// be executed only ONCE on a SINGLE host
	private void defineConstraintC1(IloCplex cplex) {
		try {

			for (int j = 0; j < getTasksIlpMap().size(); j++) {
				IloLinearIntExpr expression = cplex.linearIntExpr();

				// create expressions
				for (int t = 0; t <= getMaxTime(); t++) {

					for (int k = 0; k < getVMs().size(); k++) {
						expression.addTerm(getX()[j][t][k], 1);
					}
				}

				// add the expressions
				cplex.addEq(expression, 1);
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// the constraints determine that a task i cannot terminate until all of its
	// instructions have been completely executed
	private void defineConstraintC2(IloCplex cplex) {
		try {

			for (int j = 0; j < getTasksIlpMap().size(); j++) {

				for (int v = 0; v < getVMs().size(); v++) {

					int limit = lambdafy(getTasksIlpMap().get(j).getSize() / getVMs().get(v).getMips());

					// create expressions
					for (int t = 0; t <= limit; t++) {
						cplex.addEq(getX()[j][t][v], 0, "C2");
					}

				}
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// the constraints establish that the execution of the task j cannot start
	// until all its predecessors have been completed and the data required by
	// the task j have been transferred to the vm that will execute j
	private void defineConstraintC3(IloCplex cplex) {
		try {

			// i is the index of the node
			for (int i = 0; i < getTasksIlpMap().size(); i++) {

				for (Task child : getTasksIlpMap().get(i).getChildren()) {

					// j is the index of the child node
					int j = getInverseTaskIlpMap().get(child);

					double communicationDemands = getTasksIlpMap().get(i).getTotalBytesToSent(child);

					for (int l = 0; l < getVMs().size(); l++) {

						for (int t = 0; t <= getMaxTime(); t++) {

							// create LEFT-HAND side expressions
							IloLinearIntExpr lExpression = cplex.linearIntExpr();

							for (int k = 0; k < getVMs().size(); k++) {

								int limit = lambdafy(dislambdafy(t) -
								// execution time of the child task j on
								// the virtual machine l
										(getTasksIlpMap().get(j).getSize() / getVMs().get(l).getMips())
										// transfer time among virtual
										// machine
										- (communicationDemands
												/ Environment.getBandwidth(getVMs().get(k), getVMs().get(l))));

								// terms of the left-hand side expression
								for (int s = 0; s <= limit; s++) {
									lExpression.addTerm(getX()[i][s][k], 1);
								}
							}

							// create RIGHT-HAND side expressions
							IloLinearIntExpr rExpression = cplex.linearIntExpr();

							// terms of the right-hand side expression
							for (int s = 0; s <= t; s++) {
								rExpression.addTerm(getX()[j][s][l], 1);
							}

							// add the expressions
							cplex.addGe(lExpression, rExpression);

						}
					}
				}
			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// The constraints establish that the number of task execution at
	// any given time is at least equal to the number of CPUs
	private void defineConstraintC4(IloCplex cplex) {
		try {

			for (int k = 0; k < getVMs().size(); k++) {

				for (int t = 0; t <= getMaxTime(); t++) {

					// create expressions
					IloLinearIntExpr expression = cplex.linearIntExpr();

					for (int j = 0; j < getTasksIlpMap().size(); j++) {

						// establishing a limit of time
						int limit = lambdafy(getTasksIlpMap().get(j).getSize() / getVMs().get(k).getMips());

						// the runtime of the task n cannot be grater than
						// maxTime value
						if (t + limit <= getMaxTime()) {

							for (int s = t; s <= t + limit - 1; s++) {
								expression.addTerm(getX()[j][s][k], 1);
							}
						}
					}

					// add the expressions
					cplex.addLe(expression, getVMs().get(k).getCores());
				}
			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// Constraints determines that the makespan
	// can not be larger than the deadline.
	private void defineConstraintC5(IloCplex cplex) {
		try {
			for (int j = 0; j < getTasksIlpMap().size(); j++) {
				IloLinearIntExpr expression = cplex.linearIntExpr();

				// create expressions
				for (int t = getDeadline() + 1; t <= getMaxTime(); t++) {

					for (int k = 0; k < getVMs().size(); k++) {
						expression.addTerm(getX()[j][t][k], 1);
					}
				}

				// add the expressions
				cplex.addEq(expression, 0);
			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// Constraints determine that a VM must stay active (i.e., with
	// status being used enabled in the variable y)
	// while it is executing the node which requires it.
	private void defineConstraintC6(IloCplex cplex) {
		try {

			for (int v = 0; v < getVMs().size(); v++) {

				// LEFT-hand side expressions
				ArrayList<IloIntExpr> max = new ArrayList<IloIntExpr>();
				// ArrayList<IloIntExpr> min = new ArrayList<IloIntExpr>();

				for (int t = 0; t <= getMaxTime(); t++) {
					for (int j = 0; j < getTasksIlpMap().size(); j++) {
						max.add(cplex.prod(getX()[j][t][v], t));

						// establishing a limit of time
						// int limit =
						// lambdafy(getTasksIlpMap().get(j).getSize()
						// / getVMs().get(v).getMips());

						// the runtime of the task n cannot be grater than
						// maxTime value
						// if (t - limit >= 0) {
						//
						// // min.add(cplex.prod(getX()[j][t][v], t - limit));
						// min.add(cplex.sum(
						// cplex.prod(10000,
						// cplex.diff(1, getX()[j][t][v])),
						// cplex.prod(getX()[j][t][v], t - limit)));
						//
						// }
					}
				}

				// add the expressions
				cplex.addEq(getF()[v], cplex.max(max.toArray(new IloIntExpr[max.size()])));

				// cplex.addEq(getS()[v],
				// cplex.min(min.toArray(new IloIntExpr[min.size()])));

			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	@Override
	public String getObjectiveValueName() {
		return "Monetary Cost";
	}

	@Override
	public IloIntVar[][] createVariableY(IloCplex cplex) {
		return null;
	}

}
