package cs.scheduling.algorithms.statizz.ilp.noms;

import java.util.List;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.ilp.ILP;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.UnknownObjectException;

public abstract class GenezILP extends ILP {

	private IloIntVar[][][] X = null;

	private IloIntVar[][] Y = null;

	private IloIntVar[] F = null;

	private double lambda = 1.0;

	private int maxTime = 0;

	private int deadline = 0;

	public GenezILP(DAG dag, List<VMType> vms, int nodeLim, double tiLim, int threads, double epgap, Logger logger,
			double deadline, double lambda) {
		super(vms, dag, nodeLim, tiLim, threads, epgap, logger);

		this.lambda = lambda;
		computingMaxTime(deadline);
	}

	//////////////// ABSTRACT METHODS ///////////////////

	public abstract IloIntVar[][] createVariableY(IloCplex cplex);

	public abstract IloIntVar[][][] createVariableX(IloCplex cplex);

	public abstract IloIntVar[] createVariableF(IloCplex cplex);

	//////////////// ABSTRACT METHODS ///////////////////

	private void computingMaxTime(double deadline) {
		this.maxTime = lambdafy(deadline);
		this.deadline = lambdafy(deadline);
	}

	public int lambdafy(double value) {
		return (int) Math.ceil(value / lambda);
	}

	public int dislambdafy(double t) {
		return (int) Math.ceil(t * lambda);
	}

	@Override
	protected void createVariables(IloCplex cplex) {
		X = createVariableX(cplex);
		Y = createVariableY(cplex);
		F = createVariableF(cplex);
	}

	@Override
	public String getExtraResults(IloCplex cplex) throws UnknownObjectException, IloException {
		return "";
	}

	@Override
	public void getResults(IloCplex cplex, Plan currentPlan, StringBuilder builder)
			throws UnknownObjectException, IloException {

		for (int t = 0; t <= this.maxTime; t++) {
			for (int n = 0; n < getTasksIlpMap().size(); n++) {
				for (int v = 0; v < getVMs().size(); v++) {
					if (cplex.getValue(X[n][t][v]) > 0) {

						builder.append("X[" + getTasksIlpMap().get(n).getId() + "," + t + "," + v + "] = " +

								"X[" + getTasksIlpMap().get(n).getId() + "," + dislambdafy(t) + "," + v + "] = "
								+ cplex.getValue(X[n][t][v]) + "\n");

						Task task = this.getTasksIlpMap().get(n);
						VMType vmType = getVMs().get(v);
						double runtime = Environment.getPredictedRuntime(task, vmType);
						double finishExecutionTime = dislambdafy(t);
						double startExecutionTime = finishExecutionTime - runtime;

						ScheduleEstimation estimation = new ScheduleEstimation(startExecutionTime, runtime,
								finishExecutionTime, vmType);

						currentPlan.addScheduleEstimation(task, estimation);
					}
				}
			}
		}

		builder.append("\n");

		if (this.Y != null) {
			for (int v = 0; v < getVMs().size(); v++) {

				for (int t = 0; t <= this.maxTime; t++) {
					VMType vm = getVMs().get(v);
					if (cplex.getValue(Y[t][v]) > 0) {

						builder.append("Y[" + t + "," + vm + "] = " + "Y[" + dislambdafy(t) + "," + vm + "] = "
								+ cplex.getValue(Y[t][v]) + "\n");
					}
				}
			}
		}

		builder.append("\n");
		if (this.F != null) {
			for (int v = 0; v < getVMs().size(); v++) {

				VMType vm = getVMs().get(v);

				builder.append("F[" + vm + "] = " + "F[" + vm + "] = " + cplex.getValue(F[v]) + "\n");
			}
		}
		builder.append("\n");

		builder.append("Deadline: " + deadline + "\n");
		builder.append("Maxtime: " + maxTime + "\n");
	}

	public void increaseMaxTime() {
		getLogger().logln(">>> Doubling the maxTime from " + maxTime + " to " + (maxTime * 1.5) + " ...");
		this.maxTime *= 1.5;
	}

	public int getMaxTime() {
		return maxTime;
	}

	public IloIntVar[][][] getX() {
		return X;
	}

	public IloIntVar[][] getY() {
		return Y;
	}

	public double getLambda() {
		return lambda;
	}

	public int getDeadline() {
		return deadline;
	}

	public IloIntVar[] getF() {
		return F;
	}

}
