package cs.scheduling.algorithms.statizz.ilp;

import java.util.List;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.Plan;
import ilog.concert.IloException;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.UnknownObjectException;
import ilog.cplex.IloCplexModeler;

public abstract class ILP {

	private Logger logger;

	private TreeMap<Integer, Task> tasksIlpMap;

	private TreeMap<Task, Integer> inverseTaskIlpMap;

	private List<VMType> vms = null;

	private DAG dag;

	private int nodeLim = -1;

	private double tiLim = -1;

	private int threads = -1;

	private double runtime = -1;

	private double epgap = -1;

	public ILP(List<VMType> vms, DAG dag, int nodeLim, double tiLim, int threads, double epgap, Logger logger) {
		this.logger = logger;
		this.vms = vms;
		this.dag = dag;
		this.tasksIlpMap = new TreeMap<Integer, Task>();
		this.inverseTaskIlpMap = new TreeMap<Task, Integer>();

		this.nodeLim = nodeLim;
		this.tiLim = tiLim;
		this.threads = threads;
		this.epgap = epgap;

		// creating internal mapping between tasks and index i's
		mappingTasksToILP(dag);
	}

	//////////////// ABSTRACT METHODS ///////////////////

	protected abstract void createVariables(IloCplex cplex);

	protected abstract void createObjectiveExpression(IloCplex cplex);

	protected abstract void defineConstraints(IloCplex cplex);

	public abstract String getExtraResults(IloCplex cplex) throws UnknownObjectException, IloException;

	public abstract String getObjectiveValueName();

	public abstract void getResults(IloCplex cplex, Plan currentPlan, StringBuilder builder)
			throws UnknownObjectException, IloException;

	//////////////// ABSTRACT METHODS ///////////////////

	private void mappingTasksToILP(DAG dag) {
		int id = 0;
		String[] tasks = dag.getTasks();

		for (String taskId : tasks) {
			Task t = dag.getTaskById(taskId);
			tasksIlpMap.put(id, t);
			inverseTaskIlpMap.put(t, id);
			id++;
		}
	}

	public Plan solve(Plan currentPlan) {
		try {
			IloCplex cplex = new IloCplex();

			logger.logln(">>> Creating Variables ...");
			createVariables(cplex);

			logger.logln(">>> Creating objective expression ...");
			createObjectiveExpression(cplex);

			logger.logln(">>> Defining constraints ...");
			defineConstraints(cplex);

			setParameters(cplex, nodeLim, tiLim, threads, epgap);
			logger.logln(">>> Solving ...");

			// cplex.exportModel("model.lp");

			long start = System.nanoTime();
			System.out.println("\nCalling CPLEX");

			boolean hasSolved = cplex.solve();

			System.out.println("Cplex FINISHED\n");
			long end = System.nanoTime();

			if (hasSolved) {

				logger.logln(">>> Solution found ...");
				this.runtime = TimeUnit.SECONDS.convert((end - start), TimeUnit.NANOSECONDS);
				logger.logln(">>> Printing results ...");

				getResults(cplex, currentPlan);

				return currentPlan;
			} else {
				logger.logln(">>> Solution NOT found ...");
			}

			cplex.endModel();
			cplex.end();

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(logger.getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(logger.getPrintStream());
		}

		return null;
	}

	public void getResults(IloCplex cplex, Plan currentPlan) {
		try {
			StringBuilder builder = new StringBuilder();

			builder.append("####### CPLEX OUTPUT ########\n");

			getResults(cplex, currentPlan, builder);

			builder.append(getExtraResults(cplex));
			builder.append("CPLEX tiLim: " + this.tiLim + "\n");
			builder.append("CPLEX nodeLim: " + this.nodeLim + "\n");
			builder.append("CPLEX runtime: " + this.runtime + " seconds \n");
			builder.append("CPLEX status: " + cplex.getStatus() + "\n");
			builder.append("CPLEX objetive value: " + cplex.getObjValue() + "\n");
			builder.append("####### CPLEX OUTPUT ########\n");

			logger.logln(builder.toString());

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(logger.getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(logger.getPrintStream());
		}
	}

	private void setParameters(IloCplex cplex, int nodeLim, double tiLim, int threads, double epgap)
			throws IloException {
		if (nodeLim >= 0) {
			cplex.setParam(IloCplex.IntParam.NodeLim, nodeLim);
		}

		if (tiLim > 0) {
			cplex.setParam(IloCplex.DoubleParam.TimeLimit, tiLim);
		}
		if (threads > 0) {
			cplex.setParam(IloCplex.IntParam.Threads, threads);
		}

		if (epgap > 0) {
			cplex.setParam(IloCplex.DoubleParam.EpGap, epgap);
		}

		// cplex.setParam(IloCplex.BooleanParam.LBHeur, true);

		// cplex.setParam(IloCplex.IntParam.VarSel, 4);
		// cplex.setParam(IloCplex.BooleanParam.MemoryEmphasis, true);
		cplex.setParam(IloCplex.Param.WorkMem, 4096);

		// Sets a relative tolerance on the gap between the best integer
		// objective and the objective of the best node remaining.
		// The relative gap tolerance is a stopping criterion for polishing.

		// cplex.setParam(IloCplex.IntParam.IntSolLim, 3);

		// Sets an absolute tolerance on the gap between the best integer
		// objective and the objective of the best node remaining.
		// The absolute gap tolerance is a stopping criterion for polishing.
		// cplex.setParam(IloCplex.DoubleParam.EpAGap, 5);

		// cplex.setParam(IloCplex.DoubleParam.CutUp, getMaxTime()* 2 );

		// To speed up the proof of optimality,
		cplex.setParam(IloCplex.DoubleParam.RelObjDif, 0.4);
		// cplex.setParam(IloCplex.DoubleParam.ObjDif, 3);
	}

	public TreeMap<Integer, Task> getTasksIlpMap() {
		return tasksIlpMap;
	}

	public TreeMap<Task, Integer> getInverseTaskIlpMap() {
		return inverseTaskIlpMap;
	}

	public List<VMType> getVMs() {
		return vms;
	}

	public Logger getLogger() {
		return logger;
	}

	public DAG getDAG() {
		return dag;
	}

}
