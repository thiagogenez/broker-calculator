package cs.scheduling.algorithms.statizz.ilp.ipdt;

import java.util.LinkedList;
import java.util.List;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearIntExpr;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.UnknownObjectException;
import ilog.cplex.IloCplexModeler;

public class IPDT_FUZZY_ILP extends BatistaILP {

	private IloNumVar lambda = null;

	private IloNumVar[] f = null;

	// uncertainty level of sigma% of computing demand of the DAG node
	private double sigma = 0.0;

	// uncertainty levl of rho% of communication demand of the DAG edge
	private double rho = 0.0;

	// uncertainty level of chi% of processing capacity of the resources
	private double chi = 0.0;

	// uncertainty level of omega% of the available bandwidth of the system
	private double omega = 0.0;

	private List<Integer> exitTasks;

	private int maxTimePrime = -1;

	private int minTimePrime = -1;

	// ILP-FULL-FUZZY
	public IPDT_FUZZY_ILP(DAG dag, List<VMType> vms, Logger logger, double sigma, double rho, double chi, double omega,
			int nodeLim, double tiLim, int threads, double epgap) {
		super(dag, vms, logger, nodeLim, tiLim, threads, epgap);

		this.sigma = sigma;

		this.rho = rho;

		this.chi = chi;

		this.omega = omega;

		exitTasks = new LinkedList<Integer>();

		// set the new maxTime value
		this.maxTimePrime = (int) Math.ceil(getMaxTime() * (1 + sigma) * (1 + chi));

		// set the minTime value
		this.minTimePrime = (int) Math.ceil(getMinTime() * (1 - sigma) * (1 - chi));

	}

	@Override
	public int getMaxTime() {
		if (this.maxTimePrime <= 0) {
			return super.getMaxTime();
		}
		return this.maxTimePrime;
	}

	@Override
	protected void createObjectiveExpression(IloCplex cplex) {
		try {

			// create the expression
			IloLinearNumExpr objective = cplex.linearNumExpr();

			this.lambda = cplex.numVar(0, 1, "lambda");
			objective.addTerm(this.lambda, 1);

			// set the CPLEX to maximize the objective
			cplex.addMaximize(objective);

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	@Override
	protected void defineConstraints(IloCplex cplex) {
		defineConstraintF0(cplex);
		defineConstraintF1(cplex);
		defineConstraintF2(cplex);
		defineConstraintF3(cplex);
		defineConstraintF4(cplex);
		defineConstraintF5(cplex);

	}

	private void defineConstraintF0(IloCplex cplex) {
		try {
			for (int j = 0; j < getTasksIlpMap().size(); j++) {

				// create expressions
				IloLinearIntExpr expression = cplex.linearIntExpr();

				for (int t = 0; t <= getMaxTime(); t++) {
					for (int v = 0; v < getVMs().size(); v++) {
						expression.addTerm(getX()[j][t][v], t);
					}
				}

				// add the expressions
				cplex.addEq(f[j], expression);
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// The constraints that establishes the relationship
	// between Lamba value and f_n value, which is
	// the time when the task n finishes its execution
	private void defineConstraintF1(IloCplex cplex) {
		try {

			for (Integer exit : exitTasks) {

				IloLinearNumExpr l_expression = cplex.linearNumExpr();
				IloLinearNumExpr r_expression = cplex.linearNumExpr();

				// left-hand side
				l_expression.addTerm(this.f[exit], -1.0);

				// right-hand side
				r_expression.addTerm(this.lambda, (this.maxTimePrime - this.minTimePrime));

				// add the expressions
				cplex.addGe(cplex.sum(l_expression, this.maxTimePrime), r_expression);
			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// Constraints that determines that a task must be
	// executed only ONCE on a SINGLE host
	private void defineConstraintF2(IloCplex cplex) {
		try {
			for (int j = 0; j < getTasksIlpMap().size(); j++) {
				IloLinearIntExpr expression = cplex.linearIntExpr();

				// create expressions
				for (int t = 0; t <= getMaxTime(); t++) {
					for (int k = 0; k < getVMs().size(); k++) {
						expression.addTerm(getX()[j][t][k], 1);
					}
				}

				// add the expressions
				cplex.addEq(expression, 1);
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// the constraints determine that a task i cannot terminate until all of its
	// instructions have been completely executed
	private void defineConstraintF3(IloCplex cplex) {
		try {
			for (int j = 0; j < getTasksIlpMap().size(); j++) {
				for (int k = 0; k < getVMs().size(); k++) {

					int limit = (int) Math.ceil((getTasksIlpMap().get(j).getSize() * (1 - this.sigma))
							/ (getVMs().get(k).getMips()) * (1 - this.chi));

					// create expressions
					for (int t = 0; t <= limit; t++) {
						cplex.addEq(getX()[j][t][k], 0);
					}
				}
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// the constraints establish that the execution of the task j cannot start
	// until all its predecessors have been completed and the data required by
	// the task j have been transferred to the vm that will execute j
	private void defineConstraintF4(IloCplex cplex) {
		try {

			// i is the index of the node
			for (int i = 0; i < getTasksIlpMap().size(); i++) {
				for (Task child : getTasksIlpMap().get(i).getChildren()) {

					// j is the index of the child node
					int j = getInverseTaskIlpMap().get(child);

					double communicationDemands = getTasksIlpMap().get(i).getTotalBytesToSent(child);

					for (int l = 0; l < getVMs().size(); l++) {
						for (int t = 0; t <= getMaxTime(); t++) {

							// create LEFT-HAND side expressions
							IloLinearIntExpr lExpression = cplex.linearIntExpr();

							for (int k = 0; k < getVMs().size(); k++) {

								int limit = (int) Math.ceil(t
										// execution time of the child task j on
										// the virtual machine l
										- ((getTasksIlpMap().get(j).getSize() * (1 + this.sigma))
												/ (getVMs().get(l).getMips() * (1 + this.chi)))

										// transfer time among
										// virtual
										// machine

										- ((communicationDemands * (1 + this.rho))
												/ (Environment.getBandwidth(getVMs().get(k), getVMs().get(l))
														* (1 + this.omega))));

								// terms of the left-hand side expression
								for (int s = 0; s <= limit; s++) {
									lExpression.addTerm(getX()[i][s][k], 1);
								}
							}

							// create RIGHT-HAND side expressions
							IloLinearIntExpr rExpression = cplex.linearIntExpr();

							// terms of the right-hand side expression
							for (int s = 1; s <= t; s++) {
								rExpression.addTerm(getX()[j][s][l], 1);
							}

							// add the expressions
							cplex.addGe(lExpression, rExpression);

						}
					}
				}
			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// The constraints establish that the number of task execution at
	// any given time is at least equal to the number of CPUs
	private void defineConstraintF5(IloCplex cplex) {
		try {

			for (int k = 0; k < getVMs().size(); k++) {
				for (int t = 0; t <= getMaxTime(); t++) {

					// create expressions
					IloLinearIntExpr expression = cplex.linearIntExpr();

					for (int j = 0; j < getTasksIlpMap().size(); j++) {

						// establishing a limit of time
						int limit = (int) Math.ceil((getTasksIlpMap().get(j).getSize() * (1 - this.sigma))
								/ (getVMs().get(k).getMips() * (1 - this.chi)));

						// the runtime of the task n cannot be grater than
						// maxTime value
						if (t + limit <= getMaxTime()) {

							for (int s = t; s <= t + limit - 1; s++) {
								expression.addTerm(getX()[j][s][k], 1);
							}
						}
					}

					// add the expressions
					cplex.addLe(expression, getVMs().get(k).getCores());
				}
			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	@Override
	protected IloIntVar[][][] createVariableX(IloCplex cplex) {
		IloIntVar[][][] X = new IloIntVar[getTasksIlpMap().size()][][];
		// creating the variable X
		try {
			// loop for each dag node
			for (int n = 0; n < getTasksIlpMap().size(); n++) {
				X[n] = new IloIntVar[getMaxTime() + 1][];

				// loop for each time slot
				for (int t = 0; t <= getMaxTime(); t++) {
					X[n][t] = new IloIntVar[getVMs().size()];

					// loop for each
					for (int v = 0; v < getVMs().size(); v++) {
						String label = "X[" + n + "," + t + "," + v + "]";
						X[n][t][v] = cplex.intVar(0, 1, label);
					}
				}
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
		return X;
	}

	@Override
	public String getObjectiveValueName() {
		return "makespan";
	}

	@Override
	public String getExtraResults(IloCplex cplex) throws UnknownObjectException, IloException {
		String result = "";
		result += "lambda = " + cplex.getValue(this.lambda) + "\n";

		for (Integer exit : exitTasks) {
			result += "f_" + exit + " = " + cplex.getValue(this.f[exit]) + "\n";
		}

		return result;
	}

	@Override
	protected void createOtherVariables(IloCplex cplex) {
		f = new IloNumVar[getTasksIlpMap().size()];
		// creating the variable X
		try {
			// loop for each dag node
			for (int n = 0; n < getTasksIlpMap().size(); n++) {
				Task t = getTasksIlpMap().get(n);

				if (getDAG().isExitTask(t)) {
					f[n] = cplex.numVar(this.minTimePrime, this.maxTimePrime, "f_" + n);
					exitTasks.add(n);

				} else {
					f[n] = cplex.numVar(0, this.maxTimePrime, "f_" + n);
				}

			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

}
