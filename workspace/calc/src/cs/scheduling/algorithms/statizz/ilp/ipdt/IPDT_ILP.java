package cs.scheduling.algorithms.statizz.ilp.ipdt;

import java.util.List;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.utils.Logger;
import cs.core.vm.scheduling.VMType;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearIntExpr;
import ilog.concert.IloLinearNumExpr;
import ilog.cplex.IloCplex;
import ilog.cplex.IloCplex.UnknownObjectException;
import ilog.cplex.IloCplexModeler;

public class IPDT_ILP extends BatistaILP {

	public IPDT_ILP(DAG dag, List<VMType> vms, Logger logger) {
		this(dag, vms, logger, -1, -1.0, -1, -1);
	}

	public IPDT_ILP(DAG dag, List<VMType> vms, Logger logger, int nodeLim, double tiLim) {
		super(dag, vms, logger, nodeLim, tiLim, -1, -1);
	}

	public IPDT_ILP(DAG dag, List<VMType> vms, Logger logger, int nodeLim, double tiLim, int threads, double epgap) {
		super(dag, vms, logger, nodeLim, tiLim, threads, epgap);
	}

	@Override
	protected void createObjectiveExpression(IloCplex cplex) {
		try {

			// create the expression of the minimization of the makespan
			IloLinearNumExpr objective = cplex.linearNumExpr();

			for (int t = 0; t <= getMaxTime(); t++) {
				for (int v = 0; v < getVMs().size(); v++) {
					objective.addTerm(getX()[getTasksIlpMap().lastKey()][t][v], t);
				}
			}

			// set the CPLEX to minimize the objective
			cplex.addMinimize(objective);

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	@Override
	protected void defineConstraints(IloCplex cplex) {
		defineConstraintD1(cplex);
		defineConstraintD2(cplex);
		defineConstraintD3(cplex);
		defineConstraintD4(cplex);
	}

	// Constraints D1 determines that a task must be executed only ONCE on a
	// SINGLE host
	private void defineConstraintD1(IloCplex cplex) {
		try {
			for (int j = 0; j < getTasksIlpMap().size(); j++) {
				IloLinearIntExpr expression = cplex.linearIntExpr();

				// create expressions
				for (int t = 0; t <= getMaxTime(); t++) {
					for (int k = 0; k < getVMs().size(); k++) {
						expression.addTerm(getX()[j][t][k], 1);
					}
				}

				// add the expressions
				cplex.addEq(expression, 1);
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// the constraints determine that a task i cannot terminate until all of its
	// instructions have been completely executed
	private void defineConstraintD2(IloCplex cplex) {
		try {
			for (int j = 0; j < getTasksIlpMap().size(); j++) {
				for (int k = 0; k < getVMs().size(); k++) {

					int limit = (int) Math.ceil(getTasksIlpMap().get(j).getSize() / getVMs().get(k).getMips());

					for (int t = 0; t <= limit; t++) {
						cplex.addEq(getX()[j][t][k], 0);
					}

				}
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// the constraints establish that the execution of the task j cannot start
	// until all its predecessors have been completed and the data required by
	// the task j have been transferred to the vm that will execute j
	private void defineConstraintD3(IloCplex cplex) {
		try {

			// i is the index of the node
			for (int i = 0; i < getTasksIlpMap().size(); i++) {
				for (Task child : getTasksIlpMap().get(i).getChildren()) {

					// j is the index of the child node
					int j = getInverseTaskIlpMap().get(child);

					double communicationDemands = getTasksIlpMap().get(i).getTotalBytesToSent(child);

					for (int l = 0; l < getVMs().size(); l++) {
						for (int t = 0; t <= getMaxTime(); t++) {

							// create LEFT-HAND side expressions
							IloLinearIntExpr lExpression = cplex.linearIntExpr();

							for (int k = 0; k < getVMs().size(); k++) {

								int limit = (int) Math.ceil(t
										// execution time of the child task j on
										// the virtual machine l
										- getTasksIlpMap().get(j).getSize() / getVMs().get(l).getMips()
										// transfer time among virtual
										// machine
										- communicationDemands
												/ Environment.getBandwidth(getVMs().get(k), getVMs().get(l)));

								// terms of the left-hand side expression
								for (int s = 0; s <= limit; s++) {
									lExpression.addTerm(getX()[i][s][k], 1);
								}
							}

							// create RIGHT-HAND side expressions
							IloLinearIntExpr rExpression = cplex.linearIntExpr();

							// terms of the right-hand side expression
							for (int s = 0; s <= t; s++) {
								rExpression.addTerm(getX()[j][s][l], 1);
							}

							// add the expressions
							cplex.addGe(lExpression, rExpression);

						}
					}
				}
			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	// The constraints establish that the number of task execution at
	// any given time is at least equal to the number of CPUs
	private void defineConstraintD4(IloCplex cplex) {
		try {

			for (int k = 0; k < getVMs().size(); k++) {
				for (int t = 0; t <= getMaxTime(); t++) {

					// create expressions
					IloLinearIntExpr expression = cplex.linearIntExpr();

					for (int j = 0; j < getTasksIlpMap().size(); j++) {

						// establishing a limit of time
						int limit = (int) Math.ceil(getTasksIlpMap().get(j).getSize() / getVMs().get(k).getMips());

						// the runtime of the task n cannot be grater than
						// maxTime value
						if (t + limit <= getMaxTime()) {

							for (int s = t; s <= t + limit - 1; s++) {
								expression.addTerm(getX()[j][s][k], 1);
							}
						}
					}

					// add the expressions
					cplex.addLe(expression, getVMs().get(k).getCores());
				}
			}

		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
	}

	@Override
	protected IloIntVar[][][] createVariableX(IloCplex cplex) {
		IloIntVar[][][] X = new IloIntVar[getTasksIlpMap().size()][][];
		// creating the variable X
		try {
			// loop for each dag node
			for (int n = 0; n < getTasksIlpMap().size(); n++) {
				X[n] = new IloIntVar[getMaxTime() + 1][];

				// loop for each time slot
				for (int t = 0; t <= getMaxTime(); t++) {
					X[n][t] = new IloIntVar[getVMs().size()];

					// loop for each
					for (int v = 0; v < getVMs().size(); v++) {
						String label = "X[" + n + "," + t + "," + v + "]";
						X[n][t][v] = cplex.intVar(0, 1, label);
					}
				}
			}
		} catch (IloCplexModeler.Exception e) {
			e.printStackTrace(getLogger().getPrintStream());
		} catch (IloException e) {
			e.printStackTrace(getLogger().getPrintStream());
		}
		return X;
	}

	@Override
	public String getObjectiveValueName() {
		return "makespan";
	}

	@Override
	public String getExtraResults(IloCplex cplex) throws UnknownObjectException, IloException {
		return "";
	}

	@Override
	protected void createOtherVariables(IloCplex cplex) {

	}

}
