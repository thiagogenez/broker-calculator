package cs.scheduling.algorithms.statizz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;

import org.javatuples.Pair;

import cs.core.dag.DAG;
import cs.core.dag.DAGFile;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.job.Job;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.Transmission;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;

public class PCH extends StaticAlgorithm {

	private Map<Task, ScheduleEstimation> EST;

	private Pair<VMType, Double> bestResource;

	private Map<VMType, List<Double>> TIME;

	private Map<VMType, TreeSet<Task>> machines;

	private List<TreeSet<Task>> clusters;

	private TreeMap<Task, VMType> schedule;

	public PCH(DAG dag, double deadline, Environment environment) {
		super(dag, deadline, environment);
	}

	/****************************
	 *** Scheduling functions ***
	 ****************************/

	private void printPriorities() {

		logln("Priorities: ");
		for (String taskName : getDAG().getTasksOrdedByUpwardRank()) {
			Task task = getDAG().getTaskById(taskName);
			logln(task + " => P:" + task.getUpwardRank());
		}
	}

	public void printSchedule() {
		StringBuilder string = new StringBuilder();

		for (String taskName : getDAG().getTasksOrdedByUpwardRank()) {
			Task task = getDAG().getTaskById(taskName);
			VMType vmType = schedule.get(task);
			string.append(String.format("%1$-15s  s: %2$-8s d: %3$-8s f: %4$-8s  =>  %5$s\n", task, EST(task, vmType),
					W(task, vmType), EFT(task, vmType), vmType));
		}
		logln(string.toString());

	}

	/**
	 * 
	 * @return The best resource that contains the highest MIPs
	 */
	private Pair<VMType, Double> getBestResource() {
		VMType fastest = null;
		double mips = 0.0;

		for (VMType vmType : getAllVMTypes()) {
			if (vmType.getMips() > mips) {
				fastest = vmType;
				mips = vmType.getMips();
			}
		}

		double highSpeedbandwidth = 0.0;

		for (VMType vmType : getAllVMTypes()) {
			highSpeedbandwidth = Math.max(highSpeedbandwidth,
					Math.max(vmType.getInternalBandwidth(), vmType.getExternalBandwidth()));
		}

		// Creating a fake resource that is a clone of the fastest resource
		VMType fake = new VMType(fastest.getMips(), fastest.getCores(), fastest.getPriceForBillingUnit(),
				fastest.getBillingTimeInSeconds(), fastest.getInternalBandwidth(), fastest.getExternalBandwidth(),
				fastest.getId(), fastest.getDatacenterId(), fastest.getDataTransferInPrice(),
				fastest.getDataTransferOutPrice(), fastest.getDataTransferUnit(), "fake");

		return new Pair<VMType, Double>(fake, highSpeedbandwidth);
	}

	/*
	 * Priority, where we use the best resource as r in the paper, because it is the
	 * initial values to calculate P
	 */
	private double priority(Task task) {

		// if the priority value of the current task has already been calculate,
		// return it
		if (task.getUpwardRank() >= 0) {
			return task.getUpwardRank();
		}

		// otherwise, calculate it
		// by using the best resource of the environment system
		// according with the paper
		double w = W(task, bestResource.getValue0());

		// the max value (priority + communication costs)
		// of the current task's children
		double max = 0.0;

		for (Task child : task.getChildren()) {

			// the communication cost of the edge
			// between the current task and its child
			double communicationCost = C(task, bestResource.getValue0(), child, bestResource.getValue0());

			max = Math.max(max, communicationCost +
			// priority value of the current task's child
					priority(child));
		}

		// storing the priority value of the current task
		task.setUpwardRank(w + max);

		// returning the rankU value of the current task
		return w + max;
	}

	/**
	 * Calculation of the computation cost (Time)
	 * 
	 * @param task
	 *            A task of the DAG
	 * @param vmType
	 *            A VM where the task will run
	 * @return the runtime of the task on the VM vmType
	 */
	double W(Task task, VMType vmType) {
		return Environment.getPredictedRuntime(task, vmType);
	}

	/**
	 * Calculation of the communication cost (Time)
	 * 
	 * @param parent
	 *            A task of the DAG
	 * @param i
	 *            A VM where the parent task will run
	 * @param child
	 *            A child task of the parent task
	 * @param j
	 *            A VM where the child task will run
	 * @return The time to transfer all dependencies of the parent task to its child
	 *         task
	 */
	private double C(Task parent, VMType i, Task child, VMType j) {

		double bytes = parent.getTotalBytesToSent(child);
		double bandwith = 0.0;

		if (i.equals(bestResource.getValue0()) && j.equals(bestResource.getValue0())) {
			bandwith = bestResource.getValue1();
		} else {
			bandwith = Environment.getBandwidth(i, j);
		}

		// answer in time
		return bytes / bandwith;
	}

	/**
	 * Calculation of the Estimated Finish Time
	 * 
	 * @param t
	 *            A task of the DAG
	 * @param v
	 *            A VM where the task will run
	 * @return the Estimated Finish Time of the execution of the task t the VM v v
	 */
	double EFT(Task t, VMType v) {
		return EST(t, v) + W(t, v);
	}

	/**
	 * Calculation of the Earliest Start Time
	 * 
	 * @param task
	 *            A task of the DAG
	 * @param vmType
	 *            A VM where the task will run
	 * @return The Earliest Start Time of the execution of the task <code>i</code>
	 *         on the VM n
	 */
	double EST(Task task, VMType vmType) {

		// if the EST value of the current task has been calculate,
		// return it
		if (EST.containsKey(task)) {
			return EST.get(task).getStartExecutionTime();
		}

		// otherwise, calculate it
		// Max value starting to me the time value when the VM n is free
		double max = getTime(vmType);
		for (Task parent : task.getParents()) {

			// get where the parent task is scheduled
			VMType scheduledParent = schedule.get(parent);

			// communication demand between the current task i and its parent
			double c = C(parent, scheduledParent, task, vmType);

			// get the EST value of the parent task
			double EST = EST(parent, scheduledParent);

			// get the computation demand of the parent task
			double W = W(parent, scheduledParent);

			max = Math.max(max, c + EST + W);
		}

		double runtime = W(task, vmType);

		ScheduleEstimation estimation = new ScheduleEstimation(max, runtime, max + runtime, vmType);

		// storing the EST value of the task i on the VM n
		EST.put(task, estimation);
		setTime(vmType, max + W(task, vmType));

		// return the EST value
		return max;
	}

	private void setTime(VMType vmType, double time) {
		if (!vmType.equals(bestResource.getValue0())) {
			List<Double> cores = TIME.get(vmType);
			int index = cores.indexOf(Collections.min(TIME.get(vmType)));
			cores.remove(index);
			cores.add(time);
		}
	}

	private double getTime(VMType vmType) {
		if (vmType.equals(bestResource.getValue0())) {
			return 0.0;
		}
		return Collections.min(TIME.get(vmType));
	}

	private Pair<Task, VMType> getTheScheduledChild(Task parent) {
		for (Task child : parent.getChildren()) {
			VMType vmType = schedule.get(child);
			if (!vmType.equals(bestResource.getValue0())) {
				return new Pair<Task, VMType>(child, vmType);
			}
		}
		return null;
	}

	private void calculatePriorityValues() {
		for (String entry : getDAG().getEntryTasks()) {
			Task task = getDAG().getTaskById(entry);
			priority(task);
		}
	}

	// most used by the HCOC
	public void deallocating(Task task) {
		VMType vmType = schedule.get(task);
		machines.get(vmType).remove(task);
		schedule.remove(task);
	}

	// most used by the HCOC
	public void allocating(Task task, VMType vmType) {
		machines.get(vmType).add(task);
		schedule.put(task, vmType);
	}

	// Used by the PCH
	private TreeSet<Task> allocating(TreeSet<Task> cluster, VMType vmType) {
		// allocating the cluster on the VmType
		TreeSet<Task> newCluster = machines.get(vmType);
		newCluster.addAll(cluster);

		// changing the schedule
		for (Task task : cluster) {
			schedule.put(task, vmType);
		}

		return newCluster;
	}

	// Used by the PCH
	private void deallocating(TreeSet<Task> cluster) {
		// restoring to the initial fake scheduling
		for (Task task : cluster) {
			VMType vmType = schedule.put(task, bestResource.getValue0());
			machines.get(vmType).remove(task);
		}
	}

	public void instanciateMachine(VMType vmType) {
		if (!machines.containsKey(vmType)) {
			TreeSet<Task> cluster = new TreeSet<Task>(Collections.reverseOrder());
			machines.put(vmType, cluster);

			TIME.put(vmType, new ArrayList<Double>(Collections.nCopies(vmType.getCores(), 0.0)));
		}
	}

	public void update() {
		// clear the EST information
		this.EST.clear();

		// Reset the clock of the VM
		for (VMType vmType : getAllVMTypes()) {
			TIME.put(vmType, new ArrayList<Double>(Collections.nCopies(vmType.getCores(), 0.0)));
		}

		// getting the tasks orded by the priority value
		for (String taskName : getDAG().getTasksOrdedByUpwardRank()) {
			Task task = getDAG().getTaskById(taskName);
			EST(task, schedule.get(task));
		}
	}

	private VMType getResource(TreeSet<Task> originalCluster) {
		VMType selectedVmType = null;

		double smallestEST = Double.MAX_VALUE;

		for (VMType candidateVmType : getAllVMTypes()) {

			double EST_successor = 0.0;

			// Allocate the orignal cluster into the candidate VM
			// for debugging proposes
			@SuppressWarnings("unused")
			TreeSet<Task> temporaryCluster = allocating(originalCluster, candidateVmType);

			// Update the EST of all tasks
			update();

			// get the last task of the original cluster
			Task lastClusterTask = originalCluster.last();

			// the EST of the successor (child) of the last task
			// of the original cluster is at least equal to
			// the EFT of the last task of the original
			// cluster because the last task of the original
			// cluster can be a exit task of the DAG
			EST_successor = EFT(lastClusterTask, candidateVmType);

			// otherwise, a communication cost will be exist
			// let's find out
			if (!getDAG().isExitTask(lastClusterTask)) {

				// get the scheduled child
				Pair<Task, VMType> scheduledChild = getTheScheduledChild(lastClusterTask);

				// sanity check
				if (scheduledChild == null) {
					throw new RuntimeException("A child task can not be NULL at this point of the scheduling");
				}

				// if the successor (child) and the last task of the original
				// cluster are scheduled in the same candidate VM
				// the communication cost is equal to the EST(successor)
				if (scheduledChild.getValue1().equals(candidateVmType)) {

					EST_successor = EST(scheduledChild.getValue0(), candidateVmType);
				} else {
					// otherwise, calculate it
					// communication costs
					double bandwidth = Environment.getBandwidth(candidateVmType, scheduledChild.getValue1());
					double communicationDemand = lastClusterTask.getTotalBytesToSent(scheduledChild.getValue0());

					EST_successor += communicationDemand / bandwidth;
				}
			}

			// select the candidate VmType that results
			// in the smallest EST value
			if (EST_successor < smallestEST) {
				smallestEST = EST_successor;
				selectedVmType = candidateVmType;
			}

			// restoring the original cluster for the fake scheduling
			deallocating(originalCluster);

			// Update the EST of all tasks
			update();
		}

		return selectedVmType;
	}

	/*
	 * Algorithm 2 of the paper (A performance-oriented adaptive scheduler for
	 * dependent tasks on grids)
	 */
	private TreeSet<Task> createCluster(LinkedList<Task> unscheduledTasks) {

		TreeSet<Task> cluster = new TreeSet<Task>(Collections.reverseOrder());
		Task k = unscheduledTasks.poll();
		cluster.add(k);

		Stack<Task> stack = new Stack<Task>();
		stack.add(k);

		// creating the cluster
		while (!stack.isEmpty()) {

			// starting the Depth-first search by taking the
			// task that contains the highest priority value
			Task n = stack.pop();

			double max = 0.0;
			Task selectedTask = null;

			// selecting the child task
			// by using depth-first search algorithm
			for (Task child : n.getChildren()) {

				// check if the task hasn't been scheduled already
				if (unscheduledTasks.contains(child)) {

					// As this part of the code deals only with UNSCHEDULED
					// tasks, it is necessary to take into consideration
					// the "best resource" as the "fake" resource where the task
					// where scheduled to calculate the initial attributes
					double value = child.getUpwardRank() + EST(child, bestResource.getValue0());
					if (value > max) {
						max = value;
						selectedTask = child;
					}
				}
			}

			// add the selected task to the cluster
			if (selectedTask != null) {

				// to continue the searching by the depth-first policy
				stack.add(selectedTask);
				// storing the selected task to the cluster
				cluster.add(selectedTask);
				// remove the selected task to the unscheduled task set
				unscheduledTasks.remove(selectedTask);
			}
		}

		return cluster;
	}

	public VMType getScheduledTask(Task task) {
		return schedule.get(task);
	}

	public int getNumberOfCluster(TreeSet<Task> list) {
		HashMap<TreeSet<Task>, Integer> counter = new HashMap<TreeSet<Task>, Integer>();

		for (TreeSet<Task> cluster : clusters) {
			counter.put(cluster, 0);
		}

		for (Task task : list) {
			for (TreeSet<Task> cluster : clusters) {
				if (cluster.contains(task)) {
					counter.put(cluster, 1);
					break;
				}
			}
		}

		int i = 0;

		for (Integer j : counter.values()) {
			i += j;
		}

		return i;
	}

	double getEstimatedMakespan() {
		double makespan = 0.0;
		for (String taskName : getDAG().getExitTasks()) {
			Task t = getDAG().getTaskById(taskName);
			VMType v = schedule.get(t);
			makespan = Math.max(makespan, EFT(t, v));
		}
		return makespan;
	}

	/****************************************************
	 ** Override functions used by the SCHEDULING step **
	 ****************************************************/

	@Override
	protected void onInternalBeforeSchedulingStart() {

		machines = new HashMap<VMType, TreeSet<Task>>();
		EST = new HashMap<Task, ScheduleEstimation>();
		schedule = new TreeMap<Task, VMType>();
		TIME = new HashMap<VMType, List<Double>>();
		clusters = new LinkedList<TreeSet<Task>>();

		// get the best resource
		logln("Finding best resource ...");
		bestResource = getBestResource();

		logln("Calculating priorities ...");
		// calculate tasks priorities
		calculatePriorityValues();
		printPriorities();

		// FAKE SCHEDULING STEP:
		logln("Making the fake scheduling step ...");
		// By considering an infinite copy of a best resource of the system
		// scheduling each task of the DAG in each resource of the system
		for (String taskName : getDAG().getTasksOrdedByUpwardRank()) {
			Task task = getDAG().getTaskById(taskName);
			schedule.put(task, bestResource.getValue0());
		}

		// calculate initial EST
		logln("Calculating initial values for EST and EFT attributes ...");
		for (String exit : getDAG().getExitTasks()) {
			Task task = getDAG().getTaskById(exit);
			EST(task, schedule.get(task));
		}

		// reset the clock and the cluster of each VMType
		for (VMType vmType : getAllVMTypes()) {
			instanciateMachine(vmType);
		}

		// At this point,
		// PCH algorithm is ready to initiate the scheduling
		logln("PCH is ready to initiate the scheduling ...");

	}

	@Override
	protected Plan planDAG(Plan currentPlan) {

		// set of unscheduled tasks
		// sorted by priority value
		LinkedList<Task> unscheduledTasks = new LinkedList<Task>();
		for (String taskName : getDAG().getTasksOrdedByUpwardRank()) {
			Task task = getDAG().getTaskById(taskName);
			unscheduledTasks.add(task);
		}

		logln("Composing and scheduling clusters  ... ");
		while (unscheduledTasks.size() > 0) {
			// get a cluster
			TreeSet<Task> cluster = createCluster(unscheduledTasks);

			clusters.add(cluster);

			// get a resource for the cluster that minimizes the EST
			// of the successor of the last task of the cluster
			VMType bestResource = getResource(cluster);

			// storing the cluster
			machines.get(bestResource).addAll(cluster);

			// allocate the cluster into the resource
			allocating(machines.get(bestResource), bestResource);

			// reset the clock
			setTime(bestResource, 0.0);

			// recalculate the values
			update();

			// logging
			logln(cluster + " => " + bestResource);
		}

		createPlan(currentPlan);

		return currentPlan;
	}

	void createPlan(Plan plan) {
		for (Task task : EST.keySet()) {
			ScheduleEstimation estimation = EST.get(task);
			plan.addScheduleEstimation(task, estimation);
		}
	}

	@Override
	protected void onInternalAfterSchedulingCompleted() {
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "PCH";
	}

	/****************************************************
	 ** Override functions used by the SCHEDULING step **
	 ****************************************************/
	@Override
	public void jobReleased(Job job) {
		Task task = job.getTask();
		job.setVM(getVM(task));
	}

	@Override
	public void jobSubmitted(Job job) {
		// do nothing
	}

	@Override
	public void jobStarted(Job job) {
		// do nothing
	}

	@Override
	public void jobFinished(Job job) {
		Task from = job.getTask();

		for (Task child : from.getChildren()) {
			for (DAGFile parentFile : from.getOutputFiles()) {
				for (DAGFile childFile : child.getInputFiles()) {

					if (parentFile.equals(childFile)) {
						Transmission t = new Transmission(child, parentFile);
						job.addTransmission(t.getReceiver(), t);
					}
				}
			}
		}
	}

}
