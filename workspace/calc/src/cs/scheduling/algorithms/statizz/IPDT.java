package cs.scheduling.algorithms.statizz;

import cs.core.dag.DAG;
import cs.core.engine.Environment;
import cs.scheduling.algorithms.statizz.ilp.ipdt.BatistaAlgorithm;
import cs.scheduling.algorithms.statizz.ilp.ipdt.BatistaILP;
import cs.scheduling.algorithms.statizz.ilp.ipdt.IPDT_ILP;

public class IPDT extends BatistaAlgorithm {

	private int nodeLim;

	private double tiLim;

	private int threads;

	private double epgap;

	public IPDT(DAG dag, double deadline, Environment environment, int nodeLim, double tiLim, int threads,
			double epgap) {
		super(dag, deadline, environment);
		this.tiLim = tiLim;
		this.nodeLim = nodeLim;
		this.threads = threads;
		this.epgap = epgap;
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "IPDT";
	}

	@Override
	public BatistaILP createILP() {
		return new IPDT_ILP(getDAG(), getAllVMTypes(), getLogger(), this.nodeLim, this.tiLim, this.threads, this.epgap);
	}

}