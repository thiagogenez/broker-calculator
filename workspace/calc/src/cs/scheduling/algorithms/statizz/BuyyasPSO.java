
package cs.scheduling.algorithms.statizz;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.javatuples.Pair;
import org.javatuples.Triplet;

import cs.core.dag.DAG;
import cs.core.dag.DAGFile;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.job.Job;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.Transmission;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;
import cs.scheduling.algorithms.statizz.plan.ScheduleEstimation;
import cs.scheduling.algorithms.statizz.pso.PSO;
import cs.scheduling.algorithms.statizz.pso.WorkflowFitnessFunction;
import cs.scheduling.algorithms.statizz.pso.WorkflowParticle;
import net.sourceforge.jswarm_pso.Swarm;

/**
 * Static-PSO modified from the paper: "A particle swarm optimization-based
 * heuristic for scheduling workflow applications in cloud computing
 * environments,"
 * 
 * Authors: S. Pandey, L. Wu, S. Guru, and R. Buyya
 * 
 * Conference: IEEE International Conference on Advanced Information Networking
 * and Applications, april 2010, pp. 400 –407.
 * 
 * @author thiagogenez <thiagogenez@ic.unicamp.br>
 */
public class BuyyasPSO extends StaticAlgorithm implements PSO {

	private HashMap<Integer, VMType> vmTypePointer;

	private HashMap<Integer, Task> taskPointer;

	public BuyyasPSO(DAG dag, double deadline, Environment environment) {
		super(dag, deadline, environment);

	}

	/****************************
	 *** Scheduling functions ***
	 ****************************/

	private void createPointer() {
		this.vmTypePointer = new HashMap<Integer, VMType>();
		this.taskPointer = new HashMap<Integer, Task>();

		int id = 0;

		// creating VMtype hash map
		for (VMType vmType : getAllVMTypes()) {
			vmTypePointer.put(id, vmType);
			id++;
		}

		List<Task> tasks = new ArrayList<Task>();

		// create a list of tasks
		for (String taskName : getDAG().getTasks()) {
			tasks.add(getDAG().getTaskById(taskName));
		}

		// sort in accordance with default comparator
		// that is basically by the taskId string
		Collections.sort(tasks, Collections.reverseOrder());

		id = 0;
		for (Task task : tasks) {
			taskPointer.put(id, task);
			id++;
		}
	}

	private void setTime(VMType vmType, double time, Map<VMType, List<Double>> TIME) {
		List<Double> cores = TIME.get(vmType);
		int index = cores.indexOf(Collections.min(TIME.get(vmType)));
		cores.remove(index);
		cores.add(time);
	}

	private double W(Task task, VMType vmType) {
		return Environment.getPredictedRuntime(task, vmType);
	}

	private double C(Task parent, VMType i, Task child, VMType j) {
		// answer in time
		return parent.getTotalBytesToSent(child) / Environment.getBandwidth(i, j);
	}

	private Pair<Map<Task, VMType>, Map<VMType, List<Task>>> parser(int[] position) {

		Map<VMType, List<Task>> resources = new HashMap<VMType, List<Task>>();
		Map<Task, VMType> schedule = new HashMap<Task, VMType>();

		// initialize the schedule map

		// for each TASK pointer ID
		for (int taskId = 0; taskId < position.length; taskId++) {
			// getting the VM pointer ID
			int vmId = (int) position[taskId];

			// get the VM according with the VM pointer ID
			VMType vmType = vmTypePointer.get(vmId);
			resources.put(vmType, new ArrayList<Task>());
		}

		// Fulfill the schedule map

		// for each TASK pointer ID
		for (int taskId = 0; taskId < position.length; taskId++) {

			// getting the VM pointer ID
			int vmId = (int) position[taskId];

			// get the data
			Task task = taskPointer.get(taskId);
			VMType vmType = vmTypePointer.get(vmId);

			// organizing the data
			resources.get(vmType).add(task);
			schedule.put(task, vmType);
		}

		return new Pair<Map<Task, VMType>, Map<VMType, List<Task>>>(schedule, resources);
	}

	private Pair<Map<Task, Triplet<Double, Double, Double>>, Double> calculateRuntimeInformation(
			Map<Task, VMType> schedule, Set<VMType> resources) {

		// memory where the runtime information will be stored
		Map<Task, Triplet<Double, Double, Double>> runtimeInformation = new HashMap<Task, Triplet<Double, Double, Double>>();

		// Clock of each VM
		Map<VMType, List<Double>> TIME = new HashMap<VMType, List<Double>>();
		double makespan = 0.0;

		// zero the clock of each VM
		for (VMType vmType : resources) {
			TIME.put(vmType, new ArrayList<Double>(Collections.nCopies(vmType.getCores(), 0.0)));
		}

		// starting the calculation by taking the exit tasks
		for (String taskName : getDAG().getExitTasks()) {

			// get a exit task
			Task exit = getDAG().getTaskById(taskName);

			// calculate the runtime information
			// in a bottom up approach
			Triplet<Double, Double, Double> triplet = calculator(exit, schedule.get(exit), runtimeInformation, TIME,
					schedule);

			// as the runtime information of a exit task is
			// being calculated here, so take the makespan value
			// of this schedule
			makespan = Math.max(makespan, triplet.getValue2());

		}

		// wrapping

		// returning the runtime information of each task of the DAG
		// and the makespan value of the schedule
		return new Pair<Map<Task, Triplet<Double, Double, Double>>, Double>(runtimeInformation, makespan);
	}

	private Triplet<Double, Double, Double> calculator(Task task, VMType vmType,
			Map<Task, Triplet<Double, Double, Double>> memory, Map<VMType, List<Double>> TIME,
			Map<Task, VMType> schedule) {

		// if the memory contains the runtime information of the current task,
		// return it
		if (memory.containsKey(task)) {
			return memory.get(task);
		}

		// otherwise, calculate it

		// max value is the EST of the current task, which is equal
		// to the next free slot available on the VM to execute the current task
		double max = Collections.min(TIME.get(vmType));

		// checking the runtime information of the parent's task
		for (Task parent : task.getParents()) {

			// get where the parent task is scheduled
			VMType scheduledParent = schedule.get(parent);

			// get the runtime information of the parent task
			Triplet<Double, Double, Double> parentsRuntimeInformation = calculator(parent, scheduledParent, memory,
					TIME, schedule);

			// calculate the communication demand between
			// the current task i and its parent
			double c = C(parent, scheduledParent, task, vmType);

			// EST of the current task is equal to the max value
			max = Math.max(max, c + parentsRuntimeInformation.getValue0() + parentsRuntimeInformation.getValue1());
		}

		// storing the runtime information of the current task in the memory
		Triplet<Double, Double, Double> runtimeInformation = new Triplet<Double, Double, Double>(max, W(task, vmType),
				max + W(task, vmType));
		memory.put(task, runtimeInformation);

		// occupy the available slot for this task on the VM
		setTime(vmType, max + W(task, vmType), TIME);

		// return the runtime information of the current task
		return runtimeInformation;
	}

	// calculating the fitness value for the particle's position
	@Override
	public double calculateFitnessValue(int[] position, Plan currentPlan) {

		// parse the schedule received from the particle's position
		Pair<Map<Task, VMType>, Map<VMType, List<Task>>> parsed = parser(position);

		// schedule parsed
		Map<Task, VMType> schedule = parsed.getValue0();
		Map<VMType, List<Task>> resources = parsed.getValue1();

		// calculate the runtime information and makespan value
		Pair<Map<Task, Triplet<Double, Double, Double>>, Double> pair = calculateRuntimeInformation(schedule,
				resources.keySet());

		// unwrap the pair object
		Map<Task, Triplet<Double, Double, Double>> runtimeInformation = pair.getValue0();
		double makespan = pair.getValue1();

		// if the makespan is greater than the deadline
		// return the MAX_VALUE as the fitness value
		if (makespan > getDeadline()) {
			return Double.MAX_VALUE;
		}

		// otherwise, lets return as a fitness value
		// the relation between total_costs / makespan

		// calculating the monetary cost of each VM
		double totalDemandExecutionCost = 0.0;
		double totalDemandTransferCosts = 0.0;
		for (VMType vmType : resources.keySet()) {

			// getting the total costs
			Pair<Double, Double> costs = getTotalCost(vmType, resources.get(vmType), runtimeInformation, schedule);

			totalDemandExecutionCost += costs.getValue0();
			totalDemandTransferCosts += costs.getValue1();

		}

		// if the total execution costs is small
		// return as the fitness value the makespan of the schedule
		if (totalDemandExecutionCost <= 0) {
			return makespan;
		}

		return (totalDemandExecutionCost + totalDemandTransferCosts) / makespan;
	}

	// calculating the overal monetary costs of this VM
	private Pair<Double, Double> getTotalCost(VMType vmType, List<Task> cluster,
			Map<Task, Triplet<Double, Double, Double>> executionTimeInformation, Map<Task, VMType> schedule) {

		// execution cost
		double executionDemandCost = getExecutionCost(vmType, cluster, executionTimeInformation, schedule);

		// data in/out costs
		double transferDemandCost = getDataInOutCost(vmType, cluster);

		// total costs involved in this vm
		return new Pair<Double, Double>(executionDemandCost, transferDemandCost);
	}

	// calculating the total execution costs of this VM
	private double getExecutionCost(VMType vmType, List<Task> cluster,
			Map<Task, Triplet<Double, Double, Double>> executionTimeInformation, Map<Task, VMType> schedule) {

		double start = Double.MAX_VALUE;
		double finish = 0.0;

		// calculating the computation time in this VM
		for (Task task : cluster) {
			Triplet<Double, Double, Double> taskRuntimeInformation = executionTimeInformation.get(task);

			start = Math.min(start, taskRuntimeInformation.getValue0());
			finish = Math.max(finish, taskRuntimeInformation.getValue2());

			for (Task father : task.getParents()) {
				// if a father task scheduled on different VM
				if (!cluster.contains(father)) {
					Triplet<Double, Double, Double> fatherRuntimeInformation = executionTimeInformation.get(father);
					start = Math.min(start, fatherRuntimeInformation.getValue2());
				}
			}

			for (Task child : task.getChildren()) {
				// if a child task scheduled on different VM
				if (!cluster.contains(child)) {
					VMType childExecution = schedule.get(child);
					double transferTime = C(task, vmType, child, childExecution);
					finish = Math.max(finish, taskRuntimeInformation.getValue2() + transferTime);
				}

			}

		}

		// sanity check
		if (finish - start < 0) {
			throw new RuntimeException("A negative value can not be assigned as a runtime value");
		}

		// calculating full-unit-times for the bill
		double billingUnits = (finish - start) / vmType.getBillingTimeInSeconds();

		// calculating how many full units were used
		double fullBillingUnits = Math.ceil(billingUnits);

		// return the execution costs
		return fullBillingUnits * vmType.getPriceForBillingUnit();
	}

	// calculating the total data transfer IN and OUT of this VM
	private double getDataInOutCost(VMType vmType, List<Task> cluster) {

		double bytesIn, bytesOut;
		bytesIn = bytesOut = 0.0;

		for (Task currentTask : cluster) {

			// calculate bytes-out sent to a child that was not scheduled in
			// this VMtype
			for (Task child : currentTask.getChildren()) {

				// if a child task scheduled on different VM
				if (!cluster.contains(child)) {
					bytesOut += currentTask.getTotalBytesToSent(child);
				}

			}

			// calculate bytes received from a father that was not scheduled in
			// this VMtype
			for (Task father : currentTask.getParents()) {

				// if a father task scheduled on different VM
				if (!cluster.contains(father)) {
					bytesIn += father.getTotalBytesToSent(currentTask);
				}
			}
		}

		// calculating units of data-in
		double billingDataUnitsIn = bytesIn / vmType.getDataTransferUnit();

		// calculating units of data-out
		double billingDataUnitsOut = bytesOut / vmType.getDataTransferUnit();

		// calculating the monetary transfer costs (in + out)
		return (billingDataUnitsIn * vmType.getDataTransferInPrice())
				+ (billingDataUnitsOut * vmType.getDataTransferOutPrice());
	}

	@SuppressWarnings("unused")
	private void print(Map<Task, Triplet<Double, Double, Double>> runtimeInformation, Map<Task, VMType> schedule) {
		StringBuilder string = new StringBuilder();

		for (Task task : getDAG().getTopologicalOrder()) {
			VMType vmType = schedule.get(task);

			string.append(task + " s: " + runtimeInformation.get(task).getValue0() + " d: "
					+ runtimeInformation.get(task).getValue1() + " f: " + runtimeInformation.get(task).getValue2()
					+ " => " + vmType + "\n");
		}

		System.out.println(string.toString());

	}

	public Plan pso(WorkflowFitnessFunction fitnessFunction, WorkflowParticle particle) {

		// Initialize the swarm
		logln(">>> Creating the swarm ...");
		Swarm swarm = new Swarm(NUMBER_OF_PARTICLES, particle, fitnessFunction);

		// setting the configuration of the particle's positions
		swarm.setMaxPosition(getAllVMTypes().size() - 1);
		swarm.setMinPosition(0);

		// evolve the swarm
		logln(">>> Evolving the swarm ... ");
		for (int i = 0; i < MAX_ITERATION; i++) {
			swarm.evolve();
		}
		logln(">>> Swarm evolved ...");

		logln(">>> Stats: ");

		int[] solution = fitnessFunction.round(swarm.getBestPosition());

		String stats = swarm.toStringStats();
		stats.substring(0, stats.length() - 1);
		stats += "Best position rounded: \t[";
		for (int i = 0; i < solution.length; i++) {
			stats += solution[i] + (i < (solution.length - 1) ? ", " : "");
		}
		stats += "]\n";

		logln("\n" + stats);

		return fitnessFunction.getSavedPlan();

	}

	/****************************************************
	 ** Override functions used by the SCHEDULING step **
	 ****************************************************/

	@Override
	protected Plan planDAG(Plan currentPlan) {
		// Step 5: Compute PSO({ a set of all tasks }) Initialize the readyTasks
		// vector with the list of all tasks

		WorkflowFitnessFunction fitnessFunction = new WorkflowFitnessFunction(this, false, currentPlan,
				getAllDags().size());

		WorkflowParticle particle = new WorkflowParticle(getDimension());

		return pso(fitnessFunction, particle);
	}

	private Plan createPlan(Plan currentPlan, int[] solution) {
		Plan other = PlanFactory.getEmptyPlan(currentPlan);
		// parse the schedule received from the particle's position
		Pair<Map<Task, VMType>, Map<VMType, List<Task>>> parsed = parser(solution);

		// schedule parsed
		Map<Task, VMType> schedule = parsed.getValue0();
		Map<VMType, List<Task>> resources = parsed.getValue1();

		// calculate the runtime information and makespan value
		Pair<Map<Task, Triplet<Double, Double, Double>>, Double> pair = calculateRuntimeInformation(schedule,
				resources.keySet());

		// unwrap the pair object
		Map<Task, Triplet<Double, Double, Double>> runtimeInformation = pair.getValue0();

		// schedule the tasks
		for (Task task : getTopologicalOrder()) {
			// schedule the task on the resource represented by the VMType
			double startExecutionTime = runtimeInformation.get(task).getValue0();
			double runtime = runtimeInformation.get(task).getValue1();
			double finishExecutionTime = runtimeInformation.get(task).getValue2();
			VMType vmType = schedule.get(task);

			ScheduleEstimation estimation = new ScheduleEstimation(startExecutionTime, runtime, finishExecutionTime,
					vmType);
			other.addScheduleEstimation(task, estimation);
		}

		return other;
	}

	@Override
	protected void onInternalBeforeSchedulingStart() {
		createPointer();
	}

	@Override
	protected void onInternalAfterSchedulingCompleted() {

	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "PSO";
	}

	/***************************************************
	 ** Override functions used by the SIMULATOR step **
	 ***************************************************/

	@Override
	public void jobReleased(Job job) {
		Task task = job.getTask();
		job.setVM(getVM(task));
	}

	@Override
	public void jobSubmitted(Job job) {
		// do nothing

	}

	@Override
	public void jobStarted(Job job) {
		// do nothing

	}

	@Override
	public void jobFinished(Job job) {
		Task from = job.getTask();

		for (Task child : from.getChildren()) {
			for (DAGFile parentFile : from.getOutputFiles()) {
				for (DAGFile childFile : child.getInputFiles()) {

					if (parentFile.equals(childFile)) {
						Transmission t = new Transmission(child, parentFile);
						job.addTransmission(t.getReceiver(), t);
					}
				}
			}
		}
	}

	@Override
	public int getDimension() {
		// Set particle dimension as equal to the size of ready tasks
		return getDAG().getNumberOfTasks();
	}

}
