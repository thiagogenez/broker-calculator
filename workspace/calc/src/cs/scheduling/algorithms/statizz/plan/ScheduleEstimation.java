package cs.scheduling.algorithms.statizz.plan;

import java.util.LinkedList;
import java.util.List;

import cs.core.dag.DAGFile;
import cs.core.dag.Task;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.Transmission;

public class ScheduleEstimation implements Comparable<ScheduleEstimation> {
	private double startExecutionTime;

	private double runtime;

	private double finishExecutionTime;

	private String resourceId;

	private VMType vmType;

	private List<Transmission> transmissions;

	public ScheduleEstimation(double startExecutionTime, double runtime, double finishExecutionTime, VMType vmType) {
		this.startExecutionTime = startExecutionTime;
		this.runtime = runtime;
		this.finishExecutionTime = finishExecutionTime;
		this.vmType = vmType;
		this.transmissions = new LinkedList<>();
	}

	public void delayExecution(double delay) {
		if (delay < 0) {
			throw new RuntimeException("Delay execution time can not be negative: " + delay);
		}

		this.startExecutionTime += delay;
		this.finishExecutionTime += delay;

	}

	public void addTransmission(Task to, DAGFile dagFile, double startTime, double duration) {
		Transmission transmission = new Transmission(to, dagFile, startTime, duration);
		transmissions.add(transmission);
	}

	public List<Transmission> getTransmissions() {
		return transmissions;
	}

	public double getStartExecutionTime() {
		return startExecutionTime;
	}

	public double getRuntime() {
		return runtime;
	}

	public double getFinishExecutionTime() {
		return finishExecutionTime;
	}

	public VMType getVmType() {
		return vmType;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String id) {
		this.resourceId = id;
	}

	@Override
	public String toString() {
		return vmType + ":<" + startExecutionTime + ";" + runtime + ";" + finishExecutionTime + ">";
	}

	@Override
	public int compareTo(ScheduleEstimation o) {
		// checking by starting execution time
		if (this.getStartExecutionTime() < o.getStartExecutionTime()) {
			return -1;
		} else if (this.getStartExecutionTime() > o.getStartExecutionTime()) {
			return 1;
		}

		return 0;
	}


}
