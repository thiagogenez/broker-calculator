package cs.scheduling.algorithms.statizz.plan;

public class PlanFactory {

	public static Plan getEmptyPlan(Plan template) {
		return new Plan(template);
	}

	public static Plan clone(Plan original) {
		return original.clone();
	}
}
