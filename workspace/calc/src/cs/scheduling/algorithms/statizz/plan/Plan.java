package cs.scheduling.algorithms.statizz.plan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.dag.algorithms.TopologicalOrder;
import cs.core.engine.Environment;
import cs.core.utils.Statistics;
import cs.core.vm.VMSlot;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.VM;
import cs.core.vm.simulation.VMFactory;

public class Plan implements Cloneable {

	private HashMap<VMType, List<Resource>> resourcesPerType;

	private LinkedHashMap<String, Resource> resourcesTable;

	private HashMap<DAG, SortedMap<Task, ScheduleEstimation>> scheduleInformation;

	private HashMap<DAG, Double> scheduledEST;

	private HashMap<DAG, Double> scheduledEFT;

	private HashSet<DAG> dags;

	private int resourceId = 0;

	private double fitnessValue;

	public Plan() {
		this.scheduleInformation = new HashMap<DAG, SortedMap<Task, ScheduleEstimation>>();
		this.scheduledEST = new HashMap<DAG, Double>();
		this.scheduledEFT = new HashMap<DAG, Double>();
		this.resourcesTable = new LinkedHashMap<String, Resource>();
		this.resourcesPerType = new HashMap<VMType, List<Resource>>();
		this.dags = new HashSet<DAG>();
	}

	@Override
	protected Plan clone() {

		Plan clonePlan = PlanFactory.getEmptyPlan(this);

		for (DAG dag : this.scheduleInformation.keySet()) {
			SortedMap<Task, ScheduleEstimation> schedules = this.scheduleInformation.get(dag);

			for (Task t : schedules.keySet()) {
				ScheduleEstimation estimation = schedules.get(t);
				clonePlan.addScheduleEstimation(t, estimation);

			}
		}

		return clonePlan;
	}

	public SortedMap<Task, ScheduleEstimation> getSchedules(DAG dag) {
		SortedMap<Task, ScheduleEstimation> schedules = new TreeMap<Task, ScheduleEstimation>(Task.COMPARE_BY_ID);

		for (Task t : scheduleInformation.get(dag).keySet()) {
			ScheduleEstimation estimation = scheduleInformation.get(dag).get(t);

			schedules.put(t, estimation);
		}

		return schedules;
	}

	protected Plan(Plan other) {
		this();

		for (VMType vmType : other.resourcesPerType.keySet()) {

			List<Resource> resources = other.resourcesPerType.get(vmType);
			List<Resource> newCopies = new LinkedList<Resource>();

			for (Resource resource : resources) {

				Resource newCopy = resource.createNewEmptyCopy();
				newCopies.add(newCopy);

				if (this.resourcesTable.containsKey(newCopy.getId())) {
					throw new RuntimeException("Resource " + newCopy + " already exists!!!");
				}

				// adding the clone one
				this.resourcesTable.put(newCopy.getId(), newCopy);
			}

			if (this.resourcesPerType.containsKey(vmType)) {
				throw new RuntimeException("vmType " + vmType + " already exists!!!");
			}

			// adding the clones
			this.resourcesPerType.put(vmType, newCopies);
		}

	}

	public void createResource(VMType vmType) {
		createResource(vmType, true);
	}

	public void createResource(VMType vmType, boolean onlyDifferentTypos) {

		// only different resources can be created
		if (resourcesPerType.containsKey(vmType) && onlyDifferentTypos) {
			return;
		}

		// equal resources can be created
		Resource resource = new Resource(vmType, String.valueOf(resourceId++));

		// get the list
		List<Resource> list = resourcesPerType.get(vmType);
		if (list == null) {
			list = new LinkedList<Resource>();
			resourcesPerType.put(vmType, list);
		}
		list.add(resource);

		resourcesTable.put(resource.getId(), resource);
	}

	public ScheduleEstimation getScheduleEstimation(Task task) {

		DAG dag = task.getDAG();

		if (!scheduleInformation.get(dag).containsKey(task)) {
			return null;
		}

		return scheduleInformation.get(task.getDAG()).get(task);
	}

	public void addScheduleEstimation(Task task, ScheduleEstimation estimation) {

		if (!this.scheduleInformation.containsKey(task.getDAG())) {
			this.scheduleInformation.put(task.getDAG(), new TreeMap<Task, ScheduleEstimation>(Task.COMPARE_BY_ID));
		}

		scheduleInformation.get(task.getDAG()).put(task, estimation);

		submit(task, estimation);

		savingTimeInformation(task, estimation);
	}

	private void savingTimeInformation(Task task, ScheduleEstimation estimation) {

		DAG dag = task.getDAG();

		if (!dags.contains(dag)) {
			dags.add(dag);
		}

		if (!scheduledEST.containsKey(dag)) {
			scheduledEST.put(dag, Double.MAX_VALUE);
		}

		if (!scheduledEFT.containsKey(dag)) {
			scheduledEFT.put(dag, 0.0);
		}

		if (dag.isEntryTask(task)) {
			double startTime = estimation.getStartExecutionTime();
			startTime = Math.min(this.scheduledEST.get(dag), startTime);
			this.scheduledEST.put(dag, startTime);
		}

		if (dag.isExitTask(task)) {
			double finishTime = estimation.getFinishExecutionTime();
			finishTime = Math.max(this.scheduledEFT.get(dag), finishTime);
			this.scheduledEFT.put(dag, finishTime);
		}

	}

	public double getOverallMakespan() {

		Statistics begin = new Statistics(this.scheduledEST.values());
		Statistics end = new Statistics(this.scheduledEFT.values());

		return end.getMax() - begin.getMin();
	}

	public HashMap<DAG, Double> getMakespan() {
		HashMap<DAG, Double> makespans = new HashMap<DAG, Double>();

		for (DAG dag : dags) {
			makespans.put(dag, getMakespan(dag));
		}
		return makespans;
	}

	public Double getMakespan(DAG dag) {

		double makespan = scheduledEFT.get(dag) - scheduledEST.get(dag);

		// sanity check
		if (makespan < 0) {
			throw new RuntimeException("The calculation of the makespan is not ready yet!!!!");
		}

		return makespan;
	}

	public HashMap<DAG, Double> getScheduledFET() {
		HashMap<DAG, Double> fet = new HashMap<>();
		for (DAG dag : dags) {
			fet.put(dag, scheduledEFT.get(dag));
		}

		return fet;
	}

	public HashMap<DAG, Double> getScheduledSET() {
		HashMap<DAG, Double> set = new HashMap<>();
		for (DAG dag : dags) {
			set.put(dag, scheduledEST.get(dag));
		}

		return set;
	}

	private void submit(Task task, ScheduleEstimation estimation) {
		List<Resource> resources = resourcesPerType.get(estimation.getVmType());

		estimation.setResourceId(null);

		if (resources == null || resources.isEmpty()) {
			throw new RuntimeException("Resource not created by the scheduling algorithm ");
		}

		for (Resource resource : resources) {
			boolean scheduled = resource.schedule(task, estimation.getStartExecutionTime(), estimation.getRuntime());

			if (scheduled) {
				estimation.setResourceId(resource.getId());
				return;
			}
		}

		// delay the execution
		for (Resource resource : resources) {
			double nextTime = resource.getEarliestIdleTimeFrom(estimation.getStartExecutionTime(),
					estimation.getRuntime());

			boolean scheduled = resource.schedule(task, nextTime, estimation.getRuntime());
			if (scheduled) {
				estimation.setResourceId(resource.getId());
				double delay = nextTime - estimation.getStartExecutionTime();
				estimation.delayExecution(delay);
				return;
			}
		}

		if (estimation.getResourceId() == null) {
			throw new RuntimeException("Resource NULL");
		}

		// sanity check
		throw new RuntimeException("Scheduling fail. The task " + task.getId()
				+ " can not be scheduled to be run in this resource at time " + estimation.getStartExecutionTime()
				+ " on a VMType " + estimation.getVmType());
	}

	public double getEarliestIdleTimeFrom(double readyTime, double runtime, VMType vmType) {
		List<Resource> resources = this.resourcesPerType.get(vmType);

		double likelyReadyTime = Double.MAX_VALUE;

		for (Resource resource : resources) {
			likelyReadyTime = Math.min(likelyReadyTime, resource.getEarliestIdleTimeFrom(readyTime, runtime));
		}

		return likelyReadyTime;
	}

	public double getFitnessValue() {
		return fitnessValue;
	}

	public void setFitnessValue(double fitnessValue) {
		this.fitnessValue = fitnessValue;
	}

	public double getExecutionCost() {

		HashSet<Resource> teste = new HashSet<>();
		for (DAG dag : dags) {
			SortedMap<Task, ScheduleEstimation> schedules = scheduleInformation.get(dag);

			for (String taskId : dag.getTasks()) {
				Task parent = dag.getTaskById(taskId);
				ScheduleEstimation scheduleParent = schedules.get(parent);
				Resource resourceParent = this.resourcesTable.get(scheduleParent.getResourceId());

				if (dag.isEntryTask(parent)) {
					resourceParent.offsetLunchTime(scheduleParent.getStartExecutionTime());
				}

				for (Task child : parent.getChildren()) {
					ScheduleEstimation scheduleChild = schedules.get(child);

					Resource resourceChild = this.resourcesTable.get(scheduleChild.getResourceId());

					double bytes = parent.getTotalBytesToSent(child);
					double bandwidth = Environment.getBandwidth(resourceParent.getVmType(), resourceChild.getVmType());

					double transferTime = bytes / bandwidth;

					resourceParent.offsetTerminateTime(scheduleParent.getFinishExecutionTime() + transferTime);

					resourceChild.offsetLunchTime(scheduleParent.getFinishExecutionTime() );

				}

				teste.add(resourceParent);
			}

		}

		double cost = 0.0;
		for (Resource resource : resourcesTable.values()) {
			if (resource.isUsed()) {
				cost += resource.getExecutionCost();
			}
		}
		return cost;
	}

	public HashMap<Task, VM> createVMs() {

		HashMap<Task, VM> taskPointerToVm = new HashMap<Task, VM>();

		// For each type of VMtype
		for (List<Resource> resources : resourcesPerType.values()) {

			// for each resource created
			for (Resource resource : resources) {

				// get the schedule of this resource
				List<VMSlot> schedule = resource.getScheduledTasks();
				// create a VM
				VM vm = VMFactory.createVM(resource.getVmType());

				// put the slots on this VM
				for (VMSlot slot : schedule) {
					Task task = slot.getObject();

					// the actual execution time calculated by the algorithm
					// and used by the simulator to sort the Jobs
					task.setActualExecutionTime(slot.getFinishExecutionTime());
					taskPointerToVm.put(task, vm);
				}
			}
		}

		// return the VMs created
		return taskPointerToVm;
	}

	public String logSchedules() {
		List<VMSlot> slots = new ArrayList<VMSlot>();

		for (List<Resource> resources : resourcesPerType.values()) {
			for (Resource r : resources) {
				slots.addAll(r.getScheduledTasks());
			}
		}

		Collections.sort(slots);

		LinkedHashMap<DAG, LinkedList<VMSlot>> schedules = new LinkedHashMap<DAG, LinkedList<VMSlot>>();
		for (DAG dag : sortMapByValue(this.scheduledEFT).keySet()) {
			schedules.put(dag, new LinkedList<VMSlot>());
		}

		for (VMSlot slot : slots) {
			schedules.get(slot.getObject().getDAG()).add(slot);
		}

		StringBuilder string = new StringBuilder();

		for (DAG dag : schedules.keySet()) {
			List<VMSlot> dagSlots = schedules.get(dag);
			string.append("--------------------------------------------------------------DAG:" + dag.getId()
					+ "--------------------------------------------------------------" + "\n");

			for (VMSlot slot : dagSlots) {
				string.append(String.format("%1$-15s  s: %2$,.3f\t d: %3$,.3f\t f: %4$,.3f\t  =>  %5$s\n",
						slot.getObject(), slot.getStartExecutionTime(), slot.getRuntime(),
						slot.getFinishExecutionTime(), this.resourcesTable.get(slot.getResourceId())));
			}

		}

		string.append("--------------------------------------------------------------"
				+ "-------------------------------------------------------------------\n");

		return string.toString();
	}

	public static <K, V extends Comparable<? super V>> Map<K, V> sortMapByValue(final Map<K, V> mapToSort) {
		List<Map.Entry<K, V>> entries = new ArrayList<Map.Entry<K, V>>(mapToSort.size());

		entries.addAll(mapToSort.entrySet());

		Collections.sort(entries, new Comparator<Map.Entry<K, V>>() {
			@Override
			public int compare(final Map.Entry<K, V> entry1, final Map.Entry<K, V> entry2) {
				return entry1.getValue().compareTo(entry2.getValue());
			}
		});

		Map<K, V> sorted = new LinkedHashMap<K, V>();
		for (Map.Entry<K, V> entry : entries) {
			sorted.put(entry.getKey(), entry.getValue());
		}
		return sorted;
	}

	public double getTransferTime(VMType to, Task scheduledParent, Task nonScheduledChild) {

		if (scheduledParent.getDAG() != nonScheduledChild.getDAG()) {
			return Double.MAX_VALUE;
		}

		DAG dag = scheduledParent.getDAG();

		ScheduleEstimation schedule = this.scheduleInformation.get(dag).get(scheduledParent);
		VMType from = schedule.getVmType();

		double bandwidth = Environment.getBandwidth(from, to);
		double bytes = scheduledParent.getTotalBytesToSent(nonScheduledChild);

		return bytes / bandwidth;
	}

	public boolean isScheduled(Task task) {
		DAG dag = task.getDAG();

		if (scheduleInformation.containsKey(dag) && scheduleInformation.get(dag).containsKey(task)) {
			return true;
		}

		return false;
	}

	public double getScheduledTotalBytesSent() {
		double fileTransferSize = 0.0;

		for (DAG dag : scheduleInformation.keySet()) {
			fileTransferSize += getScheduledTotalBytes(dag, true);
		}
		return fileTransferSize;

	}

	public double getScheduledTotalBytesReceived() {
		double fileTransferSize = 0.0;

		for (DAG dag : scheduleInformation.keySet()) {
			fileTransferSize += getScheduledTotalBytes(dag, false);
		}
		return fileTransferSize;

	}

	public double getScheduledTotalBytes(DAG dag, boolean sent) {
		double fileTransferSize = 0.0;

		if (scheduleInformation.containsKey(dag)) {
			TopologicalOrder tp = dag.getTopologicalOrder();

			for (Task parent : tp) {

				// sanity check
				if (!isScheduled(parent)) {
					throw new RuntimeException("attempting get a non-scheduled taskyet!");
				}

				// get parent schedule information
				ScheduleEstimation parentSchedule = scheduleInformation.get(dag).get(parent);

				for (Task child : parent.getChildren()) {

					// sanity check
					if (!isScheduled(child)) {
						throw new RuntimeException("attempting get a non-scheduled taskyet!");

					}

					// get child schedule information
					ScheduleEstimation childSchedule = scheduleInformation.get(dag).get(child);

					if (parentSchedule.getVmType() != childSchedule.getVmType()) {

						if (sent) {
							fileTransferSize += parent.getTotalBytesToSent(child);
						} else {
							fileTransferSize += child.getTotalBytesToReceive(parent);
						}
					}

				}
			}

		}

		return fileTransferSize;
	}

	/*
	 * Actual Start Execution Time
	 */
	public double getAST(Task task) {
		DAG dag = task.getDAG();
		ScheduleEstimation schedule = scheduleInformation.get(dag).get(task);

		return schedule.getStartExecutionTime();
	}

	/*
	 * Actual Finish Execution Time
	 */
	public double getAFT(Task task) {
		DAG dag = task.getDAG();
		ScheduleEstimation schedule = scheduleInformation.get(dag).get(task);

		return schedule.getFinishExecutionTime();
	}

}
