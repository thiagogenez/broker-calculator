package cs.scheduling.algorithms.statizz.plan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cs.core.dag.Task;
import cs.core.vm.VMCore;
import cs.core.vm.VMSlot;
import cs.core.vm.scheduling.VMType;

class Resource {
	private VMType vmType;

	private Set<VMCore> cores;

	private HashMap<Task, VMCore> taskPointerToCore;

	private String id;

	/** Time that the VM was launched */
	private double launchTime;

	/** Time that the VM was terminated */
	private double terminateTime;

	public Resource(VMType vmType, String id) {
		this.vmType = vmType;
		this.id = id;

		initialize();
	}

	// clear the schedules
	public void initialize() {
		this.cores = new HashSet<VMCore>();
		this.taskPointerToCore = new HashMap<Task, VMCore>();

		this.launchTime = Double.MAX_VALUE;
		this.terminateTime = 0.0;

		for (int i = 0; i < vmType.getCores(); i++) {
			this.cores.add(new VMCore(String.valueOf(i), id));
		}
	}

	public Resource createNewEmptyCopy() {
		Resource clone = new Resource(vmType, id);
		return clone;
	}

	// checking if has scheduled tasks
	public boolean hasTaskScheduled() {
		return !taskPointerToCore.isEmpty();
	}

	@Override
	public String toString() {
		return this.id + "=" + this.vmType.toString();
	}

	// get VmType of this resource
	public VMType getVmType() {
		return vmType;
	}

	// scheduling the task
	public boolean schedule(Task task, double startExecutionTime, double runtime) {
		VMCore core = getCore(startExecutionTime, runtime);

		if (core == null) {
			return false;
		}

		// make the schedule
		this.taskPointerToCore.put(task, core);
		core.add(task, startExecutionTime, runtime);

		launchTime = Math.min(launchTime, startExecutionTime);
		terminateTime = Math.max(terminateTime, startExecutionTime + runtime);

		return true;
	}

	@Deprecated
	public void cancel(Task task) {
		VMCore core = this.taskPointerToCore.remove(task);
		core.cancel(task);
	}

	// get a core to schedule the task
	private VMCore getCore(double startExecutionTime, double runtime) {
		// check if it is possible to schedule the task
		// in any core at the specified start execution time
		for (VMCore core : cores) {
			if (core.isFreeToScheduleAt(startExecutionTime, runtime)) {
				return core;
			}
		}

		// otherwise, it have to be scheduled
		// in the next available free slot of any core. However,
		// this is a problem to be solved by the
		// scheduling algorithm. Not in this level of the simulator
		return null;
	}

	public List<VMSlot> getScheduledTasks() {
		List<VMSlot> schedule = new ArrayList<VMSlot>();
		for (VMCore core : this.cores) {
			schedule.addAll(core.getSchedule());
		}
		Collections.sort(schedule);
		return schedule;
	}

	public double getEarliestIdleTimeFrom(double readyTime, double runtime) {
		double startExecutionTime = Double.MAX_VALUE;

		for (VMCore core : cores) {
			startExecutionTime = Math.min(core.getEarliestIdleSlotTimeFrom(readyTime, runtime), startExecutionTime);
		}

		return startExecutionTime;
	}

	public boolean isUsed() {
		return !taskPointerToCore.isEmpty();
	}

	public String getId() {
		return id;
	}

	public double getRuntime() {
		if (terminateTime - launchTime < 0)
			return 0.0;
		return terminateTime - launchTime;
	}

	public double getExecutionCost() {
	//	System.out.println("start="+this.launchTime+"\tfinish="+this.terminateTime+"\truntime="+getRuntime());
		double billingUnits = getRuntime() / vmType.getBillingTimeInSeconds();
		double fullBillingUnits = Math.ceil(billingUnits);
		return fullBillingUnits * vmType.getPriceForBillingUnit();
	}

	public void offsetLunchTime(double value) {
		launchTime = Math.min(launchTime, value);
	}

	public void offsetTerminateTime(double value) {
		terminateTime = Math.max(terminateTime, value);
	}
}