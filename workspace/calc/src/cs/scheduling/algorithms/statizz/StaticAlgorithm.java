package cs.scheduling.algorithms.statizz;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.job.JobListener;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.VM;
import cs.scheduling.algorithms.Algorithm;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;

public abstract class StaticAlgorithm extends Algorithm implements JobListener {

	/** Mapping of Task to the VM that will run the task */
	private HashMap<Task, VM> taskPointerToVM;

	/** Plan */
	private Plan plan;

	private long planningStartWallTime;

	private long planningFinishWallTime;

	public StaticAlgorithm(DAG dag, double deadline, Environment environment) {
		super(dag, deadline, environment);
		initialize();
	}

	public StaticAlgorithm(List<DAG> dags, double deadline, Environment environment) {
		super(dags, deadline, environment);
		initialize();
	}

	private void initialize() {
		// create a plan for the static scheduling algorithm
		plan = new Plan();

		// mappping task <-> VM
		taskPointerToVM = new HashMap<Task, VM>();

		// add listener for the workflow engine
		addJobListener(this);
	}

	protected final void onBeforeSchedulingStart() {
		selectingResources(plan);
		onInternalBeforeSchedulingStart();
	}

	protected final void onAfterSchedulingCompleted() {

		// let the algorithm finalize things
		onInternalAfterSchedulingCompleted();

		// get the VMs and send its to the environment execution
		this.taskPointerToVM = plan.createVMs();

		Set<VM> vms = new LinkedHashSet<VM>(taskPointerToVM.values());
		for (VM vm : vms) {
			addVmToTheExecutionEnvironment(vm);
		}

	}

	@Override
	protected final boolean simulateInternal() {

		onBeforeSchedulingStart();

		planningStartWallTime = System.nanoTime();
		plan = planDAG(PlanFactory.getEmptyPlan(plan));
		planningFinishWallTime = System.nanoTime();

		if (plan != null) {

			logln(">>> Result: Scheduled found in " + getSchedulingWallTimeInSeconds() + " seconds ...");

			onAfterSchedulingCompleted();

			logln(">>> Schedule produced:  ");
			logln(plan.logSchedules());

			return true;
		}

		logln(">>> Result: Schedule not found ...");
		return false;
	}

	@Override
	public double getSchedulingWallTimeInSeconds() {
		return TimeUnit.MILLISECONDS.convert((planningFinishWallTime - planningStartWallTime), TimeUnit.NANOSECONDS)
				/ 1000.0;
	}

	protected abstract Plan planDAG(Plan currentPlan);

	protected abstract void onInternalBeforeSchedulingStart();

	protected void selectingResources(Plan plan) {
		// create the resources for the planning
		// selecting all resources from the VM file; the boolean true means
		// that this simulator will create only one resource
		// for each VM type within the VM file
		for (VMType vmType : getAllVMTypes()) {
			plan.createResource(vmType, true);
		}
	}

	protected abstract void onInternalAfterSchedulingCompleted();

	public final HashMap<DAG, Double> getScheduledMakespan() {
		return plan.getMakespan();
	}

	public HashMap<DAG, Double> getScheduledFET() {
		return plan.getScheduledFET();
	}

	public HashMap<DAG, Double> getScheduledSET() {
		return plan.getScheduledSET();
	}

	public final double getOverallScheduledMakespan() {
		return plan.getOverallMakespan();
	}

	public final double getScheduledTotalBytesSent() {
		return plan.getScheduledTotalBytesSent();
	}
	
	public final double getScheduledExecutionCost() {
		return plan.getExecutionCost();
	}

	public final double getScheduledTotalBytesReceived() {
		return plan.getScheduledTotalBytesReceived();
	}

	public VM getVM(Task task) {
		return this.taskPointerToVM.get(task);
	}

	public void newPlan() {
		this.plan = new Plan();
		this.taskPointerToVM = new HashMap<Task, VM>();
	}

}
