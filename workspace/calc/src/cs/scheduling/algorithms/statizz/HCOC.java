package cs.scheduling.algorithms.statizz;

import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.engine.Environment;
import cs.core.vm.scheduling.VMType;
import cs.scheduling.algorithms.statizz.plan.Plan;
import cs.scheduling.algorithms.statizz.plan.PlanFactory;

public class HCOC extends PCH {

	private final static double THRESHOLD = 0.0001;

	// private resources
	private List<VMType> privateResources;

	// private resources
	private List<VMType> publicResources;

	// default value must be HYBRID
	private Cloud cloud = Cloud.HYBRID;

	public HCOC(DAG dag, double deadline, Environment environment) {
		super(dag, deadline, environment);
	}

	private void hcoc() {

		// hybrid resources
		List<VMType> H;

		// rescheduling group
		TreeSet<Task> T = new TreeSet<Task>();

		// take the tasks from PCH sorted in according priority
		LinkedList<Task> tasks = new LinkedList<Task>();
		for (String taskName : getDAG().getTasksOrdedByUpwardRank()) {
			Task task = getDAG().getTaskById(taskName);
			tasks.add(task);
		}

		// local variables
		int iteration = 0;
		int numberOfClusters = 0;

		// Algorithm 1 HCOC
		while (getEstimatedMakespan() > getDeadline() && iteration < getDAG().getNumberOfTasks()) {

			// line 4
			iteration++;

			// line 5
			Task highestPriority = tasks.poll();

			// line 6
			H = new LinkedList<VMType>(privateResources);

			// line 7
			T.add(highestPriority);

			// line 8
			numberOfClusters = getNumberOfCluster(T);

			// line 9
			while (numberOfClusters > 0) {

				// line 10
				VMType vmType = selectResource(numberOfClusters, publicResources);

				if (vmType == null) {
					// try next round
					break;
				}

				// Here it is possible to create, for example, 2 VM with the
				// same VMType, however, in the PAPER it does not occur.
				if (!H.contains(vmType)) {
					H.add(vmType);
				}

				// line 12
				numberOfClusters -= vmType.getCores();
			}
			// lines 14,15,16
			reschedule(T, H);

			// added by Thiago
			// as some tasks can be rescheduled in the
			// public cloud, the private resources will be
			// available for other remain tasks, which can be rescheduled
			// on those resources
			TreeSet<Task> remains = new TreeSet<Task>();
			remains.addAll(tasks);
			reschedule(remains, privateResources);
		}

		logln("Tasks selected to be rescheduled to the public cloud: " + T);

	}

	private void reschedule(TreeSet<Task> T, List<VMType> H) {

		// for each task in the rescheduling group
		for (Task task : T) {

			// variables initializations
			double minEFT = Double.MAX_VALUE;
			double EFT = 0.0;
			VMType selectedVmType = null;
			VMType originalVmType = getScheduledTask(task);

			// the loop bellow don't modify the plan object, only modify the
			// scheduling object produced by the PCH algorithm
			// Basically, is a "upper layer" modification
			// It is not a definitive modification in the scheduling plan

			// looking for a new resource
			for (VMType candidateVmType : H) {

				// if is the first time that the candidate VmType
				// has been provided as a volunteer
				instanciateMachine(candidateVmType);

				// deallocation from the original cluster and ...
				deallocating(task);

				// allocation to the candidate VM
				allocating(task, candidateVmType);

				// calculating EST
				update();

				// check if the current resource is the best resource that
				// minimizes the EFT of the current task
				EFT = EFT(task, candidateVmType);
				if (EFT < minEFT) {
					minEFT = EFT;
					selectedVmType = candidateVmType;
				}

				// restoring the original information ...

				// deallocation from the original cluster and ...
				deallocating(task);

				// allocation to the candidate VM
				allocating(task, originalVmType);

				// update EST
				update();

			}

			// as a VmType has been selected
			// it is necessary to allocate and update
			// one more time

			// deallocation from the original cluster and ...
			deallocating(task);

			// allocation to the candidate VM
			allocating(task, selectedVmType);

			// update EST
			update();
		}

	}

	private VMType selectResource(int numberOfClusters, List<VMType> publicResources) {

		VMType best = null;
		double min = Double.MAX_VALUE, condition = 0.0;

		for (VMType vmType : publicResources) {

			if (vmType.getCores() <= numberOfClusters) {

				condition = vmType.getPriceForBillingUnit() / (vmType.getCores() * vmType.getMips());

				if (condition < min) {
					min = condition;
					best = vmType;
				}
			}
		}

		return best;
	}

	/****************************************************
	 ** Override functions used by the SCHEDULING step **
	 ****************************************************/

	@Override
	protected void selectingResources(Plan plan) {

		privateResources = new LinkedList<VMType>();
		publicResources = new LinkedList<VMType>();

		// selecting private resources
		logln("Selecting private resources to the PCH scheduler ...");
		for (VMType vmType : super.getAllVMTypes()) {
			if (vmType.getPriceForBillingUnit() < THRESHOLD) {
				privateResources.add(vmType);
			} else {
				publicResources.add(vmType);
			}
		}

		// create the resources for the planning
		// selecting all resources from the VM file; the boolean true means
		// that this simulator will create only one resource
		// for each VM type within the VM file
		for (VMType vmType : getAllVMTypes()) {
			plan.createResource(vmType, true);
		}

		// seting cloud to private
		// this flag is only for when PCH will ask
		// for getAllVMTypes()
		cloud = Cloud.PRIVATE;

	}

	@Override
	public List<VMType> getAllVMTypes() {

		// selecting resources in accordance with
		// the moment of the scheduling algorithm
		switch (cloud) {

		case PRIVATE:
			if (privateResources.isEmpty()) {
				break;
			}
			return privateResources;

		case HYBRID:
			break;
		}

		return super.getAllVMTypes();
	}

	@Override
	protected void onInternalBeforeSchedulingStart() {
		logln("Designating the schedule to the PCH scheduler ...");
		super.onInternalBeforeSchedulingStart();
	}

	@Override
	protected Plan planDAG(Plan currentPlan) {
		// call the PCH scheduler
		Plan plan = super.planDAG(PlanFactory.getEmptyPlan(currentPlan));

		logln("Schedule produced by PCH: ");
		printSchedule();

		// check if scheduled produced by the PCH satisfies
		// the deadline
		log(String.format("Makespan <= Deadline: "));
		if (plan.getMakespan().get(getDAG()) <= getDeadline()) {
			logln(": true");
			logln("Schedule by using PCH only");
			// job done by the PCH only with private resources
			return plan;
		}
		logln(": false");
		logln("Deadline missed, starting  the HCOC scheduling algorithm");

		// setting cloud to Hybrid
		cloud = Cloud.HYBRID;

		plan = PlanFactory.getEmptyPlan(currentPlan);

		// let the HCOC scheduler try to satisfy the deadline
		// by using hybrid cloud
		// selectingResources();
		hcoc();

		// lets the HCOC return the best schedule achieved even if
		// the deadline is missed

		createPlan(plan);

		return plan;
	}

	@Override
	public String getSchedulingAlgorithmName() {
		return "HCOC";
	}

	private enum Cloud {
		PRIVATE, HYBRID;
	}

}
