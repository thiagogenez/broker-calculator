package cs.scheduling.algorithms.dynamic;

import cs.core.dag.DAG;
import cs.core.engine.Environment;
import cs.core.job.JobListener;
import cs.scheduling.algorithms.Algorithm;

public abstract class DynamicAlgorithm extends Algorithm implements JobListener {

	private long startWallTime;

	private long finishWallTime;

	public DynamicAlgorithm(DAG dag, double deadline, Environment environment) {
		super(dag, deadline, environment);

		// add listener for the workflow engine
		addJobListener(this);

	}

	protected final void onBeforeSchedulingStart() {
		logln("----BEGIN: " + getSchedulingAlgorithmName() + "----");
		onInternalBeforeSchedulingStart();
	}

	protected final void onAfterSchedulingCompleted() {
		// mapping task to VM
		// bindTaskToVM();
		// printSchedules();
		onInternalAfterSchedulingCompleted();
		logln("----END: " + getSchedulingAlgorithmName() + "----");

	}

	@Override
	protected final boolean simulateInternal() {
		startWallTime = System.nanoTime();
		onBeforeSchedulingStart();
		schedulingInternal();
		finishWallTime = System.nanoTime();

		return true;
	}

	@Override
	public double getSchedulingWallTimeInSeconds() {
		return finishWallTime - startWallTime;
	}

	protected abstract void schedulingInternal();

	protected abstract void onInternalBeforeSchedulingStart();

	protected abstract void onInternalAfterSchedulingCompleted();

}
