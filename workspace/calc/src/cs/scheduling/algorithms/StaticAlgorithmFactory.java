package cs.scheduling.algorithms;

import cs.core.dag.DAG;
import cs.core.engine.Environment;
import cs.core.exception.SimulatiorException;
import cs.scheduling.algorithms.statizz.BuyyasPSO;
import cs.scheduling.algorithms.statizz.HCOC;
import cs.scheduling.algorithms.statizz.IPDT;
import cs.scheduling.algorithms.statizz.IPDT_FULL_FUZZY;
import cs.scheduling.algorithms.statizz.IPDT_FUZZY;
import cs.scheduling.algorithms.statizz.IPWS;
import cs.scheduling.algorithms.statizz.IPWS2;
import cs.scheduling.algorithms.statizz.PCH;
import cs.scheduling.algorithms.statizz.Random;
import cs.scheduling.algorithms.statizz.StaticAlgorithm;
import cs.scheduling.algorithms.statizz.heft.HEFT;
import cs.simulation.config.DAGParameters;
import cs.simulation.config.SchedulerParameters;
import cs.simulation.config.VMParameters;

public class StaticAlgorithmFactory {
	public static StaticAlgorithm createAlgorithm(DAG dag, Environment environment, double deadline,
			DAGParameters dagParameters, VMParameters vmParameters, SchedulerParameters schedulerParameters) {

		String algorithmName = schedulerParameters.getAlgorithm().toUpperCase().trim();

		switch (algorithmName) {

		case "PCH":
			return new PCH(dag, deadline, environment);

		case "HCOC":
			return new HCOC(dag, deadline, environment);

		case "HEFT":
			return new HEFT(dag, environment);

		case "BUYYASPSO":
			return new BuyyasPSO(dag, deadline, environment);

		case "IPDT":
			return new IPDT(dag, deadline, environment, schedulerParameters.getNodeLim(),
					schedulerParameters.getTiLim(), schedulerParameters.getThreads(), schedulerParameters.getEpgap());

		case "IPDT_FUZZY":
			return new IPDT_FUZZY(dag, deadline, environment, dagParameters.getRho(), dagParameters.getSigma(),
					schedulerParameters.getNodeLim(), schedulerParameters.getTiLim(), schedulerParameters.getThreads(),
					schedulerParameters.getEpgap());

		case "IPDT_FULL_FUZZY":
			return new IPDT_FULL_FUZZY(dag, deadline, environment, dagParameters.getRho(), dagParameters.getSigma(),
					vmParameters.getChi(), vmParameters.getOmega(), schedulerParameters.getNodeLim(),
					schedulerParameters.getTiLim(), schedulerParameters.getThreads(), schedulerParameters.getEpgap());
		case "IPWS":
			return new IPWS(dag, deadline, environment, schedulerParameters.getNodeLim(),
					schedulerParameters.getTiLim(), schedulerParameters.getThreads(), schedulerParameters.getLambda(),
					schedulerParameters.getEpgap());
		case "IPWS2":
			return new IPWS2(dag, deadline, environment, schedulerParameters.getNodeLim(),
					schedulerParameters.getTiLim(), schedulerParameters.getThreads(), schedulerParameters.getLambda(),
					schedulerParameters.getEpgap());

		case "RANDOM":
			return new Random(dag, deadline, environment);

		default:
			throw new SimulatiorException("Unknown algorithm name: " + algorithmName);
		}

	}

}
