package cs.core.dag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;

import org.apache.commons.math3.distribution.ParetoDistribution;

/**
 * Class for generating list of DAG files for experiments.
 * 
 * @author malawski
 * 
 *         Code adapted from:
 *         https://github.com/malawski/cloudworkflowsimulator/blob/master/src/
 *         cws/core/dag/DAGListGenerator.java
 * 
 */

public class EnsembleDAGListGenerator {
	/** Possible DAG sizes */

	private static final int[] SIZES = new int[] { 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };

	private static final int DAG_NUMBER = 20;

	/**
	 * Generates list of DAG names (.dag files) to be used with workflows from the
	 * workflow generator
	 * https://confluence.pegasus.isi.edu/display/pegasus/WorkflowGenerator
	 * converted to simplified dag format using dax2dag.rb script It is better to
	 * use DAG files, since XML parser tends to be 10x slower.
	 * 
	 * @param name
	 *            Name prefix of DAG file (e.g. MONTAGE)
	 * @param sizes
	 *            array of sizes, e.g. {100, 200}
	 * @param sizeCount
	 *            number of files of each size (usually 20)
	 * @return array of file names (no path)
	 */

	// public static String[] generateDAGList(String name, int[] sizes,
	// int sizeCount) {
	//
	// String dags[] = new String[sizes.length * sizeCount];
	// for (int i = 0; i < sizes.length; i++) {
	// for (int j = 0; j < sizeCount; j++) {
	// dags[i * sizeCount + j] = name + ".n." + sizes[i] + "." + j
	// + ".dag";
	// }
	// }
	// return dags;
	// }

	private static ArrayList<String> generateDAGList(Random seed, String name, int[] sizes) {
		ArrayList<String> dags = new ArrayList<String>(sizes.length);

		for (int i = 0; i < sizes.length; i++) {
			int size = sizes[i];
			int index = seed.nextInt(DAG_NUMBER);
			String dag = name + ".n." + size + "." + index + ".dag";
			dags.add(dag);
		}

		return dags;
	}

	private static int[] generateUniformSizesArray(Random seed, int length) {
		int[] sizes = new int[length];

		for (int i = 0; i < length; i++) {
			int size = SIZES[seed.nextInt(SIZES.length)];
			sizes[i] = size;
		}

		return sizes;
	}

	public static String[] generateEnsembleDAGListUniformSorted(Random seed, String name, int length,
			boolean ascending) {
		int[] sizes = generateUniformSizesArray(seed, length);

		Arrays.sort(sizes);
		ArrayList<String> dags = generateDAGList(seed, name, sizes);
		if (!ascending) {
			Collections.reverse(dags);
		}

		return dags.toArray(new String[0]);
	}

	public static String[] generateEnsembleDAGListUniformUnsorted(Random seed, String name, int length) {
		int[] sizes = generateUniformSizesArray(seed, length);

		ArrayList<String> dags = generateDAGList(seed, name, sizes);

		return dags.toArray(new String[0]);
	}

	public static String[] generateEnsembleDAGListConstant(Random seed, String name, int size, int length) {
		String dags[] = new String[length];
		for (int i = 0; i < length; i++) {
			int index = seed.nextInt(DAG_NUMBER);
			dags[i] = name + ".n." + size + "." + index + ".dag";
		}
		return dags;
	}

	public static String[] generateEnsembleDAGListConstant(Random seed, String name, int length) {
		int size = SIZES[seed.nextInt(SIZES.length)];

		return generateEnsembleDAGListConstant(seed, name, size, length);

	}

	public static String[] generateEnsembleDAGListParetoSorted(Random seed, String name, int length,
			boolean ascending) {
		ArrayList<String> dags = new ArrayList<String>(length);

		ParetoDistribution pareto = new ParetoDistribution(50, 1);
		pareto.reseedRandomGenerator(seed.nextLong());

		HashMap<Integer, Integer> distr = new HashMap<Integer, Integer>();
		for (int i = 0; i < length; i++) {
			double d = pareto.sample();
			int n;
			if (d < 100) {
				n = 50;
			} else if (d > 1000) {
				n = 1000;
			} else {
				n = (int) Math.floor(d / 100) * 100;
			}

			if (!distr.containsKey(n)) {
				distr.put(n, 1);
			} else {
				distr.put(n, distr.get(n) + 1);
			}
		}
		System.out.println(distr);
		Integer[] sizes = distr.keySet().toArray(new Integer[0]);
		Arrays.sort(sizes);
		for (int size : sizes) {
			int count = distr.get(size);
			for (int i = 0; i < count; i++) {
				String dag = name + ".n." + size + "." + seed.nextInt(DAG_NUMBER) + ".dag";
				dags.add(dag);
			}
		}
		if (!ascending) {
			Collections.reverse(dags);
		}

		return dags.toArray(new String[0]);
	}

	public static String[] generateEnsembleDAGListParetoUnsorted(Random seed, String name, int length) {
		String[] list = generateEnsembleDAGListParetoSorted(seed, name, length, false);
		int n = list.length;

		// Use Fisher-Yates algorithm to randomize list
		// To shuffle an array a of n elements (indices 0..n-1):
		// for i from n − 1 downto 1 do
		for (int i = n - 1; i >= 1; i--) {
			// j = random integer with 0 ≤ j ≤ i
			int j = seed.nextInt(i + 1);

			// exchange a[j] and a[i];
			String tmp = list[j];
			list[j] = list[i];
			list[i] = tmp;
		}

		return list;
	}

	public static void main(String[] args) {
		System.out.println(Arrays.toString(EnsembleDAGListGenerator
				.generateEnsembleDAGListUniformSorted(new Random(System.currentTimeMillis()), "Montage", 10, true)));

		System.out.println(Arrays.toString(EnsembleDAGListGenerator
				.generateEnsembleDAGListUniformUnsorted(new Random(System.currentTimeMillis()), "Montage", 10)));

		System.out.println(Arrays.toString(EnsembleDAGListGenerator
				.generateEnsembleDAGListConstant(new Random(System.currentTimeMillis()), "Montage", 10)));

		System.out.println(Arrays.toString(EnsembleDAGListGenerator
				.generateEnsembleDAGListConstant(new Random(System.currentTimeMillis()), "Montage", 100, 10)));

		System.out.println(Arrays.toString(EnsembleDAGListGenerator
				.generateEnsembleDAGListParetoUnsorted(new Random(System.currentTimeMillis()), "Montage", 20)));

		System.out.println(Arrays.toString(EnsembleDAGListGenerator
				.generateEnsembleDAGListParetoSorted(new Random(System.currentTimeMillis()), "Montage", 20, true)));
	}

}
