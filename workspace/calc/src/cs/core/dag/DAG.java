package cs.core.dag;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

import cs.core.dag.algorithms.TopologicalOrder;
import cs.core.exception.DAGFileNotFoundException;

/**
 * @author Gideon Juve <juve@usc.edu>
 */
public class DAG {
	private HashMap<String, Long> files = new HashMap<String, Long>();

	private LinkedHashMap<String, Task> tasks = new LinkedHashMap<String, Task>();

	private double totalCPUDemands = 0.0;

	private double totalTransferFileDemands = 0.0;

	// for logging purposes
	private String id;

	private String path;

	private String name;

	public void addTask(Task t) {
		if (tasks.containsKey(t.getId())) {
			throw new RuntimeException("Task already exists: " + t.getId());
		}
		t.setDAG(this);
		totalCPUDemands += t.getSize();
		tasks.put(t.getId(), t);
	}

	public void removeTask(Task t) {
		if (!tasks.containsKey(t.getId())) {
			throw new RuntimeException("Task does not exists: " + t.getId());
		}

		totalCPUDemands -= t.getSize();
		tasks.remove(t.getId(), t);

		if (t.isClone()) {
			Task original = getTaskById(t.getOriginalId());
			original.deleteClone(t);
		}
		
		//sanity check
		for(String taskId: getTasks()) {
			Task task = getTaskById(taskId);
			
			for(Task parent: task.getParents()) {
				if(parent.getId().equals(taskId)) {
					throw new RuntimeException("Task=" + t.getId()+ " still has a parent="+parent);
				}
			}
			
			for(Task child: task.getChildren()) {
				if(child.getId().equals(taskId)) {
					throw new RuntimeException("Task=" + t.getId()+ " still has a child="+child);
				}
			}
		}

	}

	public void addFile(String name, long size) {
		if (size < 0) {
			throw new RuntimeException("Invalid size for file '" + name + "': " + size);
		}
		totalTransferFileDemands += size;
		files.put(name, size);
	}

	public void addEdge(String parent, String child) {
		Task p = tasks.get(parent);
		if (p == null) {
			throw new RuntimeException("Invalid edge: Parent not found: " + parent);
		}
		Task c = tasks.get(child);
		if (c == null) {
			throw new RuntimeException("Invalid edge: Child not found: " + child);
		}
		p.getChildren().add(c);
		c.getParents().add(p);
	}

	public void removeEdge(String parent, String child) {
		Task p = tasks.get(parent);
		if (p == null) {
			throw new RuntimeException("Invalid edge: Parent not found: " + parent);
		}
		Task c = tasks.get(child);
		if (c == null) {
			throw new RuntimeException("Invalid edge: Child not found: " + child);
		}
		p.getChildren().remove(c);
		c.getParents().remove(p);
	}

	public void setInputs(String taskId, List<DAGFile> inputs) {
		Task t = getTaskById(taskId);
		t.addInputFiles(inputs);
	}

	public void setOutputs(String task, List<DAGFile> outputs) {
		Task t = getTaskById(task);
		t.addOutputFiles(outputs);
	}

	public int getNumberOfTasks() {
		return tasks.size();
	}

	public int numFiles() {
		return files.size();
	}

	public Task getTaskById(String id) {
		if (!tasks.containsKey(id)) {
			throw new RuntimeException("Task not found: " + id);
		}
		return tasks.get(id);
	}

	public long getFileSize(String name) {
		if (!files.containsKey(name)) {
			throw new DAGFileNotFoundException(name);
		}
		return files.get(name);
	}

	public String[] getEntryTasks() {
		LinkedHashSet<String> entryTask = new LinkedHashSet<String>();
		for (Task t : tasks.values()) {
			if (t.getParents().size() == 0) {
				entryTask.add(t.getId());
			}
		}
		return entryTask.toArray(new String[0]);
	}

	public TopologicalOrder getTopologicalOrder() {
		return new TopologicalOrder(this);
	}

	public List<String> getLongestPath() {

		// from:
		// http://www.geeksforgeeks.org/find-longest-path-directed-acyclic-graph/

		// Input: Weighted DAG G = (V, E)
		// Output: Largest path cost in G

		// Topologically sort G
		// for each vertex v in V in linearized order
		// do dist(v) = max(u,v) in E {dist(u) + w(u, v)}
		// return max {v in V {dist(v)}}

		LinkedList<String> longestPath = new LinkedList<String>();
		TopologicalOrder order = getTopologicalOrder();

		TreeMap<String, Double> distance = new TreeMap<String, Double>();

		for (String task : this.tasks.keySet()) {
			distance.put(task, -1.0);
		}

		for (String entry : getEntryTasks()) {
			distance.put(entry, 0.0);
		}

		for (Task u : order) {
			double distU = distance.get(u.getId());
			if (distU >= 0) {
				for (Task v : u.getChildren()) {
					double transferSize = u.getTotalBytesToSent(v);
					if (distance.get(v.getId()) < distU + transferSize) {
						distance.put(v.getId(), distU + transferSize);
					}
				}
			}
		}

		// get the exist tasks
		// if the dag has more than one
		TreeMap<Double, String> candidates = new TreeMap<Double, String>();
		for (String exit : getExitTasks()) {
			candidates.put(distance.get(exit), exit);
		}

		// select the exist task with highest distance
		String criticalTask = candidates.get(candidates.descendingKeySet().first());
		String nextCandidate = null;
		double max = Double.NEGATIVE_INFINITY;

		// create the longest path from the selected exit task
		while (criticalTask != null) {

			max = Double.NEGATIVE_INFINITY;
			nextCandidate = null;

			for (Task parent : getTaskById(criticalTask).getParents()) {
				if (distance.get(parent.getId()) > max) {
					max = distance.get(parent.getId());
					nextCandidate = parent.getId();
				}
			}
			longestPath.addFirst(criticalTask);
			criticalTask = nextCandidate;

		}

		// sanity check
		if (!isEntryTask(longestPath.peekFirst()) || !isExitTask(longestPath.peekLast())) {
			throw new RuntimeException("Invalid Longest Path: " + longestPath);
		}

		// return the logest path of the dag
		return longestPath;
	}

	public boolean isEntryTask(String entry) {
		String[] tasks = getEntryTasks();
		for (int i = 0; i < tasks.length; i++) {
			if (tasks[i].equals(entry))
				return true;
		}
		return false;
	}

	public boolean isEntryTask(Task entry) {
		return isEntryTask(entry.getId());
	}

	public boolean isExitTask(String exit) {
		String[] tasks = getExitTasks();
		for (int i = 0; i < tasks.length; i++) {
			if (tasks[i].equals(exit))
				return true;
		}
		return false;
	}

	public boolean isExitTask(Task exit) {
		return isExitTask(exit.getId());
	}

	public String[] getExitTasks() {
		LinkedHashSet<String> exitTasks = new LinkedHashSet<String>();
		for (Task t : tasks.values()) {
			if (t.getChildren().isEmpty()) {
				exitTasks.add(t.getId());
			}
		}
		return exitTasks.toArray(new String[0]);
	}

	public String[] getFiles() {
		return files.keySet().toArray(new String[0]);
	}

	public String[] getTasks() {
		return tasks.keySet().toArray(new String[0]);
	}

	public String[] getTasksOrdedByDownwardRank() {
		ArrayList<Task> list = new ArrayList<Task>(tasks.values());
		Collections.sort(list, Task.COMPARE_BY_DOWNWARD_RANK);
		return toArray(list);
	}

	public String[] getTasksOrdedByUpwardRank() {

		ArrayList<Task> list = new ArrayList<Task>(tasks.values());
		Collections.sort(list, Collections.reverseOrder(Task.COMPARE_BY_UPWARD_RANK));
		return toArray(list);
	}

	private String[] toArray(List<Task> list) {
		String[] sortedTaskNames = new String[list.size()];
		int i = 0;
		for (Task task : list) {
			sortedTaskNames[i++] = task.getId();
		}
		return sortedTaskNames;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public double getTotalCPUDemands() {
		return totalCPUDemands;
	}

	public double getTotalTransferFileDemands() {
		return totalTransferFileDemands;
	}

	@Override
	public String toString() {
		return "DAG <workflow_id = " + this.id + ">";
	}

}
