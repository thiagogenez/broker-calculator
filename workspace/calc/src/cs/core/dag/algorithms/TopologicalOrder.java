package cs.core.dag.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import cs.core.dag.DAG;
import cs.core.dag.Task;

/**
 * Compute topological order of a DAG. Uses depth-first search. A reverse
 * postorder in a DAG provides a topological order. Reverse postorder: Put the
 * vertex on a stack after the recursive calls. See:
 * http://algs4.cs.princeton.edu/42directed/
 * 
 * @author malawski
 * 
 */
public class TopologicalOrder implements Iterable<Task> {

	private Set<Task> marked = new HashSet<Task>();

	private Deque<Task> postorder = new LinkedList<Task>();

	public TopologicalOrder(DAG dag) {
		List<Task> tasks = new ArrayList<Task>();

		List<String> names = Arrays.asList(dag.getEntryTasks());
		Collections.shuffle(names);
		// create a list of tasks
		for (String taskName : names) {
			tasks.add(dag.getTaskById(taskName));
		}

		for (Task task : tasks) {
			if (!marked.contains(task)) {
				dfs(task);
			}
		}
		marked = null;
	}

	private void dfs(Task task) {
		marked.add(task);
		List<Task> children = task.getChildren();
		Collections.shuffle(children);

		for (Task child : children) {
			if (!marked.contains(child))
				dfs(child);
		}
		postorder.add(task);
	}

	public Iterable<Task> reverse() {
		return new Iterable<Task>() {
			@Override
			public Iterator<Task> iterator() {
				return postorder.iterator();
			}
		};
	}

	@Override
	public Iterator<Task> iterator() {
		return postorder.descendingIterator();
	}
}
