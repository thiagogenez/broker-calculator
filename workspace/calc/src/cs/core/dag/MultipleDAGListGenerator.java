package cs.core.dag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class MultipleDAGListGenerator {

	private static final int DAG_NUMBER = 20;

	public static void shuffleArray(String[] array) {
		List<String> list = new ArrayList<>();
		for (String i : array) {
			list.add(i);
		}

		Collections.shuffle(list);

		for (int i = 0; i < list.size(); i++) {
			array[i] = list.get(i);
		}
	}

	public static String[] generateMultipleDAGListConstantShuffled(Random seed, List<String> names, int size,
			int length) {

		String dags[] = new String[length];
		for (int i = 0; i < length; i++) {
			int index = seed.nextInt(DAG_NUMBER);
			int dagNameIndex = seed.nextInt(names.size());

			dags[i] = names.get(dagNameIndex) + ".n." + size + "." + index + ".dag";
		}
		shuffleArray(dags);
		return dags;
	}

	public static String[] generateMultipleDAGEnsembleListConstant(Random seed, List<String> names, int size,
			int length) {

		String multipleEnsembles[] = new String[length];
		int index = 0;

		for (int ensembleId = 0; ensembleId < names.size(); ensembleId++) {

			String ensembles[] = new String[length / names.size()];

			for (int dagId = 0; dagId < length / names.size(); dagId++) {

				int number = seed.nextInt(DAG_NUMBER);

				ensembles[dagId] = names.get(ensembleId) + ".n." + size + "." + number + ".dag";
			}

			shuffleArray(ensembles);

			for (int i = 0; i < ensembles.length; i++) {
				multipleEnsembles[index] = ensembles[i];
				index++;
			}
		}

		return multipleEnsembles;
	}

	public static String[] generateMultipleDAGListProportionShuffled(Random seed, HashMap<String, Double> proportion,
			int size, int length) {

		String dags[] = new String[length];
		int index = 0;

		if (length % 2 != 0) {
			throw new RuntimeException("DAG set size must be a even number: " + length);
		}

		for (String dagName : proportion.keySet()) {

			int amount = (int) (proportion.get(dagName) * length);

			for (int i = 0; i < amount; i++) {
				int dagNumber = seed.nextInt(DAG_NUMBER);

				dags[index] = dagName + ".n." + size + "." + dagNumber + ".dag";

				index++;
			}
		}
		shuffleArray(dags);
		return dags;
	}

	public static void main(String[] args) {

		List<String> names = new ArrayList<>();
		names.add("MONTAGE");
		System.out.println(Arrays.toString(MultipleDAGListGenerator
				.generateMultipleDAGListConstantShuffled(new Random(System.currentTimeMillis()), names, 20, 10)));

		HashMap<String, Double> teste = new HashMap<>();

		teste.put("MONTAGE", 0.0);
		teste.put("LIGO", 1.0);

		System.out.println(Arrays.toString(MultipleDAGListGenerator
				.generateMultipleDAGListProportionShuffled(new Random(System.currentTimeMillis()), teste, 20, 10)));
	}
}
