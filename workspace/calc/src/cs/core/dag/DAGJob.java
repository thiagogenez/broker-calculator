package cs.core.dag;

import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

import cs.core.job.Job;

/**
 * This class records information about the execution of a DAG, including the
 * state of all tasks.
 * 
 * @author Gideon Juve <juve@usc.edu>
 */
public class DAGJob {
	/** The DAG being executed */
	private DAG dag;

	/** Set of tasks that have been released */
	private HashMap<Task, Job> releasedJobs;

	/** Set of tasks that are finished */
	private HashMap<Task, Job> completedJobs;

	/** List of all tasks that are ready but have not been claimed */
	private PriorityQueue<Job> readyJobsQueue;

	public DAGJob(DAG dag) {
		this.dag = dag;

		this.readyJobsQueue = new PriorityQueue<Job>(Job.COMPARE_BY_EST);
		this.releasedJobs = new HashMap<Task, Job>();
		this.completedJobs = new HashMap<Task, Job>();

		// Release all root tasks
		for (String tid : dag.getEntryTasks()) {
			Task t = dag.getTaskById(tid);
			releaseTask(t);
		}

	}

	public DAG getDAG() {
		return dag;
	}

	public void setDAG(DAG dag) {
		this.dag = dag;
	}

	/** Check to see if a task has been released */
	public boolean isReleased(Task t) {
		return releasedJobs.containsKey(t);
	}

	/** Check to see if a task has been completed */
	public boolean isComplete(Task t) {
		return completedJobs.containsKey(t);
	}

	public Job getCompletedJob(Task task) {
		return completedJobs.get(task);
	}

	public Job getReleasedJob(Task task) {
		return releasedJobs.get(task);
	}

	/** Return true if the workflow is finished */
	public boolean isFinished() {
		// The workflow must be finished if all the tasks that
		// have been released have been completed
		return releasedJobs.size() == completedJobs.size();
	}

	/** Mark a task as completed */
	public void completeJob(Job job) {
		// Sanity check
		if (!releasedJobs.containsKey(job.getTask())) {
			throw new RuntimeException("Job has not been released: " + job);
		}

		// Add it to the list of completed jobs
		completedJobs.put(job.getTask(), job);

		// Release all ready children
		for (Task c : job.getTask().getChildren()) {
			if (!releasedJobs.containsKey(c)) {
				if (allParentsCompleted(c.getParents())) {
					releaseTask(c);
				}
			}
		}
	}

	private void releaseTask(Task task) {
		Job job = new Job(this, task, task.getId());
		releasedJobs.put(task, job);
		readyJobsQueue.add(job);
	}

	private boolean allParentsCompleted(List<Task> parents) {
		for (Task parent : parents) {
			if (!completedJobs.containsKey(parent)) {
				return false;
			}
		}
		return true;
	}

	/** Return the next ready job */
	public Job nextReadyJob() {
		if (readyJobsQueue.size() <= 0)
			return null;
		return readyJobsQueue.poll();
	}

	/** Return the number of ready tasks */
	public int readyTasks() {
		return readyJobsQueue.size();
	}

	@Override
	public String toString() {
		return "DAGJob <workflow_id = " + this.dag.getId() + ">";
	}

}
