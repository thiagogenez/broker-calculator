package cs.core.dag;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * @author Gideon Juve <juve@usc.edu>
 */
public class Task {
	/** Globally uniqe task id */
	private String id = null;

	/**
	 * Transformation string taken from some daxes. Not really important and used
	 * only for logging.
	 */
	private String transformation = null;

	/** Number of MIPS needed to compute this task */
	private double size = 0.0;

	/** Task's parents - the tasks that produce inputFiles */
	private List<Task> parents = new ArrayList<Task>(2);

	/** Task's children - the tasks which this Task produce files for */
	private List<Task> children = new ArrayList<Task>(5);

	/** Task's input files */
	private List<DAGFile> inputFiles = new ArrayList<DAGFile>();

	/** Task's output files */
	private List<DAGFile> outputFiles = new ArrayList<DAGFile>();

	private DAG dag;

	private int cloneId = 0;

	private String originalId = null;

	private boolean isClone;

	public Task(String id, String transformation, double size, boolean isClone) {
		this.id = id;
		this.transformation = transformation;
		this.size = size;
		this.isClone = isClone;
	}

	// upward rank calculated by the algorithm
	private double upwardRank = -1;

	// downward rank calculated by the algorithm
	private double downwardRank = -1;

	// actual start execution time given by the algorithm
	private double AST = -1;

	/**
	 * IT IS IMPORTANT THAT THESE ARE NOT IMPLEMENTED
	 * 
	 * Using the default implementation allows us to put Tasks from different DAGs
	 * that have the same task ID into a single HashMap or HashSet. That way we
	 * don't have to maintain a reference from the Task to the DAG that owns it--we
	 * can mix tasks from different DAGs in the same data structure.
	 * 
	 * <pre>
	 * public int hashCode() {
	 * 	return id.hashCode();
	 * }
	 * 
	 * public boolean equals(Object o) {
	 * 	if (!(o instanceof Task)) {
	 * 		return false;
	 * 	}
	 * 	Task t = (Task) o;
	 * 	return this.id.equals(t.id);
	 * }
	 * </pre>
	 */

	@Override
	public String toString() {
		return "<task id=" + getId() + ";dag_id=" + dag.getId() + ">";
	}

	public void scaleSize(double scalingFactor) {
		size *= scalingFactor;
	}

	public double getSize() {
		return size;
	}

	public String getTransformation() {
		return transformation;
	}

	public String getId() {
		return id;
	}

	public List<Task> getParents() {
		return parents;
	}

	public List<Task> getChildren() {
		return children;
	}

	public List<DAGFile> getInputFiles() {
		return inputFiles;
	}

	public void addInputFiles(List<DAGFile> inputs) {
		this.inputFiles.addAll(inputs);
	}

	public List<DAGFile> getOutputFiles() {
		return outputFiles;
	}

	public List<DAGFile> getOutputFilesTo(Task task) {
		ArrayList<DAGFile> files = new ArrayList<DAGFile>();

		for (DAGFile output : getOutputFiles()) {
			for (DAGFile input : task.getInputFiles()) {
				if (output.equals(input)) {
					files.add(input);
				}
			}
		}

		return files;
	}

	public List<DAGFile> getInputFilesFrom(Task task) {
		ArrayList<DAGFile> files = new ArrayList<DAGFile>();

		for (DAGFile input : getInputFiles()) {
			for (DAGFile output : task.getOutputFiles()) {
				if (output.equals(input)) {
					files.add(input);
				}
			}
		}

		return files;

	}

	public void addOutputFiles(List<DAGFile> outputs) {
		this.outputFiles.addAll(outputs);
	}

	public void setDAG(DAG dag) {
		this.dag = dag;
	}

	public DAG getDAG() {
		return dag;
	}

	public double getUpwardRank() {
		return upwardRank;
	}

	public void setUpwardRank(double rank) {
		this.upwardRank = rank;
	}

	public double getDownwardRank() {
		return downwardRank;
	}

	public void setDownwardRank(double rank) {
		this.downwardRank = rank;
	}

	public double getTotalBytesToSent(Task child) {
		if (!children.contains(child)) {
			return Double.MAX_VALUE;
		}

		double bytes = 0.0;

		for (DAGFile parentFile : this.getOutputFiles()) {
			for (DAGFile childFile : child.getInputFiles()) {
				if (parentFile.equals(childFile)) {
					bytes += childFile.getSize();
				}
			}
		}

		return bytes;
	}

	public double getTotalBytesToReceive(Task parent) {
		if (!parents.contains(parent)) {
			return Double.MAX_VALUE;
		}

		double bytes = 0.0;

		for (DAGFile parentFile : this.getInputFiles()) {
			for (DAGFile childFile : parent.getOutputFiles()) {
				if (parentFile.equals(childFile)) {
					bytes += childFile.getSize();
				}
			}
		}

		return bytes;
	}

	public double getBiggerFile(Task child) {
		if (!children.contains(child)) {
			return Double.MAX_VALUE;
		}

		double bytes = 0.0;

		for (DAGFile parentFile : this.getOutputFiles()) {
			for (DAGFile childFile : child.getInputFiles()) {
				if (parentFile.equals(childFile)) {
					bytes = Math.max(bytes, childFile.getSize());
				}
			}
		}

		return bytes;
	}

	// @Override
	// public int compareTo(Task o) {
	// // checking by starting execution time
	// if (this.EST < o.EST) {
	// return -1;
	// } else if (this.EST > o.EST) {
	// return 1;
	// }
	//
	// String id1 = this.getId().replaceAll("[^0-9.]", "");
	// String id2 = o.getId().replaceAll("[^0-9.]", "");
	//
	// if (Integer.parseInt(id1) < Integer.parseInt(id2))
	// return -1;
	// else if (Integer.parseInt(id1) > Integer.parseInt(id2))
	// return 1;
	//
	// return 0;
	// }

	public static Comparator<Task> COMPARE_BY_UPWARD_RANK = new Comparator<Task>() {
		public int compare(Task one, Task other) {
			return Double.compare(one.upwardRank, other.upwardRank);
		}
	};

	public static Comparator<Task> COMPARE_BY_DOWNWARD_RANK = new Comparator<Task>() {
		public int compare(Task one, Task other) {
			return Double.compare(one.downwardRank, other.downwardRank);
		}
	};

	public static Comparator<Task> COMPARE_BY_ID = new Comparator<Task>() {
		public int compare(Task one, Task other) {
			return one.id.compareTo(other.id);
		}
	};

	public double geAST() {
		return AST;
	}

	public void setActualExecutionTime(double time) {
		this.AST = time;
	}

	public boolean isClone() {
		return isClone;
	}

	public String getOriginalId() {
		return originalId;
	}

	public Task clone() {

		String id = this.getId() + "-CLONE-" + this.cloneId;
		this.cloneId = this.cloneId + 1;

		String transformation = this.getTransformation();
		double size = this.getSize();

		// create a clone with a different id
		Task clone = new Task(id, transformation, size, true);

		// copy necessary things
		clone.dag = this.getDAG();
		clone.originalId = this.getId();
		clone.downwardRank = this.downwardRank;
		clone.upwardRank = this.upwardRank;

		clone.inputFiles.addAll(this.inputFiles);
		clone.outputFiles.addAll(this.outputFiles);
		return clone;

	}

	public void cloneBecomingOriginal(Task clone, LinkedHashMap<String, Task> clones) {

		if (this.isClone || !clone.isClone()) {
			throw new RuntimeException("Only original Task can delete turn a clone into the original");
		}

		// clone.id = this.id;
		// clone.cloneId = 0;
		clone.originalId = null;
		clone.isClone = this.isClone;

		for (Task task : clones.values()) {
			task.originalId = clone.id;
		}
	}

	public void deleteClone(Task clone) {

		// sanity check
		if (this.isClone && !clone.isClone()) {
			throw new RuntimeException("Only original Task can delete its clone");
		}

		if (clone.originalId != this.id) {
			throw new RuntimeException("clone.originalId != this.id");
		}

		// deleting clone
		// clone.dag = null;
		clone.inputFiles = null;
		clone.outputFiles = null;

	}

	public void setOriginalId(String id) {
		this.originalId = id;
	}
}
