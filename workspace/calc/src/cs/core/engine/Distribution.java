package cs.core.engine;

public interface Distribution {
	public double getActualValue(double value);
}
