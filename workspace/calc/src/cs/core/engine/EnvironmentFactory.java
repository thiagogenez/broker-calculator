package cs.core.engine;

import java.util.Map;

import cs.core.vm.scheduling.VMType;

public class EnvironmentFactory {
	public static Environment createEnvironment(Map<String, VMType> vmTypes) {
		return new Environment(vmTypes);
	}

	public static Environment createEnvironment(Map<String, VMType> vmTypes, String br) {
		return new Environment(vmTypes, br);
	}
}
