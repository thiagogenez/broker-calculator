package cs.core.engine;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import cs.core.dag.DAG;
import cs.core.dag.DAGJob;
import cs.core.dag.Task;
import cs.core.job.Job;
import cs.core.job.JobListener;
import cs.core.utils.FileSizeAmplifier;
import cs.core.utils.IdentityDistribution;
import cs.core.utils.QoI;
import cs.core.utils.Statistics;
import cs.core.vm.simulation.Transmission;
import cs.core.vm.simulation.VM;

public class WorkflowEngine {

	private HashSet<JobListener> jobListeners = new HashSet<JobListener>();

	private LinkedList<Job> pendingTransmission;

	/** The list of unmatched ready jobs */
	private PriorityQueue<Job> readyJobsQueue;

	private List<DAGJob> dagJobs;

	// finishing execution time
	private HashMap<DAGJob, Double> simulatedEFT;

	// starting execution time
	private HashMap<DAGJob, Double> simulatedEST;

	private List<DAG> orderOfExecution;

	private Distribution QoI;

	private Distribution fileSizeAmplifier;

	private Environment environment;

	private boolean contention;

	public WorkflowEngine(Environment environment) {
		this.environment = environment;

	}

	public void addJobListener(JobListener l) {
		jobListeners.add(l);
	}

	public void removeJobListener(JobListener l) {
		jobListeners.remove(l);
	}

	public void execute(List<DAGJob> dagJobs, boolean contention) {

		this.contention = contention;
		this.dagJobs = dagJobs;

		queueReadyJobs();
		process();

		if (!pendingTransmission.isEmpty()) {
			throw new RuntimeException("The Workflow Engine still have transmission to carry out!");
		}

		this.pendingTransmission.clear();
	}

	public void setQoI(double uncertainty) {
		if (uncertainty <= 0) {
			this.QoI = new IdentityDistribution();
		} else {
			this.QoI = new QoI(uncertainty);
		}
	}

	public void setDataFileSizeAmplifier(double amplifier) {
		this.fileSizeAmplifier = new FileSizeAmplifier(amplifier);
	}

	private void process() {
		while (true) {
			Job currentJob = readyJobsQueue.poll();
			if (currentJob == null)
				break;

			VM vm = currentJob.getVM();

			DAG dag = currentJob.getTask().getDAG();
			if (!orderOfExecution.contains(dag)) {
				orderOfExecution.add(dag);
			}

			if (vm == null) {
				throw new IllegalStateException("No VM has been bind for the: " + currentJob);
			}

			// execute the currentJob
			execute(vm, currentJob);

			// queue next task in job object
			queueReadyJobs();

			// notify listeners that job has been finished
			jobFinished(currentJob);

			// prepare transmissions
			addTransmissionList(currentJob);
		}

	}

	private void execute(VM vm, Job jobInExecution) {
		// execute the job in the vm
		vm.execute(jobInExecution);
		DAGJob dagJob = jobInExecution.getDAGJob();
		DAG dag = dagJob.getDAG();

		if (!simulatedEFT.containsKey(dagJob)) {
			this.simulatedEFT.put(dagJob, 0.0);
		}

		if (!simulatedEST.containsKey(dagJob)) {
			this.simulatedEST.put(dagJob, Double.MAX_VALUE);
		}

		if (dag.isEntryTask(jobInExecution.getTask())) {
			double startTime = jobInExecution.getStartExecutionTime();
			startTime = Math.min(this.simulatedEST.get(dagJob), startTime);
			this.simulatedEST.put(dagJob, startTime);
		}

		if (dag.isExitTask(jobInExecution.getTask())) {
			double finishTime = jobInExecution.getFinishExecutionTime();
			finishTime = Math.max(this.simulatedEFT.get(dagJob), finishTime);
			this.simulatedEFT.put(dagJob, finishTime);
		}

		jobInExecution.getDAGJob().completeJob(jobInExecution);

	}

	private void addTransmissionList(Job currentJob) {
		pendingTransmission.add(currentJob);
		LinkedList<Job> transmissionsDone = new LinkedList<Job>();

		// make all the transmissions stored in the linkedlist
		for (Job from : pendingTransmission) {
			boolean transmitted = false;

			HashMap<Task, List<Transmission>> transmissions = from.getTransmissions();

			List<Task> toRemove = new LinkedList<Task>();
			for (Task toTask : transmissions.keySet()) {
				List<Transmission> listOfTransmission = transmissions.get(toTask);

				Job to = currentJob.getDAGJob().getReleasedJob(toTask);

				transmitted = transmit(from, to, listOfTransmission);

				if (transmitted) {
					toRemove.add(toTask);
				}
			}

			// remove all transmission that has the taskId 's' of the
			// transmission list
			for (Task s : toRemove) {
				from.removeTransmission(s);
			}

			// check if job has transmission to transmit
			if (!from.hasTransmissions()) {
				transmissionsDone.add(from);
			}
		}

		// remove transmitted transmissions
		pendingTransmission.removeAll(transmissionsDone);

	}

	private boolean transmit(Job from, Job to, List<Transmission> listOfTransmission) {

		// Job to is not ready yet
		// the transmission will have to wait
		if (to == null) {
			return false;
		}

		// Sanity check
		if (from.equals(to)) {
			throw new RuntimeException(from + " is trying to send a file to itself");
		}

		// Sanity check
		if (!from.getDAGJob().equals(to.getDAGJob())) {
			throw new RuntimeException("CROSSOVER: " + from + " is trying to send a file to the " + to);
		}

		VM dest = to.getVM();
		VM src = from.getVM();
		double bytes = 0.0;

		for (Transmission t : listOfTransmission) {
			bytes = t.getDAGFileSize();

			// intra-vm transmission
			if (dest.equals(src)) {
				src.internalTransmit(from, to, t.getDagFile());
				to.setStartExecutionTime(Math.max(to.getStartExecutionTime(), from.getFinishExecutionTime()));
			}

			// inter-vm transmission
			else {

				// get the actual bandwidth value
				// in according with the distribution
				double availableBandwidth = environment.getBandwidth(src, dest);
				bytes = fileSizeAmplifier.getActualValue(bytes);

				double timeTakenToTransmit = QoI.getActualValue(bytes / availableBandwidth);

				double startTransmissionTime = src.sending(from, to, t.getDagFile(), dest, timeTakenToTransmit,
						environment.areWithinTheSameCloud(dest.getVmType(), src.getVmType()), this.contention);

				double finishTransmissionTime = dest.receive(from, to, t.getDagFile(), src, startTransmissionTime,
						timeTakenToTransmit, environment.areWithinTheSameCloud(dest.getVmType(), src.getVmType()),
						this.contention);

				to.setStartExecutionTime(Math.max(to.getStartExecutionTime(), finishTransmissionTime));

			}
		}

		return true;
	}

	private void queueReadyJobs() {
		// Get the ready tasks and convert them into jobs
		for (DAGJob dagJob : this.dagJobs) {
			while (true) {
				Job job = dagJob.nextReadyJob();
				if (job == null)
					break;
				jobReleased(job);
			}
		}
	}

	private void jobReleased(Job j) {
		readyJobsQueue.add(j);

		// Notify listeners that job was released
		for (JobListener jl : jobListeners) {
			jl.jobReleased(j);
		}
	}

	private void jobFinished(Job job) {
		// Notify the listeners that job has finished
		for (JobListener jl : jobListeners) {
			jl.jobFinished(job);
		}

	}

	public void initialize() {

		this.readyJobsQueue = new PriorityQueue<Job>(Job.COMPARE_BY_EST);
		this.simulatedEFT = new HashMap<DAGJob, Double>();
		this.simulatedEST = new HashMap<DAGJob, Double>();
		this.pendingTransmission = new LinkedList<Job>();
		this.QoI = new IdentityDistribution();
		this.fileSizeAmplifier = new IdentityDistribution();
		this.orderOfExecution = new LinkedList<DAG>();
		this.dagJobs = new LinkedList<>();

		for (List<VM> vms : environment.getLeasedVMs().values()) {
			for (VM vm : vms) {
				vm.inicialize();
			}
		}
	}

	public HashMap<DAG, Double> getMakespan() {
		HashMap<DAG, Double> makespans = new HashMap<DAG, Double>(this.dagJobs.size());

		for (DAGJob dagJob : this.dagJobs) {
			// double makespan = this.simulatedEFT.get(dagJob)
			// - this.simulatedEST.get(dagJob);

			double makespan = this.simulatedEFT.get(dagJob) - 0;

			// sanity check
			if (makespan < 0) {
				throw new RuntimeException("The calculation of the makespan is not ready yet!!!!");
			}

			makespans.put(dagJob.getDAG(), makespan);
		}
		return makespans;
	}

	public HashMap<DAG, Double> getSimulatedEFT() {
		HashMap<DAG, Double> fet = new HashMap<>();
		for (DAGJob dagJob : dagJobs) {
			fet.put(dagJob.getDAG(), simulatedEFT.get(dagJob));
		}

		return fet;
	}

	public HashMap<DAG, Double> getSimulatedEST() {
		HashMap<DAG, Double> set = new HashMap<>();
		for (DAGJob dagJob : dagJobs) {
			set.put(dagJob.getDAG(), simulatedEST.get(dagJob));
		}

		return set;
	}

	public List<DAG> getDAGOrderOfExecution() {
		return new LinkedList<DAG>(this.orderOfExecution);
	}

	public double getOverallMakespan() {

		Statistics begin = new Statistics(this.simulatedEST.values());
		Statistics end = new Statistics(this.simulatedEFT.values());

		return end.getMax() - begin.getMin();
	}

}
