package cs.core.engine;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cs.core.dag.DAG;
import cs.core.dag.Task;
import cs.core.utils.BandwidthReductor;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.VM;

public class Environment {

	private static BandwidthReductor reductor = new BandwidthReductor("0.0");

	private Map<String, VMType> vmTypes;

	private Map<String, List<VM>> leasedVMs;

	public Environment(Map<String, VMType> vmTypes) {
		this(vmTypes, "0.0");
	}

	public Environment(Map<String, VMType> vmTypes, String reductor) {
		this.vmTypes = vmTypes;
		this.leasedVMs = new HashMap<String, List<VM>>();
		Environment.reductor = new BandwidthReductor(reductor);
	}

	public static double getPredictedRuntime(Task task, VMType vmType) {
		return task.getSize() / vmType.getMips();
	}

	public double getPredictedRuntime(DAG dag, VMType vmType) {
		double sum = 0.0;
		for (String taskName : dag.getTasks()) {
			sum += getPredictedRuntime(dag.getTaskById(taskName), vmType);
		}
		return sum;
	}

	// function used at the scheduling time, it is before the simulation
	public static double getBandwidth(VMType v1, VMType v2) {
		if (v1.equals(v2)) {
			return Double.MAX_VALUE;
		}
		if (v1.getDatacenterId().equals(v2.getDatacenterId())) {
			return Math.min(v1.getInternalBandwidth(), v2.getInternalBandwidth());
		}
		return Math.min(v1.getExternalBandwidth(), v2.getExternalBandwidth());
	}

	// function used at the execution of the workflow
	// get the bandwidth value between 2 vms
	public double getBandwidth(VM v1, VM v2) {
		VMType vmType1 = v1.getVmType();
		VMType vmType2 = v2.getVmType();
		return getBandwidth(vmType1, vmType2);
	}

	public double getVMCostFor(double runtimeInSeconds, VMType vmType) {
		double billingUnits = runtimeInSeconds / vmType.getBillingTimeInSeconds();
		int fullBillingUnits = (int) Math.ceil(billingUnits);
		return Math.max(1, fullBillingUnits) * vmType.getPriceForBillingUnit();
	}

	public boolean areWithinTheSameCloud(VMType vmType1, VMType vmType2) {
		return vmType1.getDatacenterId().equals(vmType2.getDatacenterId());
	}

	public void addLeasedVM(VM vm) {
		if (!leasedVMs.containsKey(vm.getVmType().getDatacenterId())) {
			leasedVMs.put(vm.getVmType().getDatacenterId(), new LinkedList<VM>());
		}
		leasedVMs.get(vm.getVmType().getDatacenterId()).add(vm);
	}

	// get the deflated bandwidth value for simulation
	public static double getBandwidth(double value) {
		return reductor.getActualValue(value);
	}

	public static void setReductorValue(double value) {
		if (value > 0) {
			reductor.setReductor(value);
			return;
		}
		reductor.setReductor(0.0);
	}

	// FIXME(thiagogenez): temporary encapsulation breakage
	public Map<String, List<VM>> getLeasedVMs() {
		return leasedVMs;
	}

	public List<VM> getUsedVMs() {
		List<VM> used = new LinkedList<>();
		for (List<VM> list : leasedVMs.values()) {
			used.addAll(list);
		}

		return used;
	}

	public List<VMType> getAllVMTypes() {
		LinkedList<VMType> list = new LinkedList<VMType>();
		list.addAll(vmTypes.values());
		return list;
	}

	public void clearLeasedVMs() {
		if (leasedVMs != null) {
			leasedVMs.clear();
		}
	}

}
