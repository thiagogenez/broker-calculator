package cs.core.job;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import cs.core.dag.DAGJob;
import cs.core.dag.Task;
import cs.core.vm.simulation.Transmission;
import cs.core.vm.simulation.VM;

/**
 * A Job is a unit of work that executes on a VM
 * 
 * @author Gideon Juve <juve@usc.edu>
 */
public class Job {
	/** Job states */
	public static enum State {
		QUEUED, IDLE, RUNNING, TERMINATED
	}

	/** Job results */
	public static enum Result {
		NONE, SUCCESS, FAILURE
	}

	/** The ID of this job */
	private String id;

	/** The VM where this job ran */
	private VM vm;

	/** The DAG that spawned this job */
	private DAGJob dagJob;

	/** The task that this job executes */
	private Task task;

	/** Time the job was released */
	private double releaseTime;

	/** Submit time of the job */
	private double submitTime;

	/** The start time of the job */
	private double startTime;

	/** The finish time of the job */
	private double finishTime;

	/** What is the current state of the job? */
	private State state;

	/** Job result */
	private Result result;

	/** If this job is a retry of the task */
	private boolean isRetry = false;

	private HashMap<Task, List<Transmission>> transmissions;

	public Job(DAGJob dagJob, Task task, String id) {
		this.id = id;
		this.state = State.QUEUED;
		this.result = Result.NONE;
		this.dagJob = dagJob;
		this.task = task;
		this.transmissions = new HashMap<Task, List<Transmission>>();
	}

	public String getID() {
		return id;
	}

	public void setVM(VM vm) {
		this.vm = vm;
	}

	public VM getVM() {
		return vm;
	}

	public DAGJob getDAGJob() {
		return dagJob;
	}

	public Task getTask() {
		return task;
	}

	public void setReleaseTime(double releaseTime) {
		this.releaseTime = releaseTime;
	}

	public double getReleaseTime() {
		return this.releaseTime;
	}

	public void setSubmitTime(double submitTime) {
		this.submitTime = submitTime;
	}

	public double getSubmitTime() {
		return submitTime;
	}

	public void setStartExecutionTime(double startTime) {
		this.startTime = startTime;
	}

	public double getStartExecutionTime() {
		return startTime;
	}

	public void setFinishExecutionTime(double finishTime) {
		this.finishTime = finishTime;
	}

	public double getFinishExecutionTime() {
		return finishTime;
	}

	public double getDuration() {
		return finishTime - startTime;
	}

	public void setState(State state) {
		this.state = state;
	}

	public State getState() {
		return state;
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public Result getResult() {
		return result;
	}

	// @Override
	// public int hashCode() {
	// int id1 = Integer.parseInt(id.replaceAll("[^0-9.]", ""));
	// final int prime = 31;
	// int result = 1;
	// result = prime * result + id1;
	// return result;
	// }
	//
	// @Override
	// public boolean equals(Object obj) {
	// if (this == obj)
	// return true;
	// if (obj == null)
	// return false;
	// if (getClass() != obj.getClass())
	// return false;
	// Job other = (Job) obj;
	// if (id != other.id)
	// return false;
	// return true;
	// }

	@Override
	public String toString() {
		return String.format("Job %s (task_id = %s, workflow = %s)", getID(), getTask().getId(),
				getDAGJob().getDAG().getId());
	}

	public boolean isRetry() {
		return isRetry;
	}

	public void setRetry(boolean retry) {
		isRetry = retry;
	}

	public void addTransmission(Task to, Transmission transmission) {

		if (transmissions != null) {
			if (transmissions.containsKey(to)) {
				transmissions.get(to).add(transmission);
			} else {
				ArrayList<Transmission> list = new ArrayList<Transmission>();
				list.add(transmission);
				transmissions.put(to, list);
			}
		}
	}

	// FIXME(thiagogenez): temporary encapsulation breakage
	public HashMap<Task, List<Transmission>> getTransmissions() {
		return transmissions;
	}

	// public List<Transmission> getTransmissions(Task task) {
	// LinkedList<Transmission> list = new LinkedList<Transmission>();
	// if (transmissions != null && transmissions.containsKey(task)) {
	// list.addAll(transmissions.get(task.getId()));
	// }
	// return list;
	// }

	public boolean hasTransmissions() {
		return !this.transmissions.isEmpty();
	}

	public void removeTransmission(Task to) {
		if (transmissions != null && transmissions.containsKey(to)) {
			transmissions.remove(to);
		}
	}

	public static Comparator<Job> COMPARE_BY_EST = new Comparator<Job>() {
		public int compare(Job one, Job other) {
			return Double.compare(one.task.geAST(), other.task.geAST());
		}
	};

}
