package cs.core.utils;

import cs.core.engine.Distribution;

public class BandwidthReductor implements Distribution {

	private double reductor;

	public BandwidthReductor(double reductor) {
		this.reductor = reductor;
	}

	public BandwidthReductor(String reductor) {
		this.reductor = Double.parseDouble(reductor);
	}

	@Override
	public double getActualValue(double value) {
		return value - (value * reductor);
	}

	public double getReductor() {
		return reductor;
	}

	public void setReductor(double reductor) {
		this.reductor = reductor;
	}

}
