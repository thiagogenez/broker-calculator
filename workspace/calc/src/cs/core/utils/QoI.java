package cs.core.utils;

import java.util.Random;

import cs.core.engine.Distribution;

public class QoI implements Distribution {

	private Random random;

	private double uncertainty;

	public QoI(double uncertainty) {
		if (uncertainty <= 0) {
			throw new RuntimeException("uncertainty value must be > 0");
		}

		this.random = new Random(System.currentTimeMillis());
		this.uncertainty = uncertainty;
	}

	@Override
	public double getActualValue(double value) {
		// Get a random number in the range [-1,+1]

		double plusorminus = (random.nextDouble() * 2.0d) - 1.0d;
		return value + (plusorminus * uncertainty * value);

	}

	public static void main(String[] args) {
		QoI d = new QoI(0.25);

		for (int i = 0; i < 50; i++) {
			System.out.println(d.getActualValue(10));
		}
		System.out.println(" ");
	}
}
