package cs.core.utils;

public class AssortedMethods {

	public static double randomDouble() {
		return Math.random();
	}

	public static int randomInt(int n) {
		return (int) (Math.random() * n);
	}

	public static int randomIntInRange(int min, int max) {
		return randomInt(max + 1 - min) + min;
	}

	public static void main(String[] args) {
		for (int i = 0; i < 20; i++) {
			System.out.println(randomIntInRange(99, 101) / 100.0);
		}
	}

}
