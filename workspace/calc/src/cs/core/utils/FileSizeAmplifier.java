package cs.core.utils;

import cs.core.engine.Distribution;

public class FileSizeAmplifier implements Distribution {

	private double amplifier;

	public FileSizeAmplifier() {
		this.amplifier = 1.0;
	}

	public FileSizeAmplifier(double amplifier) {
		this.amplifier = amplifier;
	}

	@Override
	public double getActualValue(double value) {
		return value * amplifier;
	}

}
