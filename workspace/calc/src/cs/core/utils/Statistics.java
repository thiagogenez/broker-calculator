package cs.core.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Statistics {

	private double[] values;

	private double sum;

	private double sumSquared;

	private double min;

	private double max;

	private int size;

	public Statistics(Collection<Double> values) {
		sum = 0.0;
		sumSquared = 0.0;
		min = Double.MAX_VALUE;
		max = 0.0;
		this.values = new double[values.size()];
		size = 0;

		for (Double value : values) {
			add(value);
		}
	}

	public void add(double value) {
		values[size] = value;
		sum += value;
		sumSquared += (value * value);
		min = Math.min(min, value);
		max = Math.max(max, value);
		size++;
	}

	public double getMean() {
		if (values == null || values.length == 0) {
			return Double.NaN;
		}

		return sum / values.length;
	}

	public double getVariance() {

		double mean = getMean();
		double dv = 0;
		double n = values.length;

		for (double d : values) {
			dv += (d - mean) * (d - mean);
		}

		return dv / (n - 1);
	}

	public double getPopulationVariance() {
		double mean = getMean();
		if (Double.isNaN(mean)) {
			return Double.NaN;
		}

		double dv = 0;
		double n = values.length;

		for (double d : values) {
			dv += (d - mean) * (d - mean);
		}

		return dv / n;
	}

	public double getStandardDeviation() {
		return Math.sqrt(getVariance());
	}

	public double getPopulationStandardDeviation() {
		return Math.sqrt(getPopulationVariance());
	}

	public double getMax() {
		return max;
	}

	public double getMin() {
		return min;
	}

	public double getSum() {
		return sum;
	}

	public double getSumSquared() {
		return sumSquared;
	}

	public int getN() {
		return size;
	}

	public static void main(String[] args) {
		List<Double> v = new ArrayList<>();
		for (int i = 1; i <= 15; i++) {
			v.add(i / 1.0);
		}

		Statistics stats = new Statistics(v);

		System.out.println("mean: " + stats.getMean());
		System.out.println("variance: " + stats.getVariance());
		System.out.println("pop variance: " + stats.getPopulationVariance());
		System.out.println("std: " + stats.getStandardDeviation());
		System.out.println("pop std: " + stats.getPopulationStandardDeviation());

	}

	public double[] getValues() {
		return Arrays.copyOf(values, size);
	}
}