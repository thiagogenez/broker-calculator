package cs.core.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

public class CSVFileWriter {
	private FileWriter fileWriter = null;

	private CSVPrinter csvFilePrinter = null;

	private CSVFormat csvFileFormat = CSVFormat.DEFAULT;

	public CSVFileWriter(String path, String filename, Object[] header) {

		try {
			// initialize FileWriter object
			fileWriter = new FileWriter(new File(path, filename + ".csv"));

			// initialize CSVPrinter object
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

			// Create CSV file header
			csvFilePrinter.printRecord(header);
		} catch (IOException e) {
			System.err.println("File not open, redirecting to STDOUT");
			try {
				csvFilePrinter = new CSVPrinter(System.out, csvFileFormat);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	public void write(List<Object> line) {
		try {
			csvFilePrinter.printRecord(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void close() {
		if (fileWriter != null) {
			try {
				fileWriter.flush();
				fileWriter.close();
				csvFilePrinter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
