package cs.core.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {

	/** The input stream to write logs to */
	private PrintStream printStream;

	/** Whether logging is enabled. Defaults to true. */
	private boolean logsEnabled = true;

	private String path = "/tmp";

	private String filename = "simulation-" + new SimpleDateFormat("yyyy-MM-dd-HH_mm_ss").format(new Date());

	private String ext = ".log";

	public Logger(String path, String filename, String ext, boolean enable) {
		this.logsEnabled = enable;
		this.path = path;
		this.filename = filename;
		this.ext = ext;

		if (enable) {
			try {
				OutputStream outputStream = new FileOutputStream(new File(path, filename + ext));
				this.printStream = new PrintStream(outputStream);
			} catch (FileNotFoundException e) {
				System.err.println("File not open, redirecting to STDOUT");
				printStream = System.out;
			}
		}
	}

	public Logger() {
		printStream = System.out;
	}

	/**
	 * Logs the given message to previously set output stream.
	 * 
	 * @param msg
	 *            The message to logs.
	 */
	public void logln(String msg) {
		if (logsEnabled) {
			printStream.println(msg);
		}
	}

	public void log(String msg) {
		if (logsEnabled) {
			printStream.print(msg);
		}
	}

	public void error(String msg) {
		if (logsEnabled) {
			printStream.println(msg);
		}
	}

	public void error(String format, Object... args) {
		if (logsEnabled) {
			printStream.printf(format, args);
		}
	}

	public void log(String format, Object... args) {
		if (logsEnabled) {
			printStream.printf(format, args);
		}
	}

	public void close() {
		if (logsEnabled && printStream != System.out) {
			printStream.flush();
			printStream.close();
		}
	}

	/**
	 * @param logsEnabled
	 *            Whether logging should be enabled.
	 */
	public void setLogsEnabled(boolean logsEnabled) {
		this.logsEnabled = logsEnabled;
	}

	public PrintStream getPrintStream() {
		return printStream;
	}

	public String getPath() {
		return path;
	}

	public String getFilename() {
		return filename;
	}

	public String getExt() {
		return ext;
	}

}
