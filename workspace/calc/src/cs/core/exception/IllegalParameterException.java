package cs.core.exception;

import com.beust.jcommander.ParameterException;

public class IllegalParameterException extends ParameterException {

	public IllegalParameterException(String string) {
		super(string);
	}

	private static final long serialVersionUID = 1L;

}
