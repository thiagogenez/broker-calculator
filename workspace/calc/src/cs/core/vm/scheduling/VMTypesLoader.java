package cs.core.vm.scheduling;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.error.YAMLException;

import cs.core.exception.IllegalParameterException;
import cs.core.exception.SimulatiorException;
import cs.simulation.config.VMParameters;

/**
 * Loads VMType from *.vm.yaml config files.
 * 
 * This loader loads heterogeneous vms from within a provider. The VM options
 * are read from input file only! VMs params can't be overload by command line
 * 
 * @author Thiago Genez <thiagogenez@ic.unicamp.br>
 *         <thiago.genez@manchester.ac.uk>
 */
public class VMTypesLoader {

	public static Map<String, String> CLOUD_PROVIDER_FILENAME = new HashMap<String, String>();

	public Map<String, VMType> determineVMTypes(VMParameters vmParameters) throws IllegalParameterException {
		List<Map<String, Object>> vmConfigs = tryLoadVMFromConfigFiles(vmParameters);

		int vmId = 0;
		Map<String, VMType> vms = new HashMap<String, VMType>();
		for (int cloudProviderId = 0; cloudProviderId < CLOUD_PROVIDER_FILENAME.size(); cloudProviderId++) {
			Map<String, Object> vmConfig = vmConfigs.get(cloudProviderId);
			Map<String, VMType> vmsLoaded = loadVMs(vmConfig, vmId, cloudProviderId, vmParameters);
			vmId += vmsLoaded.size();
			vms.putAll(vmsLoaded);
		}

		return vms;
	}

	Map<String, VMType> loadVMs(Map<String, Object> config, int vmId, int cloudProviderId, VMParameters vmParameters)
			throws IllegalParameterException {

		Map<String, VMType> vms = new HashMap<String, VMType>();
		Map<String, Object> bandwidthConfig = getBandwidthSection(config);
		Map<String, Object> dataTransferOutsideConfig = getDataTransferSection(config);

		double internalBandwidth = -1.0;
		if (vmParameters.getInternalBandwidth() > 0.0) {
			internalBandwidth = vmParameters.getInternalBandwidth();
		} else if (bandwidthConfig != null) {
			if (bandwidthConfig.get("internal") instanceof Integer) {
				internalBandwidth = new Double((int) bandwidthConfig.get("internal")).doubleValue();
			}

			else {
				internalBandwidth = (double) bandwidthConfig.get("internal");
			}
		} else {
			throw new RuntimeException("Internal bandwidth value is missing");
		}

		double externalBandwidth = -1.0;
		if (vmParameters.getExternalBandwidth() > 0.0) {
			externalBandwidth = vmParameters.getExternalBandwidth();
		} else if (bandwidthConfig != null) {
			if (bandwidthConfig.get("external") instanceof Integer) {
				externalBandwidth = new Double((int) bandwidthConfig.get("external")).doubleValue();
			} else {
				externalBandwidth = (double) bandwidthConfig.get("external");
			}
		} else {
			throw new RuntimeException("External bandwidth value is missing");
		}

		double dataTransferInPrice = (double) dataTransferOutsideConfig.get("in");
		double dataTransferOutPrice = (double) dataTransferOutsideConfig.get("out");
		double dataTransferUnit = (double) dataTransferOutsideConfig.get("unitData");

		Map<String, Object> vmsConfig = getVMsSection(config);
		for (String nameKey : vmsConfig.keySet()) {
			@SuppressWarnings("unchecked")
			Map<String, Object> vmConfig = (Map<String, Object>) vmsConfig.get(nameKey);
			VMType vmType = loadVM(vmConfig, internalBandwidth, externalBandwidth, vmId++, cloudProviderId,
					dataTransferInPrice, dataTransferOutPrice, dataTransferUnit, nameKey);
			vms.put(vmType.getId(), vmType);
		}

		return vms;

	}

	VMType loadVM(Map<String, Object> config, double internalBandwidth, double externalBandwidth, int idVm,
			int cloudProviderId, double dataTransferIn, double dataTransferOut, double dataTransferUnit, String nameKey)
			throws IllegalParameterException {

		Map<String, Object> billingConfig = getBillingSection(config);
		double unitPrice = ((Number) billingConfig.get("unitPrice")).doubleValue();
		double unitTime = ((Number) billingConfig.get("unitTime")).doubleValue();

		double mips = ((Number) config.get("mips")).doubleValue();
		int cores = ((Number) config.get("cores")).intValue();

		return VMTypeBuilder.newBuilder().mips(mips).cores(cores).price(unitPrice).billingTimeInSeconds(unitTime)
				.externalBandwidth(externalBandwidth).internalBandwidth(internalBandwidth).id(String.valueOf(idVm))
				.datacenterId(String.valueOf(cloudProviderId)).dataTransferInPrice(dataTransferIn)
				.dataTransferOutPrice(dataTransferOut).dataTransferUnit(dataTransferUnit).name(nameKey).build();
	}

	private List<Map<String, Object>> tryLoadVMFromConfigFiles(VMParameters parameters) {
		try {
			return loadVMFromConfigFiles(parameters);
		} catch (FileNotFoundException e) {
			throw new IllegalParameterException("Cannot load VM config file: " + e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	private List<Map<String, Object>> loadVMFromConfigFiles(VMParameters parameters) throws FileNotFoundException {
		List<Map<String, Object>> files = new ArrayList<Map<String, Object>>();

		int cloudProviderId = 0;
		for (String configFilename : parameters.getVms()) {
			InputStream input = new FileInputStream(new File(parameters.getVmDir(), configFilename));
			Yaml yaml = new Yaml();
			try {
				files.add((Map<String, Object>) yaml.load(input));
				CLOUD_PROVIDER_FILENAME.put(String.valueOf(cloudProviderId++), configFilename);
			} catch (YAMLException e) {
				throw new SimulatiorException("Cannot parse VM config file: " + e.getMessage());
			}

		}
		return files;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getBandwidthSection(Map<String, Object> config) {
		if (config.containsKey("bandwidth")) {
			return (Map<String, Object>) config.get("bandwidth");
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getDataTransferSection(Map<String, Object> config) {
		return (Map<String, Object>) config.get("dataTransferCostOutside");
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getVMsSection(Map<String, Object> config) {
		return (Map<String, Object>) config.get("vms");
	}

	@SuppressWarnings("unchecked")
	private Map<String, Object> getBillingSection(Map<String, Object> vmConfig) {
		return (Map<String, Object>) vmConfig.get("billing");
	}
}
