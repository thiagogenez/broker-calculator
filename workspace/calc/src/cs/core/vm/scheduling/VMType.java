package cs.core.vm.scheduling;

import java.io.Serializable;

import cs.core.engine.Environment;

public class VMType implements Cloneable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2892341845784421672L;

	/** Globally uniqe task id */
	private final String id;

	private final String name;

	private final String datacenterId;

	/**
	 * The processing power of this VM
	 */
	private double mips;

	/**
	 * The number of cores of this VM
	 */
	private final int cores;

	/**
	 * Price per billing unit of usage
	 */
	private double billingUnitPrice;

	/**
	 * For how long we pay in advance
	 */
	private double billingTimeInSeconds;

	/**
	 * Intra-bandwidth value within a datacenter
	 */
	private double internalBandwidth;

	/**
	 * Inter-vandwidth value outside a datacenter
	 */
	private double externalBandwidth;

	private double dataTransferInPrice;

	private double dataTransferOutPrice;

	private double dataTransferUnit;

	public double getMips() {
		return mips;
	}

	public int getCores() {
		return cores;
	}

	public double getPriceForBillingUnit() {
		return billingUnitPrice;
	}

	public double getBillingTimeInSeconds() {
		return billingTimeInSeconds;
	}

	// get the reduced bandwidth value in accordance
	// with the input data of the simulation
	public double getInternalBandwidth() {
		return Environment.getBandwidth(internalBandwidth);
	}

	// get the reduced bandwidth value in accordance
	// with the input data of the simulation
	public double getExternalBandwidth() {
		return Environment.getBandwidth(externalBandwidth);
	}

	public String getId() {
		return id;
	}

	public String getDatacenterId() {
		return datacenterId;
	}

	public double getDataTransferInPrice() {
		return dataTransferInPrice;
	}

	public double getDataTransferOutPrice() {
		return dataTransferOutPrice;
	}

	public double getDataTransferUnit() {
		return dataTransferUnit;
	}

	public String getName() {
		return name;
	}

	public VMType(double mips, int cores, double billingUnitPrice, double billingTimeInSeconds,
			double internalBandwidth, double externalBandwidth, String id, String datacenterId,
			double dataTransferInPrice, double dataTransferOutPrice, double dataTransferUnit, String name) {
		this.mips = mips;
		this.cores = cores;
		this.billingUnitPrice = billingUnitPrice;
		this.billingTimeInSeconds = billingTimeInSeconds;
		this.internalBandwidth = internalBandwidth;
		this.externalBandwidth = externalBandwidth;
		this.id = id;
		this.datacenterId = datacenterId;
		this.dataTransferInPrice = dataTransferInPrice;
		this.dataTransferOutPrice = dataTransferOutPrice;
		this.dataTransferUnit = dataTransferUnit;
		this.name = name;
	}

	// public boolean hasContention() {
	// return contention;
	// }

	@Override
	public String toString() {
		return "<" + getName() + " id=" + getId() + "," + getDatacenterId() + ":" + getCores() + ">";
	}

	public void setMips(double value) {
		this.mips = value;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public void setInternalBandwidth(double internalBandwidth) {
		this.internalBandwidth = internalBandwidth;
	}

	public void setExternalBandwidth(double externalBandwidth) {
		this.externalBandwidth = externalBandwidth;
	}

	public double getBillingUnitPrice() {
		return billingUnitPrice;
	}

	public void setBillingUnitPrice(double billingUnitPrice) {
		this.billingUnitPrice = billingUnitPrice;
	}

	public void setBillingTimeInSeconds(double billingTimeInSeconds) {
		this.billingTimeInSeconds = billingTimeInSeconds;
	}
	
	
	

}
