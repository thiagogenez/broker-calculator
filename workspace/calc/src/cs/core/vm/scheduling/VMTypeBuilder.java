package cs.core.vm.scheduling;

public class VMTypeBuilder {
	private static final double DEFAULT_BILLING_TIME = 3600;

	private static final double DEFAULT_DATA_UNIT = 1.0;

	public static MipsStep newBuilder() {
		return new Steps();
	}

	public interface MipsStep {
		CoresStep mips(double mips);
	}

	public interface CoresStep {
		PriceStep cores(int cores);
	}

	public interface PriceStep {
		OptionalsStep price(double price);
	}

	public interface OptionalsStep {
		OptionalsStep billingTimeInSeconds(double billingTimeInSeconds);

		OptionalsStep internalBandwidth(double internalBandwidth);

		OptionalsStep externalBandwidth(double externalBandwidth);

		OptionalsStep id(String id);

		OptionalsStep datacenterId(String id);

		OptionalsStep dataTransferInPrice(double dataTransferIn);

		OptionalsStep dataTransferOutPrice(double dataTransferOut);

		OptionalsStep dataTransferUnit(double dataTransferUnit);

		OptionalsStep name(String name);

		VMType build();
	}

	static class Steps implements MipsStep, CoresStep, PriceStep, OptionalsStep {
		private double mips;
		private int cores;
		private double price;
		private double internalBandwidth;
		private double externalBandwidth;
		private String id;
		private String datacenterId;
		private double dataTransferInPrice;
		private double dataTransferOutPrice;
		private double dataTransferUnit = DEFAULT_DATA_UNIT;
		private String name;

		private double billingTimeInSeconds = DEFAULT_BILLING_TIME;

		@Override
		public CoresStep mips(double mips) {
			this.mips = mips;
			return this;
		}

		@Override
		public PriceStep cores(int cores) {
			this.cores = cores;
			return this;
		}

		@Override
		public OptionalsStep price(double price) {
			this.price = price;
			return this;
		}

		@Override
		public OptionalsStep billingTimeInSeconds(double billingTimeInSeconds) {
			this.billingTimeInSeconds = billingTimeInSeconds;
			return this;
		}

		@Override
		public VMType build() {
			return new VMType(mips, cores, price, billingTimeInSeconds, internalBandwidth, externalBandwidth, id,
					datacenterId, dataTransferInPrice, dataTransferOutPrice, dataTransferUnit, name);
		}

		@Override
		public OptionalsStep internalBandwidth(double internalBandwidth) {
			this.internalBandwidth = internalBandwidth;
			return this;
		}

		@Override
		public OptionalsStep externalBandwidth(double externalBandwidth) {
			this.externalBandwidth = externalBandwidth;
			return this;
		}

		@Override
		public OptionalsStep id(String id) {
			this.id = id;
			return this;
		}

		@Override
		public OptionalsStep datacenterId(String id) {
			this.datacenterId = id;
			return this;
		}

		@Override
		public OptionalsStep dataTransferInPrice(double dataTransferIn) {
			this.dataTransferInPrice = dataTransferIn;
			return this;
		}

		@Override
		public OptionalsStep dataTransferOutPrice(double dataTransferOut) {
			this.dataTransferOutPrice = dataTransferOut;
			return this;
		}

		@Override
		public OptionalsStep dataTransferUnit(double dataTransferUnit) {
			this.dataTransferUnit = dataTransferUnit;
			return this;
		}

		@Override
		public OptionalsStep name(String name) {
			this.name = name;
			return this;
		}

	}
}
