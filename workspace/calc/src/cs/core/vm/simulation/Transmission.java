
package cs.core.vm.simulation;

import cs.core.dag.DAGFile;
import cs.core.dag.Task;

public class Transmission{

	private Task to;

	private DAGFile dagFile;

	private double startTime = 0.0;

	private double duration = 0.0;

	public Transmission(Task to, DAGFile dagFile) {
		this.to = to;
		this.dagFile = dagFile;
	}

	public Transmission(Task to, DAGFile dagFile, double startTime, double duration) {
		this.to = to;
		this.dagFile = dagFile;
		this.startTime = startTime;
		this.duration = duration;
	}

	public Task getReceiver() {
		return to;
	}

	public double getDAGFileSize() {
		return dagFile.getSize();
	}

	public DAGFile getDagFile() {
		return dagFile;
	}

	public double getStartTime() {
		return startTime;
	}

	public double getDuration() {
		return duration;
	}

	public double getFinishTime() {
		return startTime + duration;
	}


}
