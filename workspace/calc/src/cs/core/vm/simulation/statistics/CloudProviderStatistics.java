package cs.core.vm.simulation.statistics;

public class CloudProviderStatistics {

	private String id;

	private String name;

	private double executionCost;

	private double runtimeUsed;

	private double dataTransferFromOutside;

	private double dataTransferToOutside;

	private double dataTransferCostFromOutside;

	private double dataTransferCostToOutside;

	public CloudProviderStatistics(String id) {
		this.id = id;
	}

	public CloudProviderStatistics(String id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public double getExecutionCost() {
		return executionCost;
	}

	public double getRuntimeUsed() {
		return runtimeUsed;
	}

	public void setExecutionCost(double executionCost) {
		this.executionCost = executionCost;
	}

	public void setRuntimeUsed(double runtimeUsed) {
		this.runtimeUsed = runtimeUsed;
	}

	public double getDataTransferFromOutside() {
		return dataTransferFromOutside;
	}

	public void setDataTransferFromOutside(double dataTransferFromOutside) {
		this.dataTransferFromOutside = dataTransferFromOutside;
	}

	public double getDataTransferToOutside() {
		return dataTransferToOutside;
	}

	public void setDataTransferToOutside(double dataTransferToOutside) {
		this.dataTransferToOutside = dataTransferToOutside;
	}

	public double getDataTransferCostFromOutside() {
		return dataTransferCostFromOutside;
	}

	public void setDataTransferCostFromOutside(double dataTransferCostFromOutside) {
		this.dataTransferCostFromOutside = dataTransferCostFromOutside;
	}

	public double getDataTransferCostToOutside() {
		return dataTransferCostToOutside;
	}

	public void setDataTransferCostToOutside(double dataTransferCostToOutside) {
		this.dataTransferCostToOutside = dataTransferCostToOutside;
	}

	public double getTotalMonetaryCost() {
		return this.executionCost + this.dataTransferCostFromOutside + this.dataTransferCostToOutside;
	}

	public double getTotalMonetaryTransferCost() {
		return this.dataTransferCostFromOutside + this.dataTransferCostToOutside;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
