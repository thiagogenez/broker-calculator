package cs.core.vm.simulation.statistics;

import java.util.HashMap;
import java.util.LinkedList;

import cs.core.dag.DAGFile;

public class CommunicationChannelStatistics {

	private Channel filesToSend;

	private long totalBytesOut;

	private Channel filesToReceive;

	private long totalBytesIn;

	public CommunicationChannelStatistics() {
		this.filesToReceive = new Channel();
		this.filesToSend = new Channel();
		this.totalBytesIn = 0;
		this.totalBytesOut = 0;
	}

	public void clear() {
		filesToReceive.clear();
		filesToSend.clear();
		totalBytesIn = 0;
		totalBytesOut = 0;
	}

	public void addFileToSend(String to, DAGFile dagFile) {
		filesToSend.addFile(to, dagFile);
		this.totalBytesOut += dagFile.getSize();
	}

	public void addFileToReceive(String from, DAGFile dagFile) {
		filesToReceive.addFile(from, dagFile);
		this.totalBytesIn += dagFile.getSize();
	}

	public long getTotalBytesOut() {
		return totalBytesOut;
	}

	public long getTotalBytesIn() {
		return totalBytesIn;
	}

	@Override
	public String toString() {
		StringBuffer statistics = new StringBuffer();
		statistics.append("\n\tTotal bytes sent:");
		statistics.append(filesToSend);
		statistics.append("\n\tTotal bytes received:");
		statistics.append(filesToReceive);
		return statistics.toString();
	}

	private class Channel {

		private HashMap<String, LinkedList<DAGFile>> files;

		public Channel() {
			this.files = new HashMap<String, LinkedList<DAGFile>>();
		}

		public void clear() {
			files.clear();
		}

		public void addFile(String id, DAGFile dagFile) {
			if (!files.containsKey(id)) {
				files.put(id, new LinkedList<DAGFile>());
			}
			files.get(id).add(dagFile);
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			long totalBytes = 0;
			for (String datacenterId : files.keySet()) {
				long bytes = 0;
				LinkedList<DAGFile> dagFiles = files.get(datacenterId);
				for (DAGFile file : dagFiles) {
					bytes += file.getSize();
				}
				totalBytes += bytes;
				builder.append("\n\t\t-cloud provider " + datacenterId + ": " + bytes);
			}
			StringBuilder result = new StringBuilder();
			result.append(totalBytes);
			result.append(builder);
			return result.toString();
		}
	}
}
