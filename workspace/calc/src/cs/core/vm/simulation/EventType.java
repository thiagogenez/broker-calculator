package cs.core.vm.simulation;

enum EventType {

	NONE("none"), EXECUTION("execution"), SENDING_WITHIN_THE_CLOUD("sending-file-within"), SENDING_OUT_OF_THE_CLOUD(
			"sending-file-out"), RECEIVING_WITHIN_THE_CLOUD("receiving-file-within"), RECEIVING_OUT_OF_THE_CLOUD(
					"receiving-file-out"), AUTO_TRANSMISSION("auto-transmission"), WAIT_TO_TRANSMIT("wait-to-transmit");

	private final String name;

	private EventType(String s) {
		name = s;
	}

	@Override
	public String toString() {
		return name;
	}

}