package cs.core.vm.simulation;

import com.google.gson.JsonObject;

import cs.core.dag.DAGFile;
import cs.core.job.Job;

class Event implements Comparable<Event> {

	private EventType type;

	private DAGFile dagFile;

	private Job sender;

	private Job executor;

	private Job receiver;

	private double start;

	private double end;

	// For transmission
	public Event(EventType type, Job sender, Job receiver, DAGFile dagFile, double start, double end) {
		this.type = type;
		this.sender = sender;
		this.receiver = receiver;
		this.executor = null;
		this.dagFile = dagFile;
		this.start = start;
		this.end = end;
	}

	// For execution
	public Event(EventType type, Job executor, double start, double end) {
		this.type = type;
		this.sender = null;
		this.receiver = null;
		this.executor = executor;
		this.dagFile = null;
		this.start = start;
		this.end = end;
	}

	// public Boolean isTypeEqualTo(TypeNo type) {
	// return this.type.equals(type);
	// }

	public JsonObject getJsonLog() {
		JsonObject log = new JsonObject();

		log.addProperty("action", this.type.toString());

		if (type == EventType.EXECUTION) {
			log.addProperty("task", executor.getTask().toString());
		} else {
			log.addProperty("filename", dagFile.getName());
			log.addProperty("from-vm", receiver.getVM().getVmType().toString());
			log.addProperty("task-sender", sender.getTask().toString());
			log.addProperty("task-receiver", receiver.getTask().toString());
		}

		log.addProperty("start", this.start);
		log.addProperty("end", this.end);

		return log;
	}

	public String toString() {
		StringBuffer action = new StringBuffer("\n");
		StringBuffer actors = new StringBuffer();
		StringBuffer transfers = new StringBuffer("");
		StringBuffer time = new StringBuffer(String.format("start: %1$,.5f\t end: %2$,.5f\t", this.start, this.end));

		switch (this.type) {
		case EXECUTION:
			action.append("Executing task");
			actors.append(executor.getTask());
			break;
		case SENDING_WITHIN_THE_CLOUD:
			action.append("Sending within the cloud ");
			transfers.append(dagFile.getName() + " to " + receiver.getVM().getVmType());
			actors.append(sender.getTask() + " to " + receiver.getTask());
			break;
		case SENDING_OUT_OF_THE_CLOUD:
			action.append("Sending out of the cloud ");
			transfers.append(dagFile.getName() + " to " + receiver.getVM().getVmType());
			actors.append(sender.getTask() + " to " + receiver.getTask());
			break;
		case RECEIVING_WITHIN_THE_CLOUD:
			action.append("Receiving within the cloud ");
			transfers.append(dagFile.getName() + " from " + sender.getVM().getVmType());
			actors.append(sender.getTask() + " to " + receiver.getTask());
			break;
		case RECEIVING_OUT_OF_THE_CLOUD:
			action.append("Receiving out of the cloud ");
			transfers.append(dagFile.getName() + " from " + sender.getVM().getVmType());
			actors.append(sender.getTask() + " to " + receiver.getTask());
			break;
		case AUTO_TRANSMISSION:
			action.append("Auto-transmission ");
			transfers.append(dagFile.getName());
			actors.append(sender.getTask() + " to " + receiver.getTask());
			break;
		case WAIT_TO_TRANSMIT:
			action.append("Waiting to transmite the file ");
			transfers.append(dagFile.getName() + " to " + receiver.getVM().getVmType());
			actors.append(sender.getTask() + " to " + receiver.getTask());
			break;
		default:
			action.append("Error, event unknown");
			break;
		}

		String format = "%1$-30s\t%2$-30s\t%3$-40s\t%4$s";
		return String.format(format, action, transfers, actors, time);
	}

	public double getStart() {
		return start;
	}

	public double getEnd() {
		return end;
	}

	public EventType getType() {
		return type;
	}

	public Job getExecutor() {
		return executor;
	}

	@Override
	public int compareTo(Event o) {
		if (this.getStart() < o.getStart()) {
			return -1;
		} else if (this.getStart() > o.getStart()) {
			return 1;
		} else if (this.getEnd() < o.getEnd())
			return -1;
		else if (this.getEnd() > o.getEnd())
			return 1;

		return 0;
	}

}