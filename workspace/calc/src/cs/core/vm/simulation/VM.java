
package cs.core.vm.simulation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import com.google.gson.JsonObject;

import cs.core.dag.DAGFile;
import cs.core.dag.DAGJob;
import cs.core.dag.Task;
import cs.core.job.Job;
import cs.core.vm.VMCore;
import cs.core.vm.scheduling.VMType;
import cs.core.vm.simulation.statistics.CommunicationChannelStatistics;

public class VM {

	private static int nextId = 0;

	private int id = 0;

	private VMType vmType;

	private List<VMCore> cores;

	private VMEvents events;

	private double availableNetworkTime;

	private CommunicationChannelStatistics outsideTheCloudTransmission;

	private CommunicationChannelStatistics insideTheCloudTransmission;

	public VM(VMType vmType) {
		this.id = nextId++;
		this.vmType = vmType;

		inicialize();
	}

	public void inicialize() {

		this.cores = new LinkedList<>();
		for (int i = 0; i < vmType.getCores(); i++) {
			this.cores.add(new VMCore(String.valueOf(i), String.valueOf(this.id)));
		}

		this.events = new VMEvents(this);
		this.outsideTheCloudTransmission = new CommunicationChannelStatistics();
		this.insideTheCloudTransmission = new CommunicationChannelStatistics();
	}

	public double execute(Job job) {
		DAGJob dagJob = job.getDAGJob();
		double runtime = job.getTask().getSize() / this.vmType.getMips();

		/*
		 * List of all possible init task execution
		 */
		PriorityQueue<Double> possibleInitTimes = new PriorityQueue<Double>(Collections.reverseOrder());

		/*
		 * Add two possible times, the first one is time that next core was available;
		 * and the second one is the start execution task expected
		 */
		VMCore selectedCore = getNextCoreAvailable(job.getStartExecutionTime(), runtime);
		possibleInitTimes.add(selectedCore.getEarliestIdleSlotTimeFrom(job.getStartExecutionTime(), runtime));
		possibleInitTimes.add(job.getStartExecutionTime());

		/*
		 * Get maximum finish time for all predecessors of the task
		 */
		Double lastestFinishTime = 0.0;
		for (Task parent : job.getTask().getParents()) {
			if (dagJob.isComplete(parent)) {
				lastestFinishTime = Math.max(lastestFinishTime,
						dagJob.getCompletedJob(parent).getFinishExecutionTime());
			} else {
				throw new IllegalStateException("A child task " + job.getTask().toString() + "before the parent "
						+ parent.toString() + " has completed its execution");
			}
		}
		possibleInitTimes.add(lastestFinishTime);
		
		/*
		 * 
		 */

		/*
		 * Get the maximum time for all possible times
		 */
		double init = possibleInitTimes.peek();
		double end = init + runtime;

		job.setState(Job.State.TERMINATED);
		job.setResult(Job.Result.SUCCESS);
		job.setVM(this);
		job.setStartExecutionTime(init);
		job.setFinishExecutionTime(end);

		selectedCore.add(job.getTask(), init, runtime);

		this.events.add(new Event(EventType.EXECUTION, job, init, end));

		return end;

	}

	private VMCore getNextCoreAvailable(double readyTime, double runtime) {
		double min = Double.MAX_VALUE;
		VMCore selectedCore = null;
		
		for (VMCore core : cores) {
			double ready = core.getEarliestIdleSlotTimeFrom(readyTime, runtime);
			if (ready < min) {
				min = ready;
				selectedCore = core;
			}
		}

		// sanity check
		if (selectedCore == null) {
			throw new RuntimeException("No CORE has been selected");
		}

		return selectedCore;
	}

	public double internalTransmit(Job from, Job to, DAGFile dagFile) {

		double init = from.getFinishExecutionTime();

		events.add(new Event(EventType.AUTO_TRANSMISSION, from, to, dagFile, init, init));
		return init + 0.0;
	}

	public double sending(Job from, Job to, DAGFile dagFile, VM dest, double transmissionTime,
			boolean intraTransmission, boolean contention) {
		/*
		 * If the vm (or network) is busy, then the task will wait to transmit
		 */
		double init = 0.0;
		double start = from.getFinishExecutionTime();

		VM src = this;

		if (contention) {
			init = Math.max(src.getAvailableNetworkTime(), start);
			if (dest.getAvailableNetworkTime() > init) {
				events.add(
						new Event(EventType.WAIT_TO_TRANSMIT, from, to, dagFile, init, dest.getAvailableNetworkTime()));
				init = dest.getAvailableNetworkTime();
			}
		} else {
			init = start;
		}

		double end = init + transmissionTime;

		EventType type = null;

		if (intraTransmission) {
			insideTheCloudTransmission.addFileToSend(dest.getVmType().getDatacenterId(), dagFile);
			type = EventType.SENDING_WITHIN_THE_CLOUD;

		} else {
			outsideTheCloudTransmission.addFileToSend(dest.getVmType().getDatacenterId(), dagFile);
			type = EventType.SENDING_OUT_OF_THE_CLOUD;
		}

		events.add(new Event(type, from, to, dagFile, init, end));

		if (contention) {
			this.availableNetworkTime = end;
		}

		return init;
	}

	public double receive(Job from, Job to, DAGFile dagFile, VM dest, double startTime, double transmissionTime,
			boolean intraTransmission, boolean contention) {

		double init = startTime;
		double end = init + transmissionTime;

		EventType type = null;
		if (intraTransmission) {
			insideTheCloudTransmission.addFileToReceive(dest.getVmType().getDatacenterId(), dagFile);
			type = EventType.RECEIVING_WITHIN_THE_CLOUD;
		} else {
			outsideTheCloudTransmission.addFileToReceive(dest.getVmType().getDatacenterId(), dagFile);
			type = EventType.RECEIVING_OUT_OF_THE_CLOUD;
		}

		events.add(new Event(type, from, to, dagFile, init, end));

		if (contention) {
			this.availableNetworkTime = end;
		}

		return end;
	}

	public double getAvailableNetworkTime() {
		return availableNetworkTime;
	}

	public VMType getVmType() {
		return vmType;
	}

	public boolean isUsed() {
		return !this.events.isEmpty();
	}

	public JsonObject getJsonLog() {
		JsonObject log = new JsonObject();

		log.addProperty("VmType", this.vmType.toString());

		log.add("events", events.getJsonLog());

		return log;
	}

	public List<Task> getTaskExecutionsHistory() {
		return this.events.getTaskExecutionsHistory();
	}

	public String getStatement() {
		StringBuffer string = new StringBuffer();
		string.append("##################################\n");
		string.append("LOG -  " + vmType + "\n");
		string.append(events.getStatement());
		string.append("\nInside transmission: ");
		string.append(insideTheCloudTransmission);
		string.append("\nOutside transmission: ");
		string.append(outsideTheCloudTransmission);
		string.append("\nExecution cost: " + getExecutionCost());
		string.append(
				"\nData transfer-in cost  from outside: " + String.format("%f", getDataTransferCostFromOutside()));
		string.append("\nData transfer-out cost to outside: " + String.format("%f", getDataTransferCostToOutside()));
		string.append("\nTotal cost: " + getTheBill());
		string.append("\nUtilization: " + String.format("%.2f", getUtilization()));
		string.append("\n##################################");
		return string.toString();
	}

	public double getTheBill() {
		return getExecutionCost() + getDataTransferCostFromOutside() + getDataTransferCostToOutside();
	}

	public double getDataTransferCostToOutside() {
		double billingDataUnits = getTotalDataTransferToOutside() / vmType.getDataTransferUnit();
		return billingDataUnits * vmType.getDataTransferOutPrice();
	}

	public double getDataTransferCostFromOutside() {
		double billingDataUnits = getTotalDataTransferFromOutside() / vmType.getDataTransferUnit();

		return billingDataUnits * vmType.getDataTransferInPrice();
	}

	public double getTotalDataTransferFromOutside() {
		return outsideTheCloudTransmission.getTotalBytesIn();
	}

	public double getTotalDataTransferToOutside() {
		return outsideTheCloudTransmission.getTotalBytesOut();
	}

	public double getTotalDataTransferFromInside() {
		return insideTheCloudTransmission.getTotalBytesIn();
	}

	public double getTotalDataTransferToInside() {
		return insideTheCloudTransmission.getTotalBytesOut();
	}

	public double getRuntime() {
		return events.getRuntime();
	}

	public double getLaunchTime() {
		return events.getLaunchTime();
	}

	public double getBusyTime() {
		return events.getBusyTime();
	}

	public double getTerminateTime() {
		return events.getTerminateTime();
	}

	/**
	 * Compute the total cost of this VM. This is computed by taking the runtime,
	 * rounding it up to the nearest whole billing unit, and multiplying by the
	 * billing unit price.
	 */
	public double getExecutionCost() {
		double billingUnits = getRuntime() / vmType.getBillingTimeInSeconds();
		double fullBillingUnits = Math.ceil(billingUnits);
		return fullBillingUnits * vmType.getPriceForBillingUnit();
	}

	/** cpu_seconds / (runtime * cores) */
	public double getUtilization() {
		return getBusyTime() / (getRuntime() * vmType.getCores());
	}

	@Override
	public String toString() {
		return "id=" + id + vmType.toString();
	}

	public int getId() {
		return id;
	}

}
