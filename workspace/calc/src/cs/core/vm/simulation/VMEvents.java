package cs.core.vm.simulation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.JsonObject;

import cs.core.dag.Task;
import cs.core.job.Job;

public class VMEvents {

	/** Time that the VM was launched */
	private double launchTime;

	/** Time that the VM was terminated */
	private double terminateTime;

	private double busyTime;

	private List<Event> events;

	private List<Event> historyExecutionEvents;

	private VM vm;

	public VMEvents(VM vm) {
		this.vm = vm;
		initalize();
	}

	public void initalize() {
		this.launchTime = Double.MAX_VALUE;
		this.terminateTime = 0.0;
		this.busyTime = 0.0;
		this.events = new LinkedList<Event>();
		this.historyExecutionEvents = new LinkedList<Event>();
	}

	public boolean add(Event e) {
		launchTime = Math.min(launchTime, e.getStart());
		terminateTime = Math.max(terminateTime, e.getEnd());

		switch (e.getType()) {
		case EXECUTION:
			busyTime += (e.getEnd() - e.getStart());
			historyExecutionEvents.add(e);
			break;
		case RECEIVING_OUT_OF_THE_CLOUD:
		case SENDING_OUT_OF_THE_CLOUD:
		case RECEIVING_WITHIN_THE_CLOUD:
		case SENDING_WITHIN_THE_CLOUD:
		case WAIT_TO_TRANSMIT:
			break;
		default:
			break;
		}
		return events.add(e);
	}

	public String getStatement() {
		StringBuffer string = new StringBuffer();
		if (isEmpty()) {
			string.append(": No schedule event on this vm");
			return string.toString();
		}

		Collections.sort(events);
		for (Event event : events) {
			string.append(event.toString());
		}

		string.append("\n=========================================");
		string.append("\nSummary: ");
		string.append("\nLunching at time: " + launchTime);
		string.append("\nFinalizing at time: " + terminateTime);
		string.append("\nTotal used time: " + getRuntime());
		return string.toString();
	}

	public JsonObject getJsonLog() {

		JsonObject log = new JsonObject();

		int i = 1;
		for (Event event : events) {
			log.add("event_" + String.valueOf(i++), event.getJsonLog());
		}

		return log;

	}

	public List<Task> getTaskExecutionsHistory() {
		List<Task> history = new LinkedList<Task>();
		for (Event e : historyExecutionEvents) {
			Job job = e.getExecutor();
			history.add(job.getTask());
		}
		return history;
	}

	public boolean isEmpty() {
		return events.isEmpty();
	}

	public double getLaunchTime() {
		return launchTime;
	}

	public double getTerminateTime() {
		return terminateTime;
	}

	public double getRuntime() {
		return terminateTime - launchTime;
	}

	public double getBusyTime() {
		return busyTime;
	}

	public VM getVM() {
		return vm;
	}

}
