package cs.core.vm.simulation;

import cs.core.vm.scheduling.VMType;

public class VMFactory {

	public static VM createVM(VMType vmType) {
		VM vm = new VM(vmType);
		return vm;
	}

}
