package cs.core.vm;

import cs.core.dag.Task;

public class VMSlot implements Comparable<VMSlot> {

	private Task task;

	private double startExecutionTime;

	private double finishExecutionTime;

	private double runtime;

	private String coreId;

	private String resourceId;

	public VMSlot(Task t, double startExecutionTime, double runtime, String coreId, String resourceId) {
		this.task = t;
		this.startExecutionTime = startExecutionTime;
		this.runtime = runtime;
		this.finishExecutionTime = startExecutionTime + runtime;
		this.coreId = coreId;
		this.resourceId = resourceId;
	}

	public Task getObject() {
		return task;
	}

	public double getStartExecutionTime() {
		return startExecutionTime;
	}

	public double getFinishExecutionTime() {
		return finishExecutionTime;
	}

	public double getRuntime() {
		return runtime;
	}

	public String getCoreId() {
		return coreId;
	}

	public String getResourceId() {
		return resourceId;
	}

	@Override
	public int compareTo(VMSlot o) {
		// checking by starting execution time
		if (this.getStartExecutionTime() < o.getStartExecutionTime()) {
			return -1;
		} else if (this.getStartExecutionTime() > o.getStartExecutionTime()) {
			return 1;
		}

		return 0;
	}

	@Override
	public String toString() {
		return "<Slot " + task.getId() + ";" + getStartExecutionTime() + ";" + getRuntime() + ";"
				+ getFinishExecutionTime() + " : " + this.resourceId + ":" + this.coreId + ">";
	}
	
}
