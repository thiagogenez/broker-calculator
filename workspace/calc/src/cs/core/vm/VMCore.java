package cs.core.vm;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.SortedSet;
import java.util.TreeSet;

import cs.core.dag.Task;

public class VMCore{

	private SortedSet<VMSlot> slots;

	private HashMap<Task, VMSlot> HT;

	private String id;

	private String resourceId;

	public VMCore(String id, String resourceId) {
		this.id = id;
		this.resourceId = resourceId;

		this.slots = new TreeSet<VMSlot>();
		this.HT = new HashMap<Task, VMSlot>();
	}

	// clear the schedules
	public void clear() {
		slots.clear();
		HT.clear();
	}

	// Scheduling the task
	// PS: the verification if there is a free slot
	// for the task MUST BE done by the resource class
	public void add(Task task, double startExecutionTime, double runtime) {
		VMSlot slot = new VMSlot(task, startExecutionTime, runtime, this.id, this.resourceId);
		this.slots.add(slot);
		this.HT.put(task, slot);
	}

	// The verification if there is a free slot for the
	// execution of the task
	public boolean isFreeToScheduleAt(double startExecutionTime, double runtime) {

		double finishExecutionTime = startExecutionTime + runtime;

		for (VMSlot slot : slots) {

			// case 1: conflict at the beginning of the current slot
			if (slot.getStartExecutionTime() > startExecutionTime
					&& slot.getStartExecutionTime() < finishExecutionTime) {
				return false;
			}

			// case 2: conflict at the end of the current slot
			if (slot.getFinishExecutionTime() > startExecutionTime
					&& slot.getFinishExecutionTime() < finishExecutionTime) {
				return false;
			}

			// case 3: conflict of a smaller (or equal) slot time in comparison
			// with the current slot time

			if (slot.getStartExecutionTime() <= startExecutionTime
					&& slot.getFinishExecutionTime() >= finishExecutionTime) {
				return false;
			}

		}
		// otherwise, there is no slot conflict and then
		// the task can be scheduled in this core from 'startExecutionTime'
		return true;

	}

	// cancel the schedule for the task
	public void cancel(Task task) {
		VMSlot slot = HT.remove(task);
		if (slot != null) {
			slots.remove(slot);
		}
	}

	public SortedSet<VMSlot> getSchedule() {
		return slots;
	}

	public double getFinishExecutionTime(Task t) {
		if (this.HT.containsKey(t)) {
			VMSlot slot = this.HT.get(t);
			return slot.getFinishExecutionTime();
		}
		return Double.MAX_VALUE;
	}

	public double getEarliestIdleSlotTimeFrom(double readyTime, double runtime) {

		if (this.slots.isEmpty()) {
			return readyTime;
		}

		LinkedList<Double> likelyStartExecution = new LinkedList<Double>();
		likelyStartExecution.addLast(readyTime);

		// the end of each busy slot time is a most likely
		// a candidate to a idle slot time
		for (VMSlot slot : this.slots) {
			if (slot.getFinishExecutionTime() > readyTime) {
				likelyStartExecution.addLast(slot.getFinishExecutionTime());
			}
		}

		// lets check if an idle slot time fits the (readyTime + runtime)
		// ---------readyTime
		// ------------|
		// ------------v
		// |---BUSY------| IDLE |------------BUSY-----------|
		for (Double startExecutionTime : likelyStartExecution) {
			if (isFreeToScheduleAt(startExecutionTime, runtime)) {
				return startExecutionTime;
			}
		}

		// otherwise, the earliest idle slot time is the
		// end of the line

		return slots.last().getFinishExecutionTime() + 1;
	}

	public String getId() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Core[" + id + "]";
	}
	

}
